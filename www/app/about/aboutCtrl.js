(function () {
  angular
    .module('idelivery')
    .controller('aboutCtrl', aboutCtrl);

  aboutCtrl.$inject = ['$scope', 'ideliveryServices', '$cordovaAppVersion', '$ionicModal'];
  function aboutCtrl($scope, ideliveryServices, $cordovaAppVersion, $ionicModal) {


    if (ionic.Platform.isIOS()) {
      $scope.device = $scope.IOS.id;
    } else if (ionic.Platform.isAndroid()) {
      $scope.device = $scope.ANDROID.id;
    }
    if (window.cordova) {
      $cordovaAppVersion.getVersionNumber().then(function (version) {
        $scope.appVersion = version;

        var data = {
          version: $scope.appVersion,
          device_type: $scope.device,
          slug: configIDL.ESTABLISHMENT_SLUG
        };

        ideliveryServices.validateAppVersion(data).then(function (response) {
        });

        ideliveryServices.getRestaurantDetails().then(function (response) {
          $scope.restaurant = response.data;
        });
      });
    }

    /////////////////
    // Modal Terms //
    /////////////////

    $ionicModal.fromTemplateUrl('app/main/termsModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.termsModal = modal;
    });

    $scope.closeTermsModal = function () {
      $scope.termsModal.hide();
    };

    $scope.openTermsModal = function () {
      $scope.termsModal.show();
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.termsModal.remove();
    });

  }
})();
