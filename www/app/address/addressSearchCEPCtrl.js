(function () {
  "use strict";

  angular.module('idelivery')
    .controller('addressSearchCEPCtrl', addressSearchCEPCtrl);

  addressSearchCEPCtrl.$inject = ['$state', '$scope', '$ionicModal', 'ideliverySearchAddressService', 'sharedProperties'];
  function addressSearchCEPCtrl($state, $scope, $ionicModal, ideliverySearchAddressService, sharedProperties) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.ufs = Util.UFS;
      $scope.cities = {};

      $scope.selectedStore = JSON.parse(Util.getLocalSelectedStore());

      $scope.searchCEPData = sharedProperties.getAddress().data;
    });

    $scope.$on('$ionicView.enter', function () {
      $scope.searchCEP();
    });

    $scope.searchAddressCEPData = {};
    $scope.setDeliveryAddressByName = function () {

      var cep = $scope.searchAddressCEPData.postal_code;
      $scope.searchAddressCEPData.postal_code = parseInt(cep.replace(/-/g, ""));

      var addressData = {
        city: $scope.selectedCityName,
        formatted_address: $scope.searchAddressCEPData.formatted_address,
        state: $scope.selectedStateName,
        short_state: $scope.searchAddressCEPData.short_state,
        number: $scope.searchAddressCEPData.number,
        neighborhood: $scope.searchAddressCEPData.neighborhood,
        postal_code: $scope.searchAddressCEPData.postal_code,
        complement: $scope.searchAddressCEPData.complement,
        reference_point: $scope.searchAddressCEPData.reference_point
      };

      if (addressData && Util.getLocalToken()) {
        ideliverySearchAddressService.setAddress(addressData).then(function (response) {
          localStorage.setItem('deliveryAddress-' + configIDL.ESTABLISHMENT_SLUG, JSON.stringify(response.data.address_user));
          localStorage.removeItem('region-' + configIDL.ESTABLISHMENT_SLUG);

          if (response.data.count_establishments > 1) {
            localStorage.removeItem('store_selected-' + configIDL.ESTABLISHMENT_SLUG);
            $state.go('app.stores');
          } else {
            $state.go('app.products')
          }

          notie.alert(1, 'O endereço foi cadastrado.', 3);

        }, function () {
          notie.alert(3, 'Erro ao cadastrar. Verifique se todos os campos estão corretos.', 3);
        });
      } else {
        localStorage.setItem('deliveryAddress-' + configIDL.ESTABLISHMENT_SLUG, JSON.stringify(addressData));

        localStorage.removeItem('store_selected-' + configIDL.ESTABLISHMENT_SLUG);
        localStorage.removeItem('region-' + configIDL.ESTABLISHMENT_SLUG);

        if (Util.getStoresQuantity() > 1)
          $state.go('app.stores');
        else
          $state.go('app.products');

        notie.alert(1, 'O endereço foi salvo.', 3);
      }
    };

    $scope.searchCEP = function () {

      ideliverySearchAddressService.cepSearch($scope.searchCEPData.CEP).then(
        function (response) {
          if (response.data.status == 200) {

            $scope.searchAddressCEPData = response.data.data;

            ideliverySearchAddressService.getStateNameByUF($scope.searchAddressCEPData.uf).then(function (response) {
              ideliverySearchAddressService.getStateByName(response.data[0].name).then(function (res) {
                if (res.data[0] && res.data[0].name === response.data[0].name) {
                  $scope.selectedStateName = res.data[0].name;
                  $scope.currentStateId = res.data[0].id;

                  ideliverySearchAddressService.getCityByName($scope.searchAddressCEPData.localidade, $scope.currentStateId).then(function (rs) {
                    if (rs.data[0] && rs.data[0].name === $scope.searchAddressCEPData.localidade) {
                      $scope.selectedCityName = rs.data[0].name;
                      $scope.currentCityId = rs.data[0].id;

                      ideliverySearchAddressService.getNeighborhoodByName($scope.searchAddressCEPData.bairro, $scope.currentCityId).then(function (r) {
                        if (r.data[0] && r.data[0].name === $scope.searchAddressCEPData.bairro) {
                          $scope.selectedNeighborhoodName = r.data[0].name;
                          $scope.searchAddressCEPData.neighborhood = r.data[0].id;
                        } else {
                          $scope.searchAddressCEPData.neighborhood = "";
                          $scope.selectedNeighborhoodName = $scope.searchAddressCEPData.neighborhood;
                        }
                      });
                    } else {
                      $scope.selectedCityName = "";
                      $scope.searchAddressCEPData.city = "";
                    }
                  });
                } else {
                  notie.alert(3, 'Não foi possível trazer todas as informações do CEP atual. Preencha o endereço manualmente.', 7);
                  $scope.selectedStateName = "";
                  $scope.searchAddressCEPData.state = "";
                }

              });

              var logradouro = $scope.searchAddressCEPData.logradouro;
              if (logradouro.indexOf(' -') !== -1) {
                $scope.searchAddressCEPData.formatted_address = logradouro.substring(0, logradouro.indexOf(' -'));
              } else {
                $scope.searchAddressCEPData.formatted_address = logradouro;
              }
              $scope.searchAddressCEPData.number = $scope.searchCEPData.number;
              $scope.searchAddressCEPData.postal_code = $scope.searchAddressCEPData.cep;
              $scope.searchAddressCEPData.short_state = $scope.searchAddressCEPData.uf;
            });
          } else {
            notie.alert(3, 'Não foi possível trazer todas as informações do CEP atual. Preencha o endereço manualmente.', 7);
          }
        },
        function () {
          notie.alert(3, 'Não foi possível trazer todas as informações do CEP atual. Preencha o endereço manualmente.', 7);
        }
      );
    };

    //////////////////
    // Modal States //
    //////////////////

    $ionicModal.fromTemplateUrl('app/address/statesModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.statesModal = modal;
    });

    $scope.closeStatesModal = function (stateName, stateId, stateUf) {
      $scope.statesModal.hide();
      if (stateId) {
        $scope.selectedStateName = stateName;
        $scope.searchAddressCEPData.state = stateName;
        $scope.searchAddressCEPData.short_state = stateUf;
        $scope.currentStateId = stateId;

        $scope.selectedCityName = "";
        $scope.searchAddressCEPData.city = "";
        $scope.currentCityId = "";

        $scope.selectedNeighborhoodName = "";
        $scope.searchAddressCEPData.neighborhood = "";
      }
    };

    $scope.openStatesModal = function () {
      $scope.statesModal.show();
      ideliverySearchAddressService.getStates().then(function (response) {
        $scope.states = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.statesModal.remove();
    });

    //////////////////
    // Modal Cities //
    //////////////////

    $scope.currentCity = {};

    $ionicModal.fromTemplateUrl('app/address/citiesModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.citiesModal = modal;
    });

    $scope.closeCitiesModal = function (cityName, cityId) {
      $scope.citiesModal.hide();
      if (cityName) {
        $scope.selectedCityName = cityName;
        $scope.searchAddressCEPData.city = cityName;
        $scope.currentCityId = cityId;

        $scope.selectedNeighborhoodName = "";
        $scope.searchAddressCEPData.neighborhood = "";
      }
    };

    $scope.openCitiesModal = function () {
      $scope.citiesModal.show();
      ideliverySearchAddressService.getCities($scope.currentStateId).then(function (response) {
        $scope.stateCities = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.citiesModal.remove();
    });

    /////////////////////////
    // Modal Neighborhoods //
    /////////////////////////

    $scope.currentNeighborhood = {};

    $ionicModal.fromTemplateUrl('app/address/neighborhoodsModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.neighborhoodsModal = modal;
    });

    $scope.closeNeighborhoodsModal = function (neighborhoodName, neighborhoodId) {
      $scope.neighborhoodsModal.hide();
      if (neighborhoodId) {
        $scope.selectedNeighborhoodName = neighborhoodName;
        $scope.searchAddressCEPData.neighborhood = neighborhoodId;
        if (!neighborhoodId) {
          $scope.neighborhoods.name = neighborhoodName;
        }
      }
    };

    $scope.openNeighborhoodsModal = function () {
      $scope.neighborhoodsModal.show();
      ideliverySearchAddressService.getNeighborhoods($scope.currentCityId).then(function (response) {
        $scope.neighborhoods = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.neighborhoodsModal.remove();
    });

    //This event fires when the keyboard will be shown
    if ($scope.isIOS()) {
      window.addEventListener('native.keyboardhide', function () {
        angular.element(document.querySelector('#id-scroll-deway')).addClass('resize-scroll');
      });

      window.addEventListener('native.keyboardshow', function () {
        angular.element(document.querySelector('#id-scroll-deway')).removeClass('resize-scroll');
        $ionicScrollDelegate.resize();
      });
    }

  }
})();
