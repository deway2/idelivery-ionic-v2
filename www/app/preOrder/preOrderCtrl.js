(function () {
  angular.module("idelivery").controller("preOrderCtrl", preOrderCtrl);

  preOrderCtrl.$inject = [
    "$scope",
    "ideliveryServices",
    "sharedProperties",
    "productCompositionService",
    "$state",
    "$ionicHistory",
  ];
  function preOrderCtrl(
    $scope,
    ideliveryServices,
    sharedProperties,
    productCompositionService,
    $state,
    $ionicHistory
  ) {
    productCompositionService.isPreOrder(true);

    $scope.$on("$ionicView.beforeEnter", function () {
      $scope.deliveryAddress = JSON.parse(
        localStorage.getItem("deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG)
      );

      $scope.preOrder = productCompositionService.getObjects();
      $scope.product = productCompositionService.getProduct();

      $scope.counter = 1;

      if ($scope.preOrder.length > 0) {
        var arr = [];
        for (var i = 0; i < $scope.preOrder.length; i++) {
          for (var j = 0; j < $scope.preOrder[i].length; j++) {
            arr.push($scope.preOrder[i][j]);
            $scope.preOrderCompositions = arr;
          }
        }
      }

      $scope.image_array = [$scope.product.cover_image];
      if ($scope.product.image_one != null)
        $scope.image_array.push($scope.product.image_one);
      if ($scope.product.image_two != null)
        $scope.image_array.push($scope.product.image_two);
      if ($scope.product.image_three != null)
        $scope.image_array.push($scope.product.image_three);
      if ($scope.product.image_four != null)
        $scope.image_array.push($scope.product.image_four);
      if ($scope.product.image_five != null)
        $scope.image_array.push($scope.product.image_five);
    });

    $scope.$on("$ionicView.enter", function () {
      var itemData = {
        establishment_id: sharedProperties.getEstablishmentId(),
        items: [
          {
            productId: $scope.product.id,
            quantity: $scope.counter,
            compositions: $scope.preOrderCompositions,
          },
        ],
        delivery_address: $scope.deliveryAddress.id,
        isPickUp: sharedProperties.getIsPickUp(),
      };

      ideliveryServices.calculateOrder(itemData).then(function (response) {
        $scope.unityValue = response.data.items[0].unityValue;
        $scope.totalValue = $scope.unityValue;
      });
    });

    $scope.duplicateItems = function () {
      $scope.totalValue += $scope.unityValue;
      $scope.counter++;
    };

    $scope.decreaseItems = function () {
      if ($scope.totalValue > 1 && $scope.counter > 1) {
        $scope.totalValue -= $scope.unityValue;
        $scope.counter--;
      }
    };

    $scope.messageSuccess = function () {
      notie.alert(1, "Item adicionado ao carrinho.", 3);
    };

    $scope.addToCart = function () {
      $scope.orderToCart = {
        productName: $scope.product.name,
        productId: $scope.product.id,
        compositions: $scope.preOrderCompositions,
        quantity: $scope.counter,
        comments: $scope.product.comments,
        unityValue: $scope.unityValue,
        totalOrder: $scope.totalValue,
      };
      productCompositionService.restartObject();
      sharedProperties.setCart($scope.orderToCart);
      $state.go("app.products");
      $ionicHistory.clearHistory();
      $scope.messageSuccess();
    };

    /*-- Photo Viewer --*/
    $scope.viewer = function (index) {
      PhotoViewer.show($scope.image_array[index], "Imagem " + (index + 1));
    };
  }
})();
