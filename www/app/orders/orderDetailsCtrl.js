(function () {
  angular
    .module('idelivery')
    .controller('orderDetailsCtrl', orderDetailsCtrl);

  orderDetailsCtrl.$inject = ['$scope', 'ideliveryServices', '$stateParams', 'ideliverySearchAddressService'];
  function orderDetailsCtrl($scope, ideliveryServices, $stateParams, ideliverySearchAddressService) {

    ideliveryServices.orderDetails($stateParams.orderId).then(function (response) {
      var orderDetails = response.data;
      $scope.orderDetail = JSON.parse(orderDetails[0].orderJson);
      $scope.order = response.data[0];
      $scope.refuseReason = response.data[0].refuse_message_client;
      $scope.shipping = response.data[0].shipping;
      let value = $scope.orderDetail.subtotal + $scope.shipping - $scope.orderDetail.total;
      $scope.discount = {
        value: value,
        percent: value/($scope.orderDetail.subtotal) * 100
      }
      console.log($scope.orderDetail)

      ideliverySearchAddressService.getAddressById($scope.orderDetail.delivery_address).then(function (res) {
        $scope.delivery_address = res.data.street + ", " + res.data.number + ", " + res.data.neighborhood.name;
        res.data.complement ? $scope.complement = "(" + res.data.complement + ")" : $scope.complement = "";
      });
    });

    $scope.setOrderStatusName = function (status) {
      if (status == 1) {
        return "Pendente";
      } else if (status == 2) {
        return "Confirmado";
      } else if (status == 3) {
        return "Recusado";
      } else if (status == 4) {
        return "Cancelado";
      }
    };

    $scope.setOrderPayFormName = function (payform) {
      if (payform == 1) {
        return "Dinheiro";
      } else if (payform == 2) {
        return "Cartão";
      }
    };
  }
})();
