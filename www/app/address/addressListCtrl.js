(function () {
  angular
    .module('idelivery')
    .controller('addressListCtrl', addressListCtrl);

  addressListCtrl.$inject = ['$state', '$scope', 'ideliverySearchAddressService', 'sharedProperties', '$ionicPopup', '$ionicListDelegate'];
  function addressListCtrl($state, $scope, ideliverySearchAddressService, sharedProperties, $ionicPopup, $ionicListDelegate) {

    $scope.addressList = [];

    $scope.currentSelectedAddress = Util.getCurrentSelectedAddress();

    if (Util.getLocalToken()) {
      ideliverySearchAddressService.getAddress().then(function (response) {
        $scope.addressList = response.data.address;
        $scope.addressEstablishments = response.data.count_establishments;
      });
    }

    $scope.delItem = function (address) {

      $ionicPopup.show({
        title: 'Deseja realmente apagar este endereço?',
        content: "Atenção: Esta ação não poderá ser desfeita. E se caso for o atual endereço selecionado, o carrinho será esvaziado.",
        buttons: [{
          text: 'Cancelar',
          type: 'button-light'
        }, {
          text: 'Apagar',
          type: 'button-positive',
          onTap: function () {
            ideliverySearchAddressService.deleteAddress(address.id).then(function (response) {
              if (response.status === 200) {
                $scope.addressList.splice($scope.addressList.indexOf(address), 1);
                $ionicListDelegate.closeOptionButtons();

                if ($scope.currentSelectedAddress && address.id === $scope.currentSelectedAddress.id) {
                  localStorage.removeItem('deliveryAddress-' + configIDL.ESTABLISHMENT_SLUG);
                  sharedProperties.getCart().data = [];
                  if ($scope.addressEstablishments > 1) {
                    localStorage.removeItem('store_selected-' + configIDL.ESTABLISHMENT_SLUG);
                    $state.go('app.stores');
                  }
                }

                notie.alert(2, 'O endereço foi apagado.', 3);
              } else {
                notie.alert(3, 'Não foi possível apagar seu endereço no momento. Tente novamente.', 5);
              }
            });
          }
        }]
      });
    };

    $scope.delOption = {
      showDelete: false
    };

    $scope.onItemDelete = function (address) {

      $ionicPopup.show({
        title: 'Deseja realmente apagar este endereço?',
        content: "Atenção: Esta ação não poderá ser desfeita. E se caso for o atual endereço selecionado, o carrinho será esvaziado.",
        buttons: [{
          text: 'Cancelar',
          type: 'button-light'
        }, {
          text: 'Apagar',
          type: 'button-positive',
          onTap: function () {
            ideliverySearchAddressService.deleteAddress(address.id).then(function (response) {
              if (response.status === 200) {
                $scope.addressList.splice($scope.addressList.indexOf(address), 1);
                $scope.delOption.showDelete = false;

                if ($scope.currentSelectedAddress && address.id === $scope.currentSelectedAddress.id) {
                  localStorage.removeItem('deliveryAddress-' + configIDL.ESTABLISHMENT_SLUG);
                  sharedProperties.getCart().data = [];
                  if ($scope.addressEstablishments > 1) {
                    localStorage.removeItem('store_selected-' + configIDL.ESTABLISHMENT_SLUG);
                    $state.go('app.stores');
                  }
                }

                notie.alert(2, 'O endereço foi apagado.', 3);
              } else {
                notie.alert(3, 'Não foi possível apagar seu endereço no momento. Tente novamente.', 5);
              }
            });
          }
        }]
      });
    };

    $scope.addNewAddress = function () {
      $state.go('app.address');
    };

    $scope.setCurrentAddress = function (address) {

      if (sharedProperties.getCart().data.length > 0) {
        $ionicPopup.show({
          title: 'Você possui itens no carrinho.',
          content: "Deseja esvaziá-lo e escolher outro endereço?",
          buttons: [{
            text: 'Cancelar',
            type: 'button-light'
          }, {
            text: 'Sim',
            type: 'button-positive',
            onTap: function () {
              $scope.changeCurrentAddress(address);
              sharedProperties.getCart().data = [];
            }
          }]
        });
      } else {
        $scope.changeCurrentAddress(address);
      }
    };

    $scope.changeCurrentAddress = function (address) {
      localStorage.setItem('deliveryAddress-' + configIDL.ESTABLISHMENT_SLUG, JSON.stringify(address));

      notie.alert(1, 'O endereço foi definido.', 3);

      localStorage.removeItem('region-' + configIDL.ESTABLISHMENT_SLUG);

      if ($scope.addressEstablishments > 1) {
        localStorage.removeItem('store_selected-' + configIDL.ESTABLISHMENT_SLUG);
        $state.go('app.stores');
      } else {
        $state.go('app.products');
      }
    };

  }
})();
