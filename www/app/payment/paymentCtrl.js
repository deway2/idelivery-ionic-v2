(function () {
  angular.module("idelivery").controller("paymentCtrl", paymentCtrl);

  paymentCtrl.$inject = [
    "$scope",
    "ideliveryServices",
    "sharedProperties",
    "$ionicLoading",
    "RESOURCES",
    "$state",
    "$ionicModal",
    "$ionicPopup",
  ];
  function paymentCtrl(
    $scope,
    ideliveryServices,
    sharedProperties,
    $ionicLoading,
    RESOURCES,
    $state,
    $ionicModal,
    $ionicPopup
  ) {
    $scope.hasDiscount = false;
    $scope.hasDiscountCode = false;

    $scope.specieChangeAltered = function () {
      if ($scope.hasDiscount) {
        $scope.finishedOrder.totalChange =
          $scope.finishedOrder.payForm.specieChange -
          ($scope.finishedOrder.total -
            $scope.appliedDiscount.amountDiscounted);
      } else {
        $scope.finishedOrder.totalChange =
          $scope.finishedOrder.payForm.specieChange -
          $scope.finishedOrder.total;
      }
    };

    if (configIDL.OPTIONAL_RECEIPT != 2) {
      if (configIDL.OPTIONAL_RECEIPT == 3) $scope.showRequiredReceipt = true;
      else $scope.showReceipt = true;
    }

    $scope.commentsText = configIDL.COMMENTS_TEXT;

    $scope.$on("$ionicView.beforeEnter", function () {
      var currentDevicePlatform = {
        name: ionic.Platform.platform(),
        version: ionic.Platform.version(),
      };

      $scope.maskForOldAndroid =
        currentDevicePlatform.name === "android" &&
        currentDevicePlatform.version <= 4.3;
      $scope.isPickUp = sharedProperties.getIsPickUp();
      $scope.store = JSON.parse(Util.getLocalSelectedStore());
      $scope.currentUser = JSON.parse(Util.getCurrentUserData());
      if (configIDL.ESTABLISHMENT_SLUG == "betapizzaria.deway.com.br") {
        $scope.isMarketPlace = true;

        $scope.TimeToDelivery();
        $scope.accept_schedule = $scope.store.accept_schedule;
        $scope.accept_pickup_schedule = $scope.store.accept_pickup_schedule;

        var getFavorite = {
          establishment: $scope.store.id,
          user: $scope.currentUser,
        };
        ideliveryServices.getFavorite(getFavorite).then(function (response) {
          $scope.isFavorite = response.data.data.isFavorite;
        });
      } else if (
        configIDL.ESTABLISHMENT_SLUG == "br.com.ideliveryapp.chicodocarangueijo"
      ) {
        $scope.isMarketPlace = true;
        $scope.accept_schedule = $scope.store.accept_schedule;
        $scope.accept_pickup_schedule = $scope.store.accept_pickup_schedule;
      } else {
        $scope.isMarketPlace = false;
      }

      $scope.finishedOrder = sharedProperties.getFinishedOrder().data;
      $scope.card = sharedProperties.getSelectedCard().data;
      if ($scope.isPickUp) {
        $scope.finishedOrder.isPickUp = true;
      }
      $scope.establishmentSlug = configIDL.ESTABLISHMENT_SLUG;
      $scope.finishedOrder.establishment_id = configIDL.ESTABLISHMENT_ID;

      ideliveryServices
        .calculateOrder($scope.finishedOrder)
        .then(function (response) {
          var itemsTotal = 0;
          for (var i = 0; i < response.data.items.length; i++) {
            $scope.finishedOrder.items[i].totalOrder =
              response.data.items[i].totalOrder;
            itemsTotal += $scope.finishedOrder.items[i].totalOrder;
          }
          $scope.finishedOrder.freightCost = response.data.freightCost;
          $scope.discount = response.data.discount;
          $scope.discount_mesage = response.data.discount_message;
          $scope.finishedOrder.subtotal = itemsTotal;

          if ($scope.discount) {
            $scope.discountValue =
              $scope.finishedOrder.subtotal * $scope.discount;
            $scope.finishedOrder.total =
              response.data.total - $scope.discountValue;
            if (response.data.total < $scope.discountValue)
              $scope.finishedOrder.total = 0.0;
          } else {
            $scope.finishedOrder.total = response.data.total;
          }
          if ($scope.finishedOrder.discountCode) {
            let discount = {
              establishment_id: $scope.finishedOrder.establishment_id,
              discountCode: $scope.finishedOrder.discountCode,
            };
            validateDiscount(discount);
          }
        });
      ideliveryServices.getRestaurantDetails().then((response) => {
        $scope.establishment = response.data;
        $scope.establishmentAddress =
          $scope.establishment.street +
          ", " +
          $scope.establishment.number +
          ", " +
          $scope.establishment.neighborhood +
          ", " +
          $scope.establishment.city.name +
          " - " +
          $scope.establishment.state.name;
      });
      $scope.specie_negative_id = configIDL.PAY_TYPES[0].negative_id;
      $scope.card_negative_id = configIDL.PAY_TYPES[1].negative_id;
      $scope.online_negative_id = configIDL.PAY_TYPES[2].negative_id;

      ideliveryServices.getPaymentTypes().then(function (response) {
        $scope.paymentForms = response.data;
      });

      ideliveryServices.getAcceptedPaymentTypes().then(function (response) {
        $scope.acceptedPaymentTypes = response.data;
        console.log(response.data, "adasdas");
      });

      // Delivery address

      $scope.address = JSON.parse(
        localStorage.getItem("deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG)
      );

      $scope.address.complement
        ? ($scope.complement = "(" + $scope.address.complement + ")")
        : ($scope.complement = "");

      $scope.finishedOrder.delivery_address = $scope.address.id;
    });

    $scope.setPaymentForm = function (paymentForm) {
      if (paymentForm == $scope.specie_negative_id) {
        $scope.finishedOrder.payForm = configIDL.PAY_TYPES[0];
        $state.go("app.speciePayment");
        $scope.finishedOrder.totalChange =
          $scope.finishedOrder.payForm.specieChange -
          $scope.finishedOrder.total;
      } else if (paymentForm == $scope.card_negative_id) {
        $scope.finishedOrder.payForm = configIDL.PAY_TYPES[1];
        $state.go("app.cardPayment");
        $ionicPopup.confirm({
          title: "AVISO",
          template:
            "O pagamento via cartão é <b>PRESENCIAL</b>, ou seja, seu cartão é utilizado na maquineta na sua presença no momento da entrega",
          buttons: [
            {
              text: "Mudar Forma de Pagamento",
              type: "button-light",
              onTap: function (e) {
                $state.go("app.paymentForm");
              },
            },
            {
              text: "OK, Entendi.",
              type: "button-positive",
              onTap: function (e) {},
            },
          ],
        });
      } else if (paymentForm == $scope.online_negative_id) {
        $scope.finishedOrder.payForm = configIDL.PAY_TYPES[2];
        $state.go("app.onlinePayment");
      }
    };
    $scope.dealOrderDelivery = (order) => {
      if (!$scope.chooseTimeScheduled) {
        $ionicPopup.confirm({
          title: "AVISO",
          template:
            "<span> Por favor, selecione um horário para realizar o agendamento </span>",
          buttons: [
            {
              text: "Confirmar",
              type: "button-positive",
            },
          ],
        });
      } else {
        $ionicPopup.confirm({
          title: "AVISO",
          template:
            "<span> Você selecionou a entrega para " +
            $scope.dateFull +
            ", entre " +
            $scope.chooseTimeScheduled +
            ". Esse prazo poderá variar, aguarde a confirmação do lojista</span>",
          buttons: [
            {
              text: "Alterar",
              type: "button-light",
              onTap: () => {},
            },
            {
              text: "Confirmar",
              type: "button-positive",
              onTap: () => {
                $scope.chooseScheduled = true;
                $scope.chooseTimeDelivery =
                  sharedProperties.getChooseTimeDelivery().data;
                if ($scope.chooseTimeDelivery == "") {
                  $scope.chooseTimeDelivery = "Imediata";
                }
                order.is_scheduled = true;
                order.scheduled_delivery = $scope.chooseTimeDelivery;
                order.scheduled_delivery_date =
                  $scope.dateYear +
                  "-" +
                  $scope.dateMonth +
                  "-" +
                  $scope.dateDay +
                  "T" +
                  $scope.scheduledTime +
                  ":00.0000";
                order.scheduled_delivery_date_end =
                  $scope.dateYear +
                  "-" +
                  $scope.dateMonth +
                  "-" +
                  $scope.dateDay +
                  "T" +
                  $scope.scheduledTimeEnd +
                  ":00.0000";
                $scope.closeSchedulingDeliveryModal();
              },
            },
          ],
        });
      }
    };

    $scope.hourClicked = (timeScheduled, $index) => {
      if (!$scope.isPickUp) {
        $scope.chooseTimeScheduled = timeScheduled;
        sharedProperties.setChooseTimeDelivery($scope.chooseTimeScheduled);
        let fields = $scope.chooseTimeScheduled.split(" - ");
        $scope.scheduledTime = fields[0];
        $scope.scheduledTimeEnd = fields[1];
      } else if ($scope.isPickUp) {
        $scope.chooseTimePickUp = timeScheduled;
        let fields = $scope.chooseTimePickUp.split(" - ");
        $scope.pickUpTime = fields[0];
        $scope.pickUpTimeEnd = fields[1];
      }
      $scope.selectedIndex = $index;
    };

    $scope.TimeToDelivery = () => {
      ideliveryServices.availableSchedulingShifts().then((response) => {
        $scope.availableShiftsNowDay = response.data.shifts_delivery.now_date;
        $scope.availableShiftsNextDay = response.data.shifts_delivery.next_date;
        $scope.availableShiftsComingDay =
          response.data.shifts_delivery.coming_date;

        var lengthAvailableShiftsNowDay = $scope.availableShiftsNowDay.length;
        var lengthAvailableShiftsNextDay = $scope.availableShiftsNextDay.length;
        var lengthAvailableShiftsComingDay =
          $scope.availableShiftsComingDay.length;

        $scope.timeScheduling = [];

        let date = new Date($scope.availableShiftsNowDay[0]);
        $scope.dateDay = ("0" + date.getDate()).slice(-2);
        $scope.dateMonth = ("0" + (date.getMonth() + 1)).slice(-2);
        $scope.dateYear = date.getFullYear();
        $scope.dateFull =
          $scope.dateDay + "/" + $scope.dateMonth + "/" + $scope.dateYear;

        if (lengthAvailableShiftsComingDay > 1) {
          let date = new Date($scope.availableShiftsComingDay[0]);
          let dateDay = ("0" + date.getDate()).slice(-2);
          let dateMonth = ("0" + (date.getMonth() + 1)).slice(-2);
          let dateYear = date.getFullYear();
          $scope.dateFullComing = dateDay + "/" + dateMonth + "/" + dateYear;
        }
        for (
          indexFirst = 0, indexNext = 1;
          indexFirst < lengthAvailableShiftsNowDay - 1;
          indexFirst++, indexNext++
        ) {
          let timeFirst = moment($scope.availableShiftsNowDay[indexFirst]);
          let timeNext = moment($scope.availableShiftsNowDay[indexNext]);

          $scope.timeScheduling[indexFirst] =
            timeFirst.hours() + ":00" + " - " + timeNext.hours() + ":00";
        }

        sharedProperties.setAvailableShifts($scope.timeScheduling);

        $scope.deliveryTomorrow = () => {
          $scope.timeScheduling = [];
          let date = new Date($scope.availableShiftsNextDay[0]);

          $scope.dateDay = ("0" + date.getDate()).slice(-2);
          $scope.dateMonth = ("0" + (date.getMonth() + 1)).slice(-2);
          $scope.dateYear = date.getFullYear();
          $scope.dateFull =
            $scope.dateDay + "/" + $scope.dateMonth + "/" + $scope.dateYear;

          for (
            indexFirst = 0, indexNext = 1;
            indexFirst < lengthAvailableShiftsNextDay - 1;
            indexFirst++, indexNext++
          ) {
            let timeFirst = moment($scope.availableShiftsNextDay[indexFirst]);
            let timeNext = moment($scope.availableShiftsNextDay[indexNext]);

            $scope.timeScheduling[indexFirst] =
              timeFirst.hours() + ":00" + " - " + timeNext.hours() + ":00";
          }
          sharedProperties.setAvailableShifts($scope.timeScheduling);
        };

        $scope.deliveryComing = () => {
          $scope.timeScheduling = [];
          $scope.dateFull = $scope.dateFullComing;

          for (
            indexFirst = 0, indexNext = 1;
            indexFirst < lengthAvailableShiftsComingDay - 1;
            indexFirst++, indexNext++
          ) {
            let timeFirst = moment($scope.availableShiftsComingDay[indexFirst]);
            let timeNext = moment($scope.availableShiftsComingDay[indexNext]);

            $scope.timeScheduling[indexFirst] =
              timeFirst.hours() + ":00" + " - " + timeNext.hours() + ":00";
          }
          sharedProperties.setAvailableShifts($scope.timeScheduling);
        };
      });
    };

    ////////////////////////
    // Scheduling Pick Up //
    ////////////////////////

    $scope.isIOS = () => {
      return ionic.Platform.isIOS();
    };
    $scope.pickUpOpenMaps = () => {
      let date = new Date();
      let dateFull =
        ("0" + date.getDate()).slice(-2) +
        "/" +
        ("0" + (date.getMonth() + 1)).slice(-2) +
        "/" +
        date.getFullYear();
      if (
        $scope.chooseTimePickUp == $scope.timePickUpScheduling[0] &&
        $scope.dateFull == dateFull &&
        !$scope.isIOS()
      ) {
        $ionicPopup.confirm({
          title: "Navegar Agora ?",
          template:
            "Escolha seu aplicativo favorito para navegar até nossa loja!",
          buttons: [
            {
              text: "Waze",
              type: "button-positive",
              onTap: () => {
                $scope.pickUpOpenWazeMaps();
              },
            },
            {
              text: "Google Maps",
              type: "button-positive",
              onTap: () => {
                $scope.pickUpOpenGoogleMaps();
              },
            },
            {
              text: "X",
              onTap: () => {
                return true;
              },
            },
          ],
        });
      } else if (
        $scope.chooseTimePickUp == $scope.timePickUpScheduling[0] &&
        $scope.dateFull == dateFull &&
        $scope.isIOS()
      ) {
        $ionicPopup.confirm({
          title: "Navegar Agora ?",
          template:
            "Escolha seu aplicativo favorito para navegar até nossa loja!",
          buttons: [
            {
              text: "Cancelar",
              type: "button-light",
              onTap: () => {
                return true;
              },
            },
            {
              text: "Confirmar",
              type: "button-positive",
              onTap: () => {
                $scope.openMapsModal();
              },
            },
          ],
        });
      }
    };

    $scope.pickUpOpenMapsIOS = () => {
      const url = "maps://maps.apple.com/?daddr=" + $scope.establishmentAddress;
      ideliveryServices.openLink(url);
    };

    $scope.pickUpOpenGoogleMaps = () => {
      ideliveryServices.openMap($scope.establishmentAddress);
    };

    $scope.pickUpOpenWazeMaps = () => {
      const url = "https://waze.com/ul?q=" + $scope.establishmentAddress;
      ideliveryServices.openLink(url);
    };

    $scope.dealOrderPickUp = (order) => {
      if (!$scope.chooseTimePickUp) {
        $ionicPopup.confirm({
          title: "AVISO",
          template:
            "<span> Por favor, selecione um horário para realizar o agendamento </span>",
          buttons: [
            {
              text: "Confirmar",
              type: "button-positive",
            },
          ],
        });
      } else {
        $ionicPopup.confirm({
          title: "AVISO",
          template:
            "<span> Você selecionou a retirada para " +
            $scope.dateFull +
            ", entre " +
            $scope.chooseTimePickUp +
            ". Esse prazo poderá variar, aguarde a confirmação do lojista</span>",
          buttons: [
            {
              text: "Alterar",
              type: "button-light",
              onTap: () => {},
            },
            {
              text: "Confirmar",
              type: "button-positive",
              onTap: () => {
                $scope.selectTimeToPickUp = true;
                order.is_pick_up = true;
                order.scheduled_pick_up = $scope.chooseTimePickUp;
                order.scheduled_pick_up_date =
                  $scope.dateYear +
                  "-" +
                  $scope.dateMonth +
                  "-" +
                  $scope.dateDay +
                  "T" +
                  $scope.pickUpTime +
                  ":00.0000";
                $scope.closeSchedulingPickUpModal();
              },
            },
          ],
        });
      }
    };
    $scope.TimeToPickUp = () => {
      ideliveryServices.availableSchedulingShifts().then((response) => {
        $scope.availablePickUpShiftsToday =
          response.data.shifts_pick_up.now_date.full_time;
        $scope.availablePickUpShiftsTomorrow =
          response.data.shifts_pick_up.next_date.full_time;

        var lengthAvailablePickUpShiftsToday =
          $scope.availablePickUpShiftsToday.length;
        var lengthAvailablePickUpShiftsTomorrow =
          $scope.availablePickUpShiftsTomorrow.length;
        $scope.timePickUpScheduling = [];

        let date = new Date($scope.availablePickUpShiftsToday[0]);
        $scope.dateDay = ("0" + date.getDate()).slice(-2);
        $scope.dateMonth = ("0" + (date.getMonth() + 1)).slice(-2);
        $scope.dateYear = date.getFullYear();
        $scope.dateFull =
          $scope.dateDay + "/" + $scope.dateMonth + "/" + $scope.dateYear;

        let firstHour = response.data.shifts_pick_up.now_date.hour[0];
        let firstMinute = response.data.shifts_pick_up.now_date.minute[0];
        let secondHour = response.data.shifts_pick_up.now_date.hour[1];
        let secondMinute = response.data.shifts_pick_up.now_date.minute[1];

        firstMinute > 9 && secondMinute > 9
          ? $scope.timePickUpScheduling.push(
              firstHour +
                ":" +
                firstMinute +
                " - " +
                secondHour +
                ":" +
                secondMinute
            )
          : firstMinute <= 9 && secondMinute <= 9
          ? $scope.timePickUpScheduling.push(
              firstHour +
                ":0" +
                firstMinute +
                " - " +
                secondHour +
                ":0" +
                secondMinute
            )
          : firstMinute <= 9 && secondMinute > 9
          ? $scope.timePickUpScheduling.push(
              firstHour +
                ":0" +
                firstMinute +
                " - " +
                secondHour +
                ":" +
                secondMinute
            )
          : firstMinute > 9 && secondMinute <= 9
          ? $scope.timePickUpScheduling.push(
              firstHour +
                ":" +
                firstMinute +
                " - " +
                secondHour +
                ":0" +
                secondMinute
            )
          : $scope.timePickUpScheduling.push(
              firstHour + ":00" + " - " + secondHour + ":00"
            );

        $scope.availablePickUpShiftsToday.forEach((firstTime, index) => {
          if (index < lengthAvailablePickUpShiftsToday - 2) {
            $scope.timePickUpScheduling.push(
              moment(firstTime).hours() +
                ":00" +
                " - " +
                moment($scope.availablePickUpShiftsToday[index + 1]).hours() +
                ":00"
            );
          }
        });

        $scope.PickUpTomorrow = () => {
          $scope.timePickUpScheduling = [];
          let date = new Date($scope.availablePickUpShiftsTomorrow[0]);
          $scope.dateDay = ("0" + date.getDate()).slice(-2);
          $scope.dateMonth = ("0" + (date.getMonth() + 1)).slice(-2);
          $scope.dateYear = date.getFullYear();
          $scope.dateFull =
            $scope.dateDay + "/" + $scope.dateMonth + "/" + $scope.dateYear;

          $scope.availablePickUpShiftsTomorrow.forEach((time, index) => {
            if (index < lengthAvailablePickUpShiftsTomorrow - 2) {
              $scope.timePickUpScheduling[index] =
                moment(time).hours() +
                ":00" +
                " - " +
                moment(
                  $scope.availablePickUpShiftsTomorrow[index + 1]
                ).hours() +
                ":00";
            }
          });
        };
      });
    };

    $scope.changePickUp = () => {
      $ionicPopup.confirm({
        title: "AVISO",
        template:
          "Você irá retirar seu pedido na loja " +
          "<strong>" +
          $scope.establishment.name +
          "</strong>",
        buttons: [
          {
            text: "Cancelar",
            type: "button-light",
            onTap: () => {
              return true;
            },
          },
          {
            text: "Confirmar",
            type: "button-positive",
            onTap: () => {
              $ionicLoading.show({
                template: RESOURCES.TEMPLATE_LOADING,
              });

              $scope.isPickUp = true;
              $scope.finishedOrder.is_pick_up = $scope.isPickUp;
              sharedProperties.setIsPickUp($scope.isPickUp);
              $ionicLoading.hide();
              $scope.closeChangePickUpModal();
              $state.go($state.current, {}, { reload: true });
            },
          },
        ],
      });
    };

    $scope.changePickUpStore = () => {
      $ionicPopup.confirm({
        title: "AVISO",
        template:
          "Ao alterar a loja de retirada seu carrinho será esvaziado. Deseja Continuar ?",
        buttons: [
          {
            text: "Cancelar",
            type: "button-light",
            onTap: () => {
              return true;
            },
          },
          {
            text: "Confirmar",
            type: "button-positive",
            onTap: () => {
              $scope.closeChangePickUpModal();
              $state.go("app.stores");
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache();
              sharedProperties.getCart().data = [];
            },
          },
        ],
      });
    };

    $scope.pickUpNow = (order) => {
      $ionicPopup.confirm({
        title: "AVISO",
        template: $scope.establishment.pick_up_message,
        buttons: [
          {
            text: "Cancelar",
            type: "button-light",
            onTap: () => {
              return true;
            },
          },
          {
            text: "Confirmar",
            type: "button-positive",
            onTap: () => {
              $scope.chooseTimePickUp = $scope.timePickUpScheduling[0];
              let fields = $scope.chooseTimePickUp.split(" - ");
              $scope.pickUpTime = fields[0];
              $scope.pickUpTimeEnd = fields[1];
              order.scheduled_pick_up = $scope.chooseTimePickUp;
              order.scheduled_pick_up_date =
                $scope.dateYear +
                "-" +
                $scope.dateMonth +
                "-" +
                $scope.dateDay +
                "T" +
                $scope.pickUpTime +
                ":00.0000";
              order.is_pick_up_express = true;
              $scope.dealOrder(order);
            },
          },
        ],
      });
    };
    $scope.favEstablishment = () => {
      if (!$scope.isFavorite) {
        $ionicPopup.confirm({
          title: "AVISO",
          template: "Deseja Favoritar este estabelecimento ? ",
          buttons: [
            {
              text: "Cancelar",
              type: "button-light",
              onTap: () => {
                return true;
              },
            },
            {
              text: "Confirmar",
              type: "button-positive",
              onTap: () => {
                var dataFavorite = {
                  establishment: $scope.store.id,
                  user: $scope.currentUser.id,
                  isFavorite: true,
                };
                ideliveryServices
                  .setFavorite(dataFavorite)
                  .then(function (response) {
                    $scope.isFavorite = response.data.data.isFavorite;

                    notie.alert(1, "Estabelecimento Favoritado com Sucesso", 3);
                  });
              },
            },
          ],
        });
      }
    };

    /////////////////////////
    // Continue Deal Order //
    /////////////////////////

    $scope.continueDealOrder = (order) => {
      console.log($scope.card, "aq");
      var cpfcnpjInput = angular.element(
        document.querySelector("#cpfcnpjInput")
      );

      var cpfcnpjInput = angular.element(
        document.querySelector("#cpfcnpjInput")
      );
      if ($scope.hasDiscount) {
        order.total = $scope.totalWithDiscount;
      }
      if (
        $scope.finishedOrder.payForm.negative_id == $scope.specie_negative_id
      ) {
        if (
          !(
            !order.payForm.specieChange ||
            order.payForm.specieChange < order.total
          )
        ) {
          if (order.receipt && order.receipt.checked) {
            if (
              cpfcnpjInput.hasClass("ng-valid") &&
              cpfcnpjInput.val() !== ""
            ) {
              sharedProperties.setFinishedOrder(order);

              $state.go("app.continueDealOrder");
            } else {
              notie.alert(3, "CPF/CNPJ inválido!", 3);
            }
          } else {
            sharedProperties.setFinishedOrder(order);
            console.log($scope.isMarketPlace);
            console.log($scope.chooseScheduled);
            console.log($scope.accept_schedule);
            $state.go("app.continueDealOrder");
          }
        } else {
          if (order.payForm.specieChange < order.total) {
            Swal.fire({
              position: "top-center",
              icon: "error",
              title: "Valor de pagamento menor que o valor total do pedido.",
              showConfirmButton: false,
              timer: 2000,
            });
          } else if (!order.payForm.specieChange) {
            Swal.fire({
              position: "top-center",
              icon: "error",
              title: "Preencha os campos obrigatórios",
              showConfirmButton: false,
              timer: 2000,
            });
          }

          // Alert se ele nao preencher o campo de troco

          document.getElementById(
            "message-alert-input-money-troco"
          ).style.display = "block";
          document.getElementById(
            "message-alert-input-money-troco"
          ).style.color = "red";
          document.getElementById(
            "message-alert-input-money-troco"
          ).style.textAlign = "left";
          document.getElementById(
            "message-alert-input-money-troco"
          ).style.padding = "1px";
        }
      } else if (
        $scope.finishedOrder.payForm.negative_id == $scope.card_negative_id
      ) {
        $scope.cpfcnpjInput = angular.element(
          document.querySelector("#cpfcnpjInput")
        );

        if (!$scope.card || $scope.card.name === null || !$scope.card.name) {
          notie.alert(2, "Por favor, selecione o cartão.", 3);
          return;
        } else {
          sharedProperties.setCpfCnpj($scope.cpfcnpjInput);

          if ($scope.finishedOrder.payForm.name == "Cartão") {
            if ($scope.card == null || $scope.card.name === null) {
              notie.alert(2, "Por favor, selecione o cartão.", 3);
              return;
            }
          } else {
            if (order.receipt && order.receipt.checked) {
              if (
                cpfcnpjInput.hasClass("ng-valid") &&
                cpfcnpjInput.val() !== ""
              ) {
                sharedProperties.setFinishedOrder(order);
                $state.go("app.continueDealOrder");
              } else {
                notie.alert(3, "CPF/CNPJ inválido!", 3);
              }
            } else {
              sharedProperties.setFinishedOrder(order);
              $state.go("app.continueDealOrder");
            }
          }
        }
      }
    };

    /////////////////////
    // Change Delivery //
    /////////////////////

    $scope.changeDelivery = () => {
      console.log($scope.establishment.EstablishmentOperation);
      $ionicPopup.confirm({
        title: "AVISO",
        template:
          "<span>" +
          "Ao alterar para receber no seu endereço o seu carrinho será esvaziado. Deseja Continuar ?" +
          "</span>",
        buttons: [
          {
            text: "Cancelar",
            type: "button-light",
            onTap: () => {
              return true;
            },
          },
          {
            text: "Confirmar",
            type: "button-positive",
            onTap: () => {
              // $scope.establishment.EstablishmentOperation
              $state.go("app.addressList");
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache();
              sharedProperties.getCart().data = [];
            },
          },
        ],
      });
    };
    ////////////////////
    // Delivery Modal //
    ////////////////////
    $ionicModal
      .fromTemplateUrl("app/payment/schedulingDeliveryModal.html", {
        scope: $scope,
        animation: "slide-in-right",
      })
      .then((modal) => {
        $scope.schedulingDeliveryModal = modal;
      });

    $scope.openSchedulingDeliveryModal = () => {
      $scope.schedulingDeliveryModal.show();
    };

    $scope.closeSchedulingDeliveryModal = () => {
      $scope.schedulingDeliveryModal.hide();
    };

    ////////////////////
    //  PickUp Modal  //
    ////////////////////
    $ionicModal
      .fromTemplateUrl("app/payment/schedulingPickUpModal.html", {
        scope: $scope,
        animation: "slide-in-right",
      })
      .then((modal) => {
        $scope.schedulingPickUpModal = modal;
      });

    $scope.openSchedulingPickUpModal = () => {
      $scope.schedulingPickUpModal.show();
    };

    $scope.closeSchedulingPickUpModal = () => {
      $scope.schedulingPickUpModal.hide();
    };

    ////////////////
    // Maps Modal //
    ////////////////
    $ionicModal
      .fromTemplateUrl("app/payment/mapsModal.html", {
        scope: $scope,
        animation: "slide-in-right",
      })
      .then((modal) => {
        $scope.mapsModal = modal;
      });

    $scope.openMapsModal = () => {
      $scope.mapsModal.show();
    };

    $scope.closeMapsModal = () => {
      $scope.mapsModal.hide();
    };

    /////////////////////////
    // Change PickUp Modal //
    /////////////////////////
    $ionicModal
      .fromTemplateUrl("app/payment/changePickUpModal.html", {
        scope: $scope,
        animation: "slide-in-right",
      })
      .then((modal) => {
        $scope.changePickUpModal = modal;
      });

    $scope.openChangePickUpModal = () => {
      $scope.changePickUpModal.show();
    };

    $scope.closeChangePickUpModal = () => {
      $scope.changePickUpModal.hide();
    };
    $scope.dealOrder = function (order) {
      console.log($scope.card, "ali");

      if ($scope.hasDiscount) {
        order.total = $scope.totalWithDiscount;
      }
      order.total.toFixed(2);

      verifyOrderForm();

      function verifyOrderForm() {
        if (
          $scope.finishedOrder.payForm.negative_id == $scope.specie_negative_id
        ) {
          if (
            !(
              !order.payForm.specieChange ||
              order.payForm.specieChange < order.total
            )
          ) {
            requestDealOrder();
          } else {
            if (order.payForm.specieChange < order.total) {
              Swal.fire({
                position: "top-center",
                icon: "error",
                title: "Valor de pagamento menor que o valor total do pedido.",
                showConfirmButton: false,
                timer: 2000,
              });
            } else if (!order.payForm.specieChange) {
              Swal.fire({
                position: "top-center",
                icon: "error",
                title: "Preencha os campos obrigatórios",
                showConfirmButton: false,
                timer: 2000,
              });
            }

            // Alert se ele nao preencher o campo de troco

            document.getElementById(
              "message-alert-input-money-troco"
            ).style.display = "block";
            document.getElementById(
              "message-alert-input-money-troco"
            ).style.color = "red";
            document.getElementById(
              "message-alert-input-money-troco"
            ).style.textAlign = "left";
            document.getElementById(
              "message-alert-input-money-troco"
            ).style.padding = "1px";

            $scope.showPopup = function () {
              $scope.specieChangeData = {};

              var template = !$scope.maskForOldAndroid
                ? '<input class="specie-change-input" type="tel" ng-model="specieChangeData.specieChange" ui-money-mask>'
                : '<input class="specie-change-input" type="tel" ng-model="specieChangeData.specieChange" money-mask money-mask-prepend="R$">';

              $ionicPopup.show({
                template: template,
                title:
                  "Troco pra quanto? (Total: R$ " +
                  $scope.finishedOrder.total.toFixed(2) +
                  ")",
                scope: $scope,
                buttons: [
                  {
                    text: "Cancelar",
                    type: "button-light",
                  },
                  {
                    text: "Finalizar Pedido",
                    type: "button-positive",
                    onTap: function (e) {
                      if ($scope.specieChangeData.specieChange >= order.total) {
                        order.payForm.specieChange =
                          $scope.specieChangeData.specieChange;
                        $scope.finishedOrder.payForm.specieChange =
                          $scope.specieChangeData.specieChange;
                        requestDealOrder();
                      } else {
                        notie.alert(
                          2,
                          "Valor de pagamento menor que o valor total do pedido.",
                          5
                        );
                      }
                    },
                  },
                ],
              });
            };
          }
        } else if (
          $scope.finishedOrder.payForm.negative_id == $scope.card_negative_id
        ) {
          $scope.cpfcnpjInput = angular.element(
            document.querySelector("#cpfcnpjInput")
          );

          if (!$scope.card || $scope.card.name === null || !$scope.card.name) {
            notie.alert(2, "Por favor, selecione o cartão.", 3);
            return;
          } else {
            sharedProperties.setCpfCnpj($scope.cpfcnpjInput);

            if ($scope.finishedOrder.payForm.name == "Cartão") {
              if ($scope.card == null || $scope.card.name === null) {
                notie.alert(2, "Por favor, selecione o cartão.", 3);
                return;
              }
            } else {
              if (order.receipt && order.receipt.checked) {
                if (
                  cpfcnpjInput.hasClass("ng-valid") &&
                  cpfcnpjInput.val() !== ""
                ) {
                  requestDealOrder();
                } else {
                  notie.alert(3, "CPF/CNPJ inválido!", 3);
                }
              } else {
                requestDealOrder();
              }
            }
          }
        }
      }

      function requestDealOrder() {
        if (order.total >= $scope.store.min_order_value) {
          order.orderJson = JSON.stringify($scope.finishedOrder);
          order.orderJson.delivery_address = $scope.address;

          ideliveryServices.dealOrder(order).then(
            function (response) {
              $scope.orderResponse = response.data;

              if (response.data.success) {
                sharedProperties.getCart().data = [];
                sharedProperties.setSelectedCard("");
                $scope.finishedOrder.payForm.specieChange = "";

                $state.go("app.products");

                notie.alert(1, response.data.message, 3);
                if (
                  !$scope.isFavorite &&
                  configIDL.ESTABLISHMENT_SLUG == "betapizzaria.deway.com.br"
                ) {
                  $scope.favEstablishment();
                }
              } else {
                notie.alert(3, response.data.message, 3);
                if (
                  !$scope.isFavorite &&
                  configIDL.ESTABLISHMENT_SLUG == "betapizzaria.deway.com.br"
                ) {
                  $scope.favEstablishment();
                }
              }
            },
            function () {
              notie.alert(3, "Falha ao realizar o pedido. Tente novamente", 3);
            }
          );
        } else {
          notie.alert(
            2,
            "Total do pedido menor que o valor mínimo de pedido (R$" +
              $scope.store.min_order_value +
              ").",
            5
          );
        }
      }
    };

    $scope.changeOrder = function () {
      $state.go("app.products");
    };

    /////////////////
    // Cards Modal //
    /////////////////

    $scope.cards = [];

    $ionicModal
      .fromTemplateUrl("app/payment/cardsModal.html", {
        scope: $scope,
        animation: "slide-in-right",
      })
      .then(function (modal) {
        $scope.cardsModal = modal;
      });

    $scope.closeCardsModal = function (chosenCard) {
      $scope.cardsModal.hide();
      if (chosenCard) {
        $scope.card = chosenCard;
        sharedProperties.setSelectedCard(chosenCard);
        $scope.finishedOrder.payForm = chosenCard;
        $scope.finishedOrder.payForm.negative_id = $scope.card_negative_id;
      }
    };

    $scope.openCardsModal = function () {
      ideliveryServices.getCards().then(function (response) {
        $scope.cards = response.data;
        $scope.cardsModal.show();
      });
    };

    // Cleaning up modals on destroy
    $scope.$on("$destroy", function () {
      $scope.cardsModal.remove();
    });

    ///////////////////////
    //      DISCOUNT     //
    ///////////////////////

    let selectCardPopup = function () {
      $ionicPopup.show({
        title: "Por favor, selecione primeiramente o cartão.",
        scope: $scope,
        buttons: [
          {
            text: "OK",
            type: "button-positive",
          },
        ],
      });
    };

    let InvalidDiscount = function () {
      $ionicPopup.show({
        title: "Cupom Inserido Inválido",
        scope: $scope,
        buttons: [
          {
            text: "OK",
            type: "button-positive",
          },
        ],
      });
    };

    let calculateOrderWithDiscount = function (data) {
      if ($scope.hasDiscount)
        $scope.finishedOrder.total =
          $scope.finishedOrder.total + $scope.appliedDiscount.amountDiscounted;

      $scope.appliedDiscount = {
        discountCode: data.code,
      };
      $scope.finishedOrder.discountCode = data.code;
      $scope.hasDiscount = true;

      if (data.tag == 2)
        $scope.appliedDiscount.amountDiscounted =
          $scope.finishedOrder.subtotal * (data.percentage / 100);
      else if (data.tag == 1) {
        $scope.appliedDiscount.amountDiscounted = data.value;
        if (data.value > $scope.finishedOrder.total)
          $scope.appliedDiscount.amountDiscounted = $scope.finishedOrder.total;
      } else if (data.tag == 3)
        $scope.appliedDiscount.amountDiscounted =
          $scope.finishedOrder.freightCost;

      $scope.subtotalWithoutDiscount = $scope.finishedOrder.subtotal;
      $scope.finishedOrder.total =
        $scope.finishedOrder.subtotal + $scope.finishedOrder.freightCost;
      $scope.totalWithDiscount =
        $scope.finishedOrder.subtotal -
        $scope.appliedDiscount.amountDiscounted +
        $scope.finishedOrder.freightCost;
      $scope.finishedOrder.totalChange =
        $scope.finishedOrder.payForm.specieChange -
        ($scope.finishedOrder.total - $scope.appliedDiscount.amountDiscounted);
    };

    let validateDiscount = function (discount) {
      console.log("bateu aq");
      discount.user = $scope.currentUser.id;
      ideliveryServices
        .validateDiscountCode(discount)
        .then(function (response) {
          if (response.data.success) {
            calculateOrderWithDiscount(response.data.data);
            $scope.hasDiscountCode = true;
          } else {
            InvalidDiscount();
          }
        });
    };

    $scope.openDiscountPopup = function (title) {
      // No card selected
      if (
        $scope.finishedOrder.payForm.negative_id === $scope.card_negative_id &&
        !$scope.card
      ) {
        selectCardPopup();
      }

      let discount = {
        establishment_id: configIDL.ESTABLISHMENT_ID,
        discountCode: title.discountCode,
      };
      validateDiscount(discount);
    };
  }
})();
