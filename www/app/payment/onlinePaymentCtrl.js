(function () {
  angular
    .module("idelivery")
    .controller("onlinePaymentCtrl", onlinePaymentCtrl);

  onlinePaymentCtrl.$inject = [
    "$ionicLoading",
    "$interval",
    "ideliverySearchAddressService",
    "$ionicHistory",
    "RESOURCES",
    "$ionicLoading",
    "$rootScope",
    "$scope",
    "ideliveryServices",
    "sharedProperties",
    "$state",
    "$ionicModal",
    "$ionicPopup",
  ];

  function onlinePaymentCtrl(
    $ionicLoading,
    $interval,
    ideliverySearchAddressService,
    $ionicHistory,
    RESOURCES,
    $ionicLoading,
    $rootScope,
    $scope,
    ideliveryServices,
    sharedProperties,
    $state,
    $ionicModal,
    $ionicPopup
  ) {
    $scope.specieChangeAltered = function () {
      $scope.finishedOrder.totalChange =
        $scope.finishedOrder.payForm.specieChange - $scope.finishedOrder.total;
    };

    if (configIDL.OPTIONAL_RECEIPT != 2) {
      if (configIDL.OPTIONAL_RECEIPT == 3) $scope.showRequiredReceipt = true;
      else $scope.showReceipt = true;
    }

    $scope.commentsText = configIDL.COMMENTS_TEXT;

    $scope.$on("$ionicView.beforeEnter", function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      var currentDevicePlatform = {
        name: ionic.Platform.platform(),
        version: ionic.Platform.version(),
      };

      $scope.maskForOldAndroid =
        currentDevicePlatform.name === "android" &&
        currentDevicePlatform.version <= 4.3;
      $scope.selectCardFlag = false;
      $scope.store = JSON.parse(Util.getLocalSelectedStore());

      $scope.currentUser = JSON.parse(Util.getCurrentUserData());
      ideliveryServices
        .getUserProfile($scope.currentUser.id)
        .then(function (response) {
          var address = JSON.parse(
            localStorage.getItem(
              "deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG
            )
          );
          $scope.establishment = JSON.parse(
            localStorage.getItem(
              "store_selected-" + configIDL.ESTABLISHMENT_SLUG
            )
          );
          console.log($scope.establishment);

          if (address) {
            $scope.sender = {
              establishment_id: $scope.establishment.id,
              name: response.data.full_name,
              user_id: $scope.currentUser.id,
              cpf: null,
              email: response.data.email,
              area_code: response.data.phone.slice(0, 2),
              phone: null,
              birth_date: null,
              city: address.city,
              state: address.state,
              number: address.number,
              street: address.street,
              zipcode: address.cep.replace(/"-"/g, ""),
              state_collection: null,
              city_collection: null,
              external_id_customer: null,
              country_collection: "br",
              type_collection: "individual",
              neighborhood: address.neighborhood.id,
              neighborhood_collection: null,
              street_collection: null,
              street_number_collection: null,
              zipcode_collection: null,
              card_number_collection: null,
              card_holder_name: null,
              card_cvv: null,
              card_expiration_date: null,
              card_hash: null,
              card_id: null,
              flag_to_store_card: 0,
              transaction: null,
              gateway: $scope.establishment.slug.paymentGateway,
            };
            $scope.form = {
              birth: null,
              expiration: null,
              phone: $scope.sender.area_code + $scope.sender.phone,
            };
            $scope.online_card = {
              img: null,
              number: null,
            };
            $scope.online = {
              number: null,
              brand: null,
              cvv: null,
              confirmcvv: null,
            };
          }
        });

      $scope.finishedOrder = sharedProperties.getFinishedOrder().data;
      $scope.finishedOrder.establishment_id = configIDL.ESTABLISHMENT_ID;
      $scope.finishedOrder.online = {
        hash: null,
        cardtoken: null,
      };

      ideliveryServices
        .calculateOrder($scope.finishedOrder)
        .then(function (response) {
          var itemsTotal = 0;
          for (var i = 0; i < response.data.items.length; i++) {
            $scope.finishedOrder.items[i].totalOrder =
              response.data.items[i].totalOrder;
            itemsTotal += $scope.finishedOrder.items[i].totalOrder;
          }
          $scope.finishedOrder.freightCost = response.data.freightCost;
          $scope.discount = response.data.discount;
          $scope.finishedOrder.subtotal = itemsTotal;

          if ($scope.discount) {
            $scope.discountValue =
              $scope.finishedOrder.subtotal * $scope.discount;
            $scope.finishedOrder.total =
              response.data.total - $scope.discountValue;
          } else {
            $scope.finishedOrder.total = response.data.total;
          }
        });

      $scope.online_negative_id = configIDL.PAY_TYPES[2].negative_id;
      ideliveryServices.getPaymentTypes().then(function (response) {
        $scope.paymentForms = response.data;
      });

      ideliveryServices.getAcceptedPaymentTypes().then(function (response) {
        $ionicLoading.hide();
        $scope.acceptedPaymentTypes = response.data;
      });

      // Delivery address

      $scope.address = JSON.parse(
        localStorage.getItem("deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG)
      );

      $scope.address.complement
        ? ($scope.complement = "(" + $scope.address.complement + ")")
        : ($scope.complement = "");

      $scope.finishedOrder.delivery_address = $scope.address.id;
    });

    $scope.dealOrder = function (order) {
      if (
        $scope.online.number == null &&
        $scope.finishedOrder.payForm.negative_id == -3
      ) {
        notie.alert(2, "Por favor, selecione o cartão.", 3);
      } else {
        order.total.toFixed(2);
        var cpfcnpjInput = angular.element(
          document.querySelector("#cpfcnpjInput")
        );

        if (order.receipt && order.receipt.checked) {
          if (cpfcnpjInput.hasClass("ng-valid") && cpfcnpjInput.val() !== "") {
            verifyOrderForm();
          } else {
            notie.alert(3, "CPF/CNPJ inválido!", 3);
          }
        } else {
          verifyOrderForm();
        }

        function verifyOrderForm() {
          if (
            $scope.finishedOrder.payForm.negative_id ==
            $scope.online_negative_id
          ) {
            if ($scope.selectCardFlag == false) {
              if (checkOnlineData()) {
                $scope.finishedOrder.sender = $scope.sender;
                showPopup();
              }
            } else {
              $scope.finishedOrder.sender = $scope.sender;
              showPopup();
            }
          }
        }

        function requestDealOrder() {
          if (order.subtotal >= $scope.store.min_order_value) {
            order.orderJson = JSON.stringify($scope.finishedOrder);
            order.orderJson.delivery_address = $scope.address;
            $ionicLoading.show({
              template: RESOURCES.TEMPLATE_LOADING,
            });

            ideliveryServices.dealOrder(order).then(
              function (response) {
                $scope.orderResponse = response.data;
                if ($scope.orderResponse.data.type_of_payment == -3) {
                  console.log(response);
                  if (response.data.success) {
                    if ($scope.sender.gateway == 1) {
                      function getUpdatePagarme() {
                        $ionicLoading.show({
                          template: RESOURCES.TEMPLATE_LOADING,
                        });

                        ideliveryServices
                          .getUpdatePagarme(
                            $scope.orderResponse.data.transaction_id,
                            $scope.establishment.id
                          )
                          .then(function (resp) {
                            console.log(resp);

                            if (resp.data == "processing") {
                              getUpdatePagarme();
                            } else if (resp.data == "paid") {
                              sharedProperties.getCart().data = [];
                              $scope.finishedOrder.payForm.specieChange = "";
                              $ionicHistory.nextViewOptions({
                                disableBack: true,
                              });
                              if (
                                $scope.orderResponse.data.card_already_stored ==
                                false
                              ) {
                                localStorage.setItem(
                                  "display_alert_of_order_information",
                                  1
                                );
                                $state.go("app.orders");
                              } else {
                                $ionicPopup.alert({
                                  title: "Este cartão já foi armazenado",
                                  buttons: [
                                    {
                                      text: "Ok",
                                      type: "button-positive",
                                    },
                                  ],
                                });
                                $scope.confirmOnline();
                              }
                            } else {
                              $ionicPopup.alert({
                                title: "Transação recusada, não autorizada.",
                                buttons: [
                                  {
                                    text: "Ok",
                                    type: "button-positive",
                                  },
                                ],
                              });
                              $scope.setCard();
                            }
                          });
                      }
                      getUpdatePagarme();
                    } else {
                      sharedProperties.getCart().data = [];
                      $scope.finishedOrder.payForm.specieChange = "";
                      $ionicHistory.nextViewOptions({
                        disableBack: true,
                      });
                      if (
                        $scope.orderResponse.data.card_already_stored == false
                      ) {
                        localStorage.setItem(
                          "display_alert_of_order_information",
                          1
                        );
                        $state.go("app.orders");
                      } else {
                        $ionicPopup.alert({
                          title: "Este cartão já foi armazenado",
                          buttons: [
                            {
                              text: "Ok",
                              type: "button-positive",
                            },
                          ],
                        });
                        $scope.confirmOnline();
                      }
                    }
                  } else {
                    if ($scope.sender.gateway == "1") {
                      if (response.data.message[0].type) {
                        notie.alert(
                          3,
                          $scope.ErrorMessages(
                            response.data.message[0].parameter_name
                          ),
                          6
                        );
                      }
                      $scope.confirmOnline();
                    } else {
                      if (typeof response.data.message === "number") {
                        notie.alert(
                          3,
                          $scope.ErrorMessagesCielo[
                            response.data.message.toString()
                          ]["message"],
                          5
                        );
                      } else {
                        notie.alert(3, response.data.message, 5);
                      }
                    }
                  }
                }
              },
              function () {
                notie.alert(
                  3,
                  "Falha ao realizar o pedido. Tente novamente",
                  3
                );
              }
            );
          } else {
            notie.alert(
              2,
              "Total do pedido menor que o valor mínimo de pedido (R$" +
                $scope.store.min_order_value +
                ").",
              5
            );
          }
        }
        function showPopup() {
          $scope.cpf_confirm = {};

          var template =
            '<input class="input-card-little" type="tel" id="cvv" ng-model="online.confirmcvv"  ui-mask="999" style="padding: 0px 10px 0px 100px;">';
          if ($scope.sender.card_id) {
            if ($scope.finishedOrder.total < 10000) {
              requestDealOrder();
            } else {
              notie.alert(
                2,
                "Compra Onlines com valor acima de 10.000,00 Reais não são aceitas.",
                5
              );
            }
          } else {
            $ionicPopup.show({
              template: template,
              title: "Por favor confirme o cvv usado no Cartão",
              scope: $scope,
              cssClass: 'modal-cvv-online',
              buttons: [
                {
                  text: "Cancelar",
                  type: "button-light",
                },
                {
                  text: "Finalizar Pedido",
                  type: "button-positive",
                  onTap: function (e) {
                    console.log($scope.sender);
                    $scope.sender.card_cvv = $scope.online.cvv;
                    console.log($scope.online.cvv);
                    if ($scope.finishedOrder.total < 10000) {
                      if ($scope.online.cvv == $scope.online.confirmcvv) {
                        requestDealOrder();
                      } else {
                        notie.alert(2, "CVV não confere", 5);
                      }
                    } else {
                      notie.alert(
                        2,
                        "Compra Onlines com valor acima de 10.000,00 Reais não são aceitas.",
                        5
                      );
                    }
                  },
                },
              ],
            });
          }
        }
      }
    };

    $scope.ErrorMessages = function (data) {
      console.log(data);
      $scope.erros = {
        amount: "Valor mínimo de compra R$ 1,00",
        customer:
          "Há algo errado nos seus dados pessoais, por favor verifique e tente novamente, caso o erro persista entre em contato com administradora do cartão",
        billing:
          "Há algo errado nos seus dados pessoais, por favor verifique e tente novamente , caso o erro persista entre em contato com administradora do cartão",
        shipping:
          "Há algo errado nos seus dados pessoais, por favor verifique e tente novamente , caso o erro persista entre em contato com administradora do cartão",
      };
      return $scope.erros[data];
    };

    $scope.openScheduleOptions = function () {
      $state.go("app.scheduleForm");
    };

    $scope.changeOrder = function () {
      $state.go("app.products");
    };

    ///////////////////
    // Online Modal //
    //////////////////

    $scope.cards = [];
    $scope.addCard = false;

    $scope.openCardsModal = function () {
      ideliveryServices.getCards().then(function (response) {
        $scope.cards = response.data;
        $scope.cardsModal.show();
      });
    };

    $ionicModal
      .fromTemplateUrl("app/payment/onlineModal.html", {
        scope: $scope,
        animation: "slide-in-right",
      })
      .then(function (modal) {
        $scope.onlineModal = modal;
      });

    $scope.showOnlineButton = true;

    $scope.closeOnlineModal = function () {
      if ($scope.showOnlineButton) {
        $scope.online.number = null;
        $scope.online.cvv = null;
        $scope.online.brand = null;
        $scope.online_card.img = null;
      }
      $scope.onlineModal.hide();
    };

    $scope.openOnlineModal = function () {
      $scope.fetchStoredCards($scope.currentUser.id);
      $scope.onlineModal.show();
    };

    function srtCardNum() {
      var numberCard = $scope.online.number.toString();
      var size = numberCard.length;
      var str = "";
      for (var i = 0; i < size - 4; i++) {
        str += "*";
      }
      var number = numberCard.substring(size - 4, size);
      $scope.online_card.number = null;
      $scope.online_card.number = str + number;
    }

    function checkOnlineData() {
      var name = $scope.sender.name.split(" ");
      if (
        checkIfValueIsEmptyOrNull($scope.sender.name) ||
        checkIfValueIsEmptyOrNull($scope.sender.phone) ||
        checkIfValueIsEmptyOrNull($scope.sender.email) ||
        checkIfValueIsEmptyOrNull($scope.sender.cpf) ||
        checkIfValueIsEmptyOrNull($scope.sender.street_collection) ||
        checkIfValueIsEmptyOrNull($scope.sender.street_number_collection) ||
        checkIfValueIsEmptyOrNull($scope.sender.zipcode_collection) ||
        checkIfValueIsEmptyOrNull($scope.sender.country_collection) ||
        checkIfValueIsEmptyOrNull($scope.sender.state_collection) ||
        checkIfValueIsEmptyOrNull($scope.sender.city_collection) ||
        checkIfValueIsEmptyOrNull($scope.online.number) ||
        checkIfValueIsEmptyOrNull($scope.form.expiration) ||
        checkIfValueIsEmptyOrNull($scope.online.cvv)
      ) {
        notie.alert(2, "Todos os campos são obrigatórios.", 3);
      }
      //else if (!$scope.sender.cpf)
      //  notie.alert(3, 'CPF incorreto.', 3)
      else if (!$scope.online.number) {
        notie.alert(3, "Número de cartão inválido.", 3);
      } else if (name.length < 2) {
        notie.alert(3, "Insira o nome completo.", 3);
      } else if (!$scope.online.brand) {
        notie.alert(3, "Bandeira de cartão não aceita.", 3);
      } else if (!$scope.cardPagarme.card_cvv) {
        notie.alert(3, "CVV inválido.", 3);
      } else if (!$scope.cardPagarme.card_expiration_date) {
        notie.alert(3, "Data de expiração inválida.", 3);
      } else {
        return true;
      }
      return false;
    }

    function checkIfValueIsEmptyOrNull(data) {
      if (data == null || data.length == 0) {
        return true;
      }
      return false;
    }

    $scope.confirmOnline = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      $scope.cardPagarme = {};
      $scope.cardPagarme.card_number =
        $scope.online.number != null
          ? $scope.online.number.toString().trim()
          : "";
      $scope.cardPagarme.card_holder_name =
        $scope.sender.name != null ? $scope.sender.name : "";
      $scope.cardPagarme.card_cvv =
        $scope.online.cvv != null ? $scope.online.cvv : "000";
      $scope.cardPagarme.card_expiration_date =
        $scope.form.expiration != null
          ? $scope.form.expiration.slice(0, 2) + $scope.form.expiration.slice(2)
          : "0000";

      var cardValidations = pagarme.validate({ card: $scope.cardPagarme });
      if (!cardValidations.card.card_number) {
        $scope.online.number = cardValidations.card.card_number;
      } else if (!cardValidations.card.brand) {
        $scope.online.brand = cardValidations.card.brand;
      } else if (!cardValidations.card.card_cvv) {
        $scope.cardPagarme.card_cvv = cardValidations.card.card_cvv;
      } else if (!cardValidations.card.card_expiration_date) {
        $scope.cardPagarme.card_expiration_date =
          cardValidations.card.card_expiration_date;
      }

      if (checkOnlineData()) {
        $scope.setCard = function () {
          $scope.sender.card_number = $scope.cardPagarme.card_number;
          $scope.sender.cpf = $scope.sender.cpf.replace(/"."/g, "");
          $scope.sender.phone = $scope.sender.phone.replace(/"."/g, "");
          $scope.sender.card_holder_name = $scope.cardPagarme.card_holder_name;
          $scope.sender.card_cvv = $scope.cardPagarme.card_cvv;
          $scope.sender.card_expiration_date =
            $scope.cardPagarme.card_expiration_date;
          $scope.sender.card_id = null;
          //Mas caso esteja tudo certo, você pode seguir o fluxo
          console.log($scope.establishment.key_app_pagarme);
          console.log($scope.sender.gateway);
          if ($scope.sender.gateway == "1") {
            pagarme.client
              .connect({ encryption_key: $scope.establishment.key_app_pagarme })
              .then(function (client) {
                $ionicLoading.hide();
                return client.security.encrypt($scope.cardPagarme);
              })
              .then(function (card_hash) {
                $scope.sender.card_hash = card_hash;
                console.log($scope.sender.card_hash);
                $scope.showOnlineButton = false;
                srtCardNum();
                setTimeout(function () {
                  $ionicLoading.hide();
                }, 1000);

                $scope.onlineModal.hide();
              });
          } else {
            $scope.showOnlineButton = false;
            $scope.sender.card_hash = null;

            setTimeout(function () {
              $ionicLoading.hide();
            }, 1000);
            $scope.onlineModal.hide();
            srtCardNum();
          }
        };
        $scope.setCard();
        setTimeout(function () {
          $ionicLoading.hide();
        }, 1000);
        return false;
      }
    };

    $scope.fetchStoredCards = function (user) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      ideliveryServices.fetchStoredCards(user).then(function (response) {
        $scope.storedCards = response.data;
        $ionicLoading.hide();

        if ($scope.addCard == false) {
          if (response.data.length == 0) {
            $scope.flagCard = true;
          } else {
            $scope.flagCard = false;
          }
        } else {
          $scope.flagCard = true;
        }
      });
    };

    $scope.brandDetect = function () {
      var numCard =
        $scope.online.number != null
          ? $scope.online.number.replace(/" "/g, "")
          : $scope.online.number;

      var amex_regex = new RegExp("^3[47][0-9]{0,}$");
      var aura_regex = new RegExp("^50[0-9]{0,}$");
      var diners_regex = new RegExp("^3(?:0[0-59]{1}|[689])[0-9]{0,}$");
      var elo_regex = new RegExp(
        "^(4(0117[89]|3(1274|8935)|5(1416|7(393|63[12])))|50(4175|6(699|7([0-6]d|7[0-8]))|9d{3})|6(27780|36(297|368)|5(0(0(3[1-35-9]|4d|5[01])|4(0[5-9]|([1-3]d|8[5-9]|9d))|5([0-2]d|3[0-8]|4[1-9]|[5-8]d|9[0-8])|7(0d|1[0-8]|2[0-7])|9(0[1-9]|[1-6]d|7[0-8]))|16(5[2-9]|[67]d)|50([01]d|2[1-9]|[34]d|5[0-8]))))[0-9]{0,}$"
      );
      var hipercard_regex = new RegExp("^(38|60)[0-9]{0,}$");
      var mastercard_regex = new RegExp(
        "^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$"
      );
      var visa_regex = new RegExp("^4[0-9]{0,}$");

      if (elo_regex.test(numCard)) {
        $scope.online.brand = "elo";
      } else if (hipercard_regex.test(numCard)) {
        $scope.online.brand = "hipercard";
      } else if (amex_regex.test(numCard)) {
        $scope.online.brand = "amex";
      } else if (aura_regex.test(numCard)) {
        $scope.online.brand = "aura";
      } else if (diners_regex.test(numCard)) {
        $scope.online.brand = "diners";
      } else if (mastercard_regex.test(numCard)) {
        $scope.online.brand = "mastercard";
      } else if (visa_regex.test(numCard)) {
        $scope.online.brand = "visa";
      } else {
        $scope.online.brand = null;
      }
      $scope.online_card.img =
        "https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/" +
        $scope.online.brand +
        ".png";
    };

    $scope.brandDetectStoredcard = function (data) {
      var brandStored = data;
      $scope.onlineCardStored =
        "https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/" +
        brandStored +
        ".png";
      return $scope.onlineCardStored;
    };

    $scope.setNewCard = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      setTimeout(function () {
        $scope.flagCard = true;
        $scope.onlineModal.show();
        $ionicLoading.hide();
      }, 300);
    };

    $scope.selectCheckbox = function () {
      $scope.reason.selected = !$scope.reason.selected;
      $scope.sender.flag_to_store_card = $scope.reason.selected == true ? 1 : 0;
    };

    $scope.reason = {
      id: 0,
      selected: false,
    };

    $scope.selectCard = function (card) {
      $scope.selectCardFlag = true;
      $scope.showOnlineButton = false;
      $scope.sender.card_id = card.card;
      $scope.sender.cpf = card.cpf;
      $scope.sender.transaction = card.transaction;
      $scope.sender.card_hash = null;
      var str = "";
      for (var i = 0; i < 16 - 4; i++) str += "*";
      $scope.online_card.number = str + card.last_numbers;
      $scope.online_card.img = $scope.brandDetectStoredcard(card.brand);
      $scope.online.number = true;
      $scope.sender.flag_to_store_card = 0;
      $scope.onlineModal.hide();
    };

    $scope.opendeliveryAddressModal = function () {
      ideliverySearchAddressService
        .getAddressByEstablishment($scope.store.id)
        .then(function (response) {
          $scope.userAddress = response.data;
          $scope.deliveryAddressModal.show();
        });
    };

    $ionicModal
      .fromTemplateUrl("app/cart/deliveryAddressModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.deliveryAddressModal = modal;
      });

    $scope.setNewAddress = function () {
      $scope.deliveryAddressModal.hide();
      localStorage.setItem(
        "setCartDeliveryAddress-" + configIDL.ESTABLISHMENT_SLUG,
        1
      );
      $state.go("app.address");
    };

    $scope.closedeliveryAddressModal = function (chosenAddress) {
      $scope.deliveryAddressModal.hide();
      if (chosenAddress) {
        localStorage.setItem(
          "deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG,
          JSON.stringify(chosenAddress)
        );
        ideliverySearchAddressService
          .getNeighborhoodFreight(
            chosenAddress.neighborhood.city.id,
            chosenAddress.neighborhood.id
          )
          .then(function (response) {
            var newFreightValue = response.data.price_shipping;
            $scope.address.street = chosenAddress.street;
            $scope.address.number = chosenAddress.number;
            $scope.address.neighborhood = chosenAddress.neighborhood;
            $scope.address.complement = chosenAddress.complement;
            $scope.finishedOrder.delivery_address = chosenAddress.id;
            $scope.recalculateTotalOrder(newFreightValue);
          });
      }
    };

    $scope.recalculateTotalOrder = function (newFreightValue) {
      $scope.finishedOrder.total -= $scope.finishedOrder.freightCost;
      $scope.finishedOrder.total += newFreightValue;
      $scope.finishedOrder.freightCost = newFreightValue;
    };

    $scope.stringLowercaseState = function () {
      $scope.sender.state_collection = $scope.sender.state_collection.toLowerCase();
    };

    $scope.stringLowercaseCountry = function () {
      $scope.sender.country_collection = $scope.sender.country_collection.toLowerCase();
    };

    $scope.$on("$destroy", function () {
      $interval.cancel($scope.pagarmeGetter);
    });

    $scope.ErrorMessagesCielo = {
      0: {
        message: "Erro interno",
      },
      100: {
        message: "RequestId é obrigatório",
      },
      101: {
        message: "MerchantId é obrigatório",
      },
      102: {
        message: "Tipo de pagamento é obrigatório",
      },
      103: {
        message: "O tipo de pagamento só pode conter letras",
      },
      104: {
        message: "A identidade do cliente é obrigatória",
      },
      105: {
        message: "O nome do cliente é obrigatório",
      },
      106: {
        message: "O ID da transação é obrigatório",
      },
      107: {
        message: "OrderId é inválido ou não existe",
      },
      108: {
        message: "O valor deve ser maior ou igual a zero",
      },
      109: {
        message: "Moeda de pagamento é necessária",
      },
      110: {
        message: "Moeda de Pagamento Inválido",
      },
      111: {
        message: "Pagamento País é obrigatório",
      },
      112: {
        message: "País de pagamento inválido",
      },
      113: {
        message: "Código de pagamento inválido",
      },
      114: {
        message: "O MerchantId fornecido não está no formato correto",
      },
      115: {
        message: "O MerchantId fornecido não foi encontrado",
      },
      116: {
        message: "O MerchantId fornecido está bloqueado",
      },
      117: {
        message: "Titular do cartão de crédito é obrigatório",
      },
      118: {
        message: "Número do cartão de crédito é obrigatório",
      },
      119: {
        message: "Pelo menos um pagamento é necessário",
      },
      120: {
        message: "Solicitar IP não permitido. Verifique sua lista branca de IP",
      },
      121: {
        message: "O cliente é necessário",
      },
      122: {
        message: "MerchantOrderId é obrigatório",
      },
      123: {
        message: "As parcelas devem ser maiores ou iguais a um",
      },
      124: {
        message: "Cartão de crédito é obrigatório",
      },
      125: {
        message: "A data de vencimento do cartão de crédito é obrigatória",
      },
      126: {
        message: "A data de expiração do cartão de crédito é inválida",
      },
      127: {
        message: "Você deve fornecer o número do cartão de crédito",
      },
      128: {
        message: "Comprimento do número do cartão excedido",
      },
      129: {
        message: "Afiliação não encontrada",
      },
      130: {
        message: "Não foi possível obter o cartão de crédito",
      },
      131: {
        message: "MerchantKey é obrigatório",
      },
      132: {
        message: "MerchantKey é inválido",
      },
      133: {
        message: "O provedor não é compatível com este tipo de pagamento",
      },
      134: {
        message: "Comprimento da impressão digital excedido",
      },
      135: {
        message: "Comprimento de MerchantDefinedFieldValue excedido",
      },
      136: {
        message: "O tamanho do ItemDataName foi excedido",
      },
      137: {
        message: "Tamanho do ItemDataSKU excedido",
      },
      138: {
        message: "O tamanho do PassengerDataName foi excedido",
      },
      139: {
        message: "PassengerDataStatus length exceeded",
      },
      140: {
        message: "PassengerDataEmail length exceded",
      },
      141: {
        message: "Comprimento do PassengerDataPhone excedido",
      },
      142: {
        message: "Tamanho de TravelDataRoute excedido",
      },
      143: {
        message: "Duração de TravelDataJourneyType excedida",
      },
      144: {
        message: "Comprimento de TravelLegDataDestination excedido",
      },
      145: {
        message: "Comprimento de TravelLegDataOrigin excedido",
      },
      146: {
        message: "Tamanho do SecurityCode excedido",
      },
      147: {
        message: "Endereço Comprimento da rua excedido",
      },
      148: {
        message: "Comprimento do número de endereço excedido",
      },
      149: {
        message: "Endereço Comprimento do complemento excedido",
      },
      150: {
        message: "Endereço ZipCode length exceeded",
      },
      151: {
        message: "Endereço Comprimento da cidade excedido",
      },
      152: {
        message: "Tamanho do estado do endereço excedido",
      },
      153: {
        message: "Endereço País excedido",
      },
      154: {
        message: "Endereço Comprimento do distrito excedido",
      },
      155: {
        message: "Comprimento do nome do cliente excedido",
      },
      156: {
        message: "Comprimento da identidade do cliente excedido",
      },
      157: {
        message: "Comprimento da Identidade do Cliente excedido",
      },
      158: {
        message: "Comprimento do email do cliente excedido",
      },
      159: {
        message: "ExtraData Name length exceeded",
      },
      160: {
        message: "Comprimento do valor extraData excedido",
      },
      161: {
        message: "Boleto Instructions length exceeded",
      },
      162: {
        message: "Boleto Demostrative length exceeded",
      },
      163: {
        message: "O URL de retorno é obrigatório",
      },
      166: {
        message: "AuthorizeNow é obrigatório",
      },
      167: {
        message: "Antifraude não configurado",
      },
      168: {
        message: "Pagamento Recorrente não encontrado",
      },
      169: {
        message: "Pagamento Recorrente não está ativo",
      },
      170: {
        message: "Cartão Protegido not configured",
      },
      171: {
        message: "Dados de afiliação não enviados",
      },
      172: {
        message: "Código de Credencial é obrigatório",
      },
      173: {
        message: "O método de pagamento não está ativado",
      },
      174: {
        message: "O número do cartão é obrigatório",
      },
      175: {
        message: "EAN é obrigatório",
      },
      176: {
        message: "Moeda de pagamento não é suportada",
      },
      177: {
        message: "O número do cartão é inválido",
      },
      178: {
        message: "EAN é inválido",
      },
      179: {
        message:
          "O número máximo de parcelas permitidas para pagamento recorrente é 1",
      },
      180: {
        message: "O cartão fornecido PaymentToken não foi encontrado",
      },
      181: {
        message: "O MerchantIdJustClick não está configurado",
      },
      182: {
        message: "Marca é necessária",
      },
      183: {
        message: "Cliente inválido bithdate",
      },
      184: {
        message: "O pedido não pode estar vazio",
      },
      185: {
        message: "A marca não é suportada pelo provedor selecionado",
      },
      186: {
        message:
          "O provedor selecionado não suporta as opções fornecidas (Capture, Authenticate, Recurrent ou Installments)",
      },
      187: {
        message: "Coleção ExtraData contém um ou mais nomes duplicados",
      },
      188: {
        message: "Avs com CPF inválido",
      },
      189: {
        message: "Avs com comprimento de rua excedido",
      },
      190: {
        message: "Avs com o comprimento do número excedido",
      },
      191: {
        message: "Avs com duração do distrito excedido",
      },
      192: {
        message: "Avs com CEP inválido",
      },
      193: {
        message: "O valor da divisão deve ser maior que zero",
      },
      194: {
        message: "Estabelecimento dividido é necessário",
      },
      195: {
        message: "O PlataformId é obrigatório",
      },
      196: {
        message: "DeliveryAddress é obrigatório",
      },
      197: {
        message: "Rua é obrigatória",
      },
      198: {
        message: "Número é obrigatório",
      },
      199: {
        message: "ZipCode é obrigatório",
      },
      200: {
        message: "Cidade é obrigatória",
      },
      201: {
        message: "Estado é obrigatório",
      },
      202: {
        message: "Distrito é obrigatório",
      },
      203: {
        message: "O item do carrinho Nome é obrigatório",
      },
      204: {
        message: "Item do carrinho Quantidade é necessária",
      },
      205: {
        message: "O tipo de item do carrinho é obrigatório",
      },
      206: {
        message: "Comprimento do nome do item do carrinho excedido",
      },
      207: {
        message: "Comprimento da descrição do item do carrinho excedido",
      },
      208: {
        message: "Tamanho do sku do item do carrinho excedido",
      },
      209: {
        message: "O comprimento do sku do destinatário de envio foi excedido",
      },
      210: {
        message: "Os dados de envio não podem ser nulos",
      },
      211: {
        message: "WalletKey é inválido",
      },
      212: {
        message: "Configuração da Carteira do Comerciante não encontrada",
      },
      213: {
        message: "O número do cartão de crédito é inválido",
      },
      214: {
        message: "Titular do cartão de crédito deve ter apenas cartas",
      },
      215: {
        message: "Agência é necessária em Credencial de Boleto",
      },
      216: {
        message: "O endereço IP do cliente é inválido",
      },
      300: {
        message: "MerchantId não foi encontrado",
      },
      301: {
        message: "Solicitar IP não é permitido",
      },
      302: {
        message: "O MerchantOrderId enviado é duplicado",
      },
      303: {
        message: "O OrderId enviado não existe",
      },
      304: {
        message: "A identidade do cliente é obrigatória",
      },
      306: {
        message: "O comerciante está bloqueado",
      },
      307: {
        message: "Transação não encontrada",
      },
      308: {
        message: "Transação não disponível para capturar",
      },
      309: {
        message: "Transação não disponível para anular",
      },
      310: {
        message: "O método de pagamento não suporta esta operação",
      },
      311: {
        message: "O reembolso não está ativado para este comerciante",
      },
      312: {
        message: "Transação não disponível para reembolso",
      },
      313: {
        message: "Pagamento Recorrente não encontrado",
      },
      314: {
        message: "Integração Inválida",
      },
      315: {
        message: "Não é possível alterar NextRecurrency com pagamento pendente",
      },
      316: {
        message: "Não é possível definir NextRecurrency como data passada",
      },
      317: {
        message: "Dia de Recorrência Inválido",
      },
      318: {
        message: "Nenhuma transação encontrada",
      },
      319: {
        message: "A recorrência inteligente não está ativada",
      },
      320: {
        message:
          "Não é possível atualizar a afiliação porque essa recorrência não é salva",
      },
      321: {
        message:
          "Não é possível definir EndDate para antes da próxima recorrência",
      },
      322: {
        message: "O Zero Dollar Auth não está ativado",
      },
      323: {
        message: "A consulta bin não está ativada",
      },
    };
  }
})();
