(function () {
  angular.module("idelivery").controller("cartCtrl", cartCtrl);

  cartCtrl.$inject = [
    "$scope",
    "ideliveryServices",
    "$state",
    "sharedProperties",
    "$ionicPopup",
    "ideliverySearchAddressService",
    "$ionicModal",
  ];
  function cartCtrl(
    $scope,
    ideliveryServices,
    $state,
    sharedProperties,
    $ionicPopup,
    ideliverySearchAddressService,
    $ionicModal
  ) {
    $scope.$on("$ionicView.beforeEnter", function () {
      $scope.orders = sharedProperties.getCart().data;
      $scope.EstablishmentAddress = sharedProperties.getEstablishmentAddress();
      $scope.store = JSON.parse(Util.getLocalSelectedStore());

      var deliveryAddress = JSON.parse(
        localStorage.getItem("deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG)
      );
      $scope.isPickUp = sharedProperties.getIsPickUp();
      $scope.address = deliveryAddress;

      var orderData = {
        establishment_id: sharedProperties.getEstablishmentId(),
        items: $scope.orders,
        delivery_address: deliveryAddress.id,
        isPickUp: sharedProperties.getIsPickUp(),
      };

      ideliveryServices.calculateOrder(orderData).then(function (response) {
        var itemsTotal = 0;

        for (var i = 0; i < response.data.items.length; i++) {
          $scope.orders[i].totalOrder = response.data.items[i].totalOrder;
          itemsTotal += $scope.orders[i].totalOrder;
        }
        $scope.freightCost = response.data.freightCost;
        $scope.discount = response.data.discount;
        $scope.subtotal = itemsTotal;

        // Mensagem que aparece nos detalhes do pedido
        $scope.discount_span_total = $scope.discount * 100;
        console.log($scope.discount_span_total);

        if ($scope.discount) {
          $scope.discountValue = $scope.subtotal * $scope.discount;
          $scope.total = response.data.total - $scope.discountValue;
        } else {
          $scope.total = response.data.total;
        }

        $scope.discount_message = response.data.discount_message;
      });
    });

    $scope.duplicateOrderCart = function (index) {
      $scope.orders[index].totalOrder += $scope.orders[index].unityValue;
      $scope.subtotal += $scope.orders[index].unityValue;
      var total = $scope.subtotal + $scope.freightCost;

      if ($scope.discount) {
        $scope.discountValue = $scope.subtotal * $scope.discount;
        $scope.total = total - $scope.discountValue;
      } else {
        $scope.total = total;
      }

      $scope.orders[index].quantity++;
    };

    $scope.removeOrderCart = function (index) {
      if ($scope.orders[index].quantity > 1) {
        $scope.orders[index].totalOrder -= $scope.orders[index].unityValue;
        $scope.subtotal -= $scope.orders[index].unityValue;
        var total = $scope.subtotal + $scope.freightCost;

        if ($scope.discount) {
          $scope.discountValue = $scope.subtotal * $scope.discount;
          $scope.total = total - $scope.discountValue;
        } else {
          $scope.total = total;
        }

        $scope.orders[index].quantity--;
      } else {
        showConfirm();
      }

      function showConfirm() {
        var confirmPopup = $ionicPopup.confirm({
          title: "Tem certeza de apagar este item?",
          buttons: [
            {
              text: "Cancelar",
              type: "button-light",
            },
            {
              text: "OK",
              type: "idl-button",
              onTap: function () {
                return true;
              },
            },
          ],
        });
        confirmPopup.then(function (res) {
          if (res) {
            $scope.subtotal -= $scope.orders[index].unityValue;
            var total = $scope.subtotal + $scope.freightCost;

            if ($scope.discount) {
              $scope.discountValue = $scope.subtotal * $scope.discount;
              $scope.total = total - $scope.discountValue;
            } else {
              $scope.total = total;
            }

            $scope.orders.splice(index, 1);
          }
        });
      }
    };

    $scope.payOrder = function () {
      ideliveryServices.getRestaurantStatus().then(function (response) {
        $scope.restaurantStatus = response.data.isOpen;

        if (!$scope.restaurantStatus) {
          $ionicPopup.alert({
            title: "Estamos Fechados",
            template: "Não atendemos neste horário. :(",
          });
        } else {
          if ($scope.subtotal >= $scope.store.min_order_value) {
            $scope.finishOrder = {
              total: $scope.total,
              subtotal: $scope.subtotal,
              items: $scope.orders,
              freightCost: $scope.freightCost,
              isPickUp: sharedProperties.getIsPickUp(),
            };

            if (configIDL.OPTIONAL_RECEIPT == 3)
              $scope.finishOrder.receipt = {
                checked: true,
              };

            sharedProperties.setFinishedOrder($scope.finishOrder);
            $state.go("app.paymentForm");
          } else {
            notie.alert(
              2,
              "Total do pedido menor que o valor mínimo de pedido (R$" +
                $scope.store.min_order_value +
                ").",
              5
            );
          }
        }
      });
    };

    $scope.recalculateTotalOrder = function (newFreightValue) {
      $scope.total -= $scope.freightCost;
      $scope.total += newFreightValue;
      $scope.freightCost = newFreightValue;
    };

    $ionicModal
      .fromTemplateUrl("app/cart/deliveryAddressModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.deliveryAddressModal = modal;
      });

    $scope.closedeliveryAddressModal = function (chosenAddress) {
      $scope.deliveryAddressModal.hide();
      if (chosenAddress) {
        localStorage.setItem(
          "deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG,
          JSON.stringify(chosenAddress)
        );
        ideliverySearchAddressService
          .getNeighborhoodFreight(
            chosenAddress.neighborhood.city.id,
            chosenAddress.neighborhood.id
          )
          .then(function (response) {
            var newFreightValue = response.data.price_shipping;

            $scope.address.street = chosenAddress.street;
            $scope.address.number = chosenAddress.number;
            $scope.address.neighborhood = chosenAddress.neighborhood;
            $scope.address.complement = chosenAddress.complement;
            $scope.orders.delivery_address = chosenAddress.id;
            $scope.recalculateTotalOrder(newFreightValue);
          });
      }
    };

    $scope.opendeliveryAddressModal = function () {
      ideliverySearchAddressService
        .getAddressByEstablishment($scope.store.id)
        .then(function (response) {
          $scope.userAddress = response.data;
          $scope.deliveryAddressModal.show();
        });
    };

    // Cleaning up modals on destroy
    $scope.$on("$destroy", function () {
      $scope.deliveryAddressModal.remove();
    });
  }
})();
