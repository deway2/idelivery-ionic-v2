var Util = {

  getServer: function () {
    return localStorage.getItem('productionServer-' + configIDL.ESTABLISHMENT_SLUG);
  },

  getLocalToken: function () {
    return localStorage.getItem("token_app-" + configIDL.ESTABLISHMENT_SLUG);
  },

  getCurrentUserData: function () {
    return localStorage.getItem("user_data-" + configIDL.ESTABLISHMENT_SLUG);
  },

  getLocalSelectedStore: function () {
    return localStorage.getItem("store_selected-" + configIDL.ESTABLISHMENT_SLUG);
  },

  getStoresQuantity: function () {
    return JSON.parse(localStorage.getItem("store_quantity-" + configIDL.ESTABLISHMENT_SLUG));
  },

  getDeviceToken: function () {
    return JSON.parse(localStorage.getItem("device_token-" + configIDL.ESTABLISHMENT_SLUG));
  },

  getCurrentSelectedAddress: function () {
    return JSON.parse(localStorage.getItem("deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG));
  },

  UFS: [
    "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO"
  ],

  getDistanceFromLatLngInKm: function (lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    //d * 1000; Distance in meters
    return d;

    function deg2rad(deg) {
      return deg * (Math.PI / 180);
    }
  }

};
