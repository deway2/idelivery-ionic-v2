"use strict";

describe('Clicking on the login button ', function () {
    var username, password, loginButton, loginModalButton, modal, loginServer, notieAlert;

    beforeEach(function () {
        browser.get('/#/app/login');
        username = element(by.model('signInData.email'));
        password = element(by.model('signInData.password'));
        loginModalButton = element(by.linkText('Efetuar Login'));
        loginButton = element(by.linkText('Entrar'));
        modal = element(by.className('signInModal'));
        loginServer = browser.post('http://idl-back-prod.herokuapp.com/api/users/sign_in/');
        notieAlert = element(by.id('notie-alert-outer'));
        browser.waitForAngular();
    });

    it('should validate the credentials for a successful login and display the Stores/Products view', function () {
        // Test successful login

        loginModalButton.click().then(function () {
            expect(modal.isDisplayed()).toBeTruthy();

            username.sendKeys('paulo.oliveira@deway.com.br');
            password.sendKeys('123456');

            loginButton.click().then(function () {
                expect(browser.getLocationAbsUrl()).toMatch('/#/app/stores');
            });
        });
    });

    it('should display a popup for an unsuccessful login', function () {
        // Test unsuccessful login

        loginModalButton.click().then(function () {
            expect(modal.isDisplayed()).toBeTruthy();

            username.sendKeys('paulo.oliveira@deway.com.br');
            password.sendKeys('idontknow');

            loginButton.click().then(function () {
                expect(browser.getLocationAbsUrl()).toMatch('/#/app/login');
                expect(notieAlert.isDisplayed()).toBeTruthy();
            });
        });
    });
});
