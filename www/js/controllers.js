(function () {
  angular
    .module("idelivery")
    .controller("AppCtrl", AppCtrl)
    .directive("ngEnter", ngEnter)
    .directive("idlToggleOverflowScroll", idlToggleOverflowScroll);

  AppCtrl.$inject = [
    "$scope",
    "$cordovaNetwork",
    "$ionicModal",
    "$ionicPopup",
    "$state",
    "ideliveryServices",
    "$ionicLoading",
    "$interval",
    "$timeout",
    "$window",
    "$ionicHistory",
    "sharedProperties",
    "storesService",
    "$ionicPlatform",
    "$ionicNavBarDelegate",
    "$ionicSideMenuDelegate",
    "$cordovaSocialSharing",
    "$rootScope",
  ];

  function AppCtrl(
    $scope,
    $cordovaNetwork,
    $ionicModal,
    $ionicPopup,
    $state,
    ideliveryServices,
    $ionicLoading,
    $interval,
    $timeout,
    $window,
    $ionicHistory,
    sharedProperties,
    storesService,
    $ionicPlatform,
    $ionicNavBarDelegate,
    $ionicSideMenuDelegate,
    $cordovaSocialSharing,
    $rootScope
  ) {
    $scope.verifyUserSessionStatus = Util.getLocalToken();
    $scope.userProfileData = JSON.parse(
      localStorage.getItem("user_data-" + configIDL.ESTABLISHMENT_SLUG)
    );
    $scope.storeSelected = JSON.parse(Util.getLocalSelectedStore());
    $scope.server = Util.getServer();
    $scope.isDeprecated = false;

    var currentDevicePlatform = {
      name: ionic.Platform.platform(),
      version: ionic.Platform.version(),
    };

      //  && ionic.Platform.version() >= 13;

    var listLayout = "";

    if (
      currentDevicePlatform.name === "android" &&
      currentDevicePlatform.version <= 4.3
    ) {
      listLayout = localStorage.setItem(
        "listLayout-" + configIDL.ESTABLISHMENT_SLUG,
        "list"
      );
    } else {
      listLayout = localStorage.getItem(
        "listLayout-" + configIDL.ESTABLISHMENT_SLUG
      );
      if (!listLayout || listLayout === "")
        localStorage.setItem(
          "listLayout-" + configIDL.ESTABLISHMENT_SLUG,
          configIDL.LIST_LAYOUT
        );
    }

    $scope.verifyLocalStatus = function () {
      return !!$scope.verifyUserSessionStatus;
    };

    if (configIDL.MENU_STYLE === 1) {
      $scope.iconMenu = "fa-cutlery";
      $scope.optionMenu = "Cardápio";
    } else {
      $scope.iconMenu = "fa-list-ul";
      $scope.optionMenu = "Produtos";
    }

    $scope.ANDROID = {
      name: "Android",
      id: 2,
    };

    $scope.IOS = {
      name: "iOS",
      id: 1,
    };

    $scope.isAndroid = function () {
      return ionic.Platform.isAndroid();
    };

    $scope.isIOS = function () {
      return ionic.Platform.isIOS();
    };

    $scope.$on("$ionicView.beforeEnter", function () {
      $scope.isMarketPlace = null;

      var slugEstabelecimento = configIDL.ESTABLISHMENT_SLUG;

      //console.log($scope.slugEstabelecimento);

      $scope.openTermsofuser = function(){
        var website = "https://termsofuse.ideliveryapp.com.br/?slug=" + slugEstabelecimento
        var terms = cordova.InAppBrowser.open(website, "_blank", "location=yes");
      }

      if (slugEstabelecimento === "betapizzaria.deway.com.br") {
        $scope.isMarketPlace = true;
        var idlmenuitembusiness = (document.getElementById(
          "idl-menu-item-business"
        ).style.display = "block");
      } else {
        var idlmenuitembusiness = (document.getElementById(
          "idl-menu-item-business"
        ).style.display = "none");
      }

      $scope.RegisterBusiness = function () {
        var ref = cordova.InAppBrowser.open(
          "https://ideliveryapp.com.br/#contact_form",
          "_blank",
          "location=yes"
        );
      };

      $scope.openLinkRedirect = function (url) {
        var ref = cordova.InAppBrowser.open(url, "_blank", "location=yes");
      };

      $scope.openEmail = function (email) {
        var prefix = "mailto:";
        var finalEmail = prefix + email;
        window.open(finalEmail, "_system", "location=yes");
      };

      $ionicPlatform.ready(function () {
        //Check connection every 15 seconds.
        if (typeof cordova !== "undefined") {
          if (!$cordovaNetwork.isOnline()) {
            $scope.showLoading();
          } else {
            $scope.checkConnection = $interval(function () {
              if (!$cordovaNetwork.isOnline()) {
                $scope.showLoading();
              }
            }, 15000);
          }
        }

        if (window.cordova) {
          $scope.verifyAppVersion();
        }

        $scope.verifyUserSessionStatus = Util.getLocalToken();
        $scope.userProfileData = JSON.parse(
          localStorage.getItem("user_data-" + configIDL.ESTABLISHMENT_SLUG)
        );
        $scope.storeSelected = JSON.parse(Util.getLocalSelectedStore());
        $scope.server = Util.getServer();
        $scope.isDeprecated = false;

        $scope.verifyLocalStatus = function () {
          return !!$scope.verifyUserSessionStatus;
        };

        if (!$scope.userProfileData || $scope.userProfileData === "") {
          $scope.userProfileData = {
            name: "Visitante",
            photo: "img/anonUser.png",
          };
        } else if (!$scope.userProfileData.photo) {
          $scope.userProfileData.photo = "img/anonUser.png";
        }

        if ($scope.server && $scope.server !== "") {
          storesService.getStores().success(function (response) {
            $scope.verifyEstablishmentsQuantity = response;
            $scope.tersmHTML = response[0].slug.contract;
            $scope.androidUrl = response[0].slug.link_google_play;
            $scope.iOSUrl = response[0].slug.link_app_store;

            localStorage.setItem(
              "store_quantity-" + configIDL.ESTABLISHMENT_SLUG,
              response.length
            );

            if (response.length == 1) {
              localStorage.setItem(
                "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                JSON.stringify(response[0])
              );
            } else {
              for (var i = 0; i < response.length; i++) {
                if (
                  $scope.storeSelected &&
                  $scope.storeSelected.id === response[i].id
                ) {
                  localStorage.setItem(
                    "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                    JSON.stringify(response[i])
                  );
                  break;
                }
              }
            }
          });
        }
      });
    });

    // $ionicModal.fromTemplateUrl('app/ratings/ratings.html', {
    //   scope: $scope
    // }).then(function (modal) {
    //   $scope.ratingsModal = modal;
    // });

    $scope.$on("$ionicView.enter", function () {
      ////Push Notification and Google Analytics Init////
      $ionicPlatform.ready(function () {
        if (window.cordova) {
          //Google Analytics

          //Notification Android
          if (ionic.Platform.isAndroid()) {
            if (ionic.Platform.isAndroid()) {
              $scope.deviceType = $scope.ANDROID.id;
            }
            window.FirebasePlugin.onNotificationOpen(
              function (notification) {
                //alert(JSON.stringify(notification.message))
                console.log(notification);
                if (notification.body) {
                  $ionicPopup.show({
                    title: notification.body,
                    buttons: [
                      {
                        text: "OK",
                        type: "button-positive",
                        onTap: function () {
                          return true;
                        },
                      },
                    ],
                  });
                }
              },
              function (error) {
                console.error(error);
              }
            );
          } else {
            //Push Notifications
            var push = PushNotification.init({
              android: {
                senderID: configIDL.SENDER_ID,
                vibrate: true,
                icon:
                  "platforms/android/res/drawable-xxxhdpi/ic_onesignal_large_icon_default.png",
              },
              ios: {
                alert: true,
                badge: true,
                sound: true,
              },
            });

            push.on("notification", function (notification) {
              $ionicPopup.show({
                title: notification.message,
                buttons: [
                  {
                    text: "OK",
                    type: "button-positive",
                    onTap: function () {
                      return true;
                    },
                  },
                ],
              });
            });

            push.on("error", function (error) {
              console.log("error: " + JSON.stringify(error));
            });
          }
        }
      });
    });

    $scope.showLoading = function () {
      $ionicLoading.show({
        template:
          '<ion-spinner icon="circles" class="idl-spinner"></ion-spinner>',
        duration: 5000,
      });

      $timeout(function () {
        if (
          $scope.verifyUserSessionStatus &&
          $scope.verifyUserSessionStatus !== "" &&
          $cordovaNetwork.isOnline()
        ) {
          if ($scope.verifyEstablishmentsQuantity.length > 1) {
            $state.go(
              "app.stores",
              {},
              {
                reload: true,
              }
            );
          } else {
            $state.go(
              "app.products",
              {},
              {
                reload: true,
              }
            );
          }
          $ionicLoading.hide();
        } else {
          if ($cordovaNetwork.isOnline()) {
            $state.go(
              "app.main",
              {},
              {
                reload: true,
              }
            );
          } else {
            notie.alert(
              1,
              "Parece que você está sem conexão com a internet no momento.",
              5
            );
            $window.location.reload(true);
          }
          $ionicLoading.hide();
        }
      }, 5200);
    };

    $scope.doLogout = function () {
      $ionicPopup.show({
        title: "Deseja sair?",
        buttons: [
          {
            text: "Cancelar",
            type: "button-light",
          },
          {
            text: "Sim",
            type: "button-positive",
            onTap: function () {
              ideliveryServices.sendToken().then(
                function (response) {
                  if (response.data.success) {
                    $state.go("app.login");
                    $ionicHistory.clearHistory();
                    $ionicHistory.clearCache();
                    localStorage.clear();
                    sharedProperties.getCart().data = [];
                    configIDL.ESTABLISHMENT_ID = null;
                  } else {
                    notie.alert(3, response.data.message, 5);
                    $state.go("app.login");
                    $ionicHistory.clearHistory();
                    $ionicHistory.clearCache();
                    localStorage.clear();
                    sharedProperties.getCart().data = [];
                    configIDL.ESTABLISHMENT_ID = null;
                  }
                },
                function () {
                  notie.alert(
                    3,
                    "Não foi possível efetuar logout da aplicação. Tente Novamente.",
                    5
                  );
                }
              );
            },
          },
        ],
      });
    };
    $ionicModal
      .fromTemplateUrl("app/main/termsModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.termsModal = modal;
      });

    $scope.closeTermsModal = function () {
      $scope.termsModal.hide();
    };

    $scope.openTermsModal = function () {
      $scope.termsModal.show();
    };

    // Cleaning up modals on destroy
    $scope.$on("$destroy", function () {
      $scope.termsModal.remove();
    });
    $scope.goToLogin = function () {
      $state.go("app.login");
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
      localStorage.clear();
    };
    $scope.goFavorites = function () {
      if (sharedProperties.getCart().data.length > 0) {
        $ionicPopup.show({
          title: "Você possui itens no carrinho.",
          content: "Deseja esvaziá-lo e escolher outro estabelecimento?",
          buttons: [
            {
              text: "Cancelar",
              type: "button-light",
            },
            {
              text: "Sim",
              type: "button-positive",
              onTap: function () {
                $state.go("app.favorites");
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache();
                sharedProperties.getCart().data = [];
              },
            },
          ],
        });
      } else {
        $state.go("app.favorites");
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
        sharedProperties.getCart().data = [];
      }
    };
    $scope.chooseEstablishment = function () {
      if (sharedProperties.getCart().data.length > 0) {
        $ionicPopup.show({
          title: "Você possui itens no carrinho.",
          content: "Deseja esvaziá-lo e escolher outro estabelecimento?",
          buttons: [
            {
              text: "Cancelar",
              type: "button-light",
            },
            {
              text: "Sim",
              type: "button-positive",
              onTap: function () {
                $state.go("app.stores");
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache();
                sharedProperties.getCart().data = [];
              },
            },
          ],
        });
      } else {
        $state.go("app.stores");
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
        sharedProperties.getCart().data = [];
      }
    };

    $scope.go2Products = function () {
      $rootScope.currentView = "";
      $state.go("app.products");
    };

    $scope.editUserDataOrGoToLogin = function () {
      if ($scope.verifyUserSessionStatus) $state.go("app.userProfile");
      else $scope.goToLogin();
    };

    if (ionic.Platform.isIOS()) {
      $scope.deviceType = $scope.IOS.id;
    } else if (ionic.Platform.isAndroid()) {
      $scope.deviceType = $scope.ANDROID.id;
    }

    $scope.setupPushNotifications = function () {
      $ionicPlatform.ready(function () {
        if (ionic.Platform.isAndroid()) {
          window.FirebasePlugin.onNotificationOpen(
            function (notification) {
              //alert(JSON.stringify(notification.message))
              console.log(notification);
              if (notification.body) {
                $ionicPopup.show({
                  title: notification.body,
                  buttons: [
                    {
                      text: "OK",
                      type: "button-positive",
                      onTap: function () {
                        return true;
                      },
                    },
                  ],
                });
              }
            },
            function (error) {
              console.error(error);
            }
          );
          window.FirebasePlugin.getToken(
            function (token) {
              console.log(token);
              var notificationData = {
                device_type: $scope.deviceType,
                device_token: token,
                slug: configIDL.ESTABLISHMENT_SLUG,
              };

              ideliveryServices.addDevice(notificationData).then(function () {
                //alert('Deu bom registration push')
                localStorage.setItem(
                  "device_token-" + configIDL.ESTABLISHMENT_SLUG,
                  JSON.stringify(notificationData.device_token)
                );
              });
            },
            function (error) {
              alert("erro token");
            }
          );
        } else {
          console.log("On Ready");

          var pushRegistration = PushNotification.init({
            android: {
              senderID: configIDL.SENDER_ID,
              vibrate: true,
              icon:
                "platforms/android/res/drawable-xxxhdpi/ic_onesignal_large_icon_default.png",
            },
            ios: {
              alert: true,
              badge: true,
              sound: true,
            },
          });

          console.log(pushRegistration);

          pushRegistration.on("registration", function (data) {
            console.log("registration: ");
            console.log(data);

            var notificationData = {
              device_type: $scope.deviceType,
              device_token: data.registrationId,
              slug: configIDL.ESTABLISHMENT_SLUG,
            };

            ideliveryServices.addDevice(notificationData).then(function () {
              console.log("notification_data: ");
              console.log(notificationData);
              localStorage.setItem(
                "device_token-" + configIDL.ESTABLISHMENT_SLUG,
                JSON.stringify(notificationData.device_token)
              );
            });

            pushRegistration.on("notification", function (notification) {
              //notie.alert(2, notification.message, 5);
              $ionicPopup.show({
                title: notification.message,
                buttons: [
                  {
                    text: "OK",
                    type: "button-positive",
                    onTap: function () {
                      return true;
                    },
                  },
                ],
              });
            });

            pushRegistration.on("error", function (error) {
              console.log("error: " + JSON.stringify(error));
            });
          });
        }
      });
    };

    $scope.verifyAppVersion = function () {
      var data = {
        version: localStorage.getItem(
          "appVersion-" + configIDL.ESTABLISHMENT_SLUG
        ),
        device_type: $scope.deviceType,
        slug: configIDL.ESTABLISHMENT_SLUG,
      };
      ideliveryServices.validateAppVersion(data).then(function (response) {
        if (response.data.isDeprecated) {
          $scope.isDeprecated = true;
          $ionicNavBarDelegate.showBar(false);
          $ionicSideMenuDelegate.canDragContent(false);
        }
      });
    };

    $scope.openAppMarket = function () {
      if (ionic.Platform.isIOS()) {
        window.open(encodeURI($scope.iOSUrl), "_system", "location=yes");
      } else if (ionic.Platform.isAndroid()) {
        window.open(encodeURI($scope.androidUrl), "_system", "location=yes");
      }
    };

    $scope.nextInput = function (elementId) {
      document.getElementById(elementId).focus();
    };

    $scope.inviteFriend = function () {
      ideliveryServices.shareInfo().then(function (response) {
        var message = response.data.share_message;

        $cordovaSocialSharing
          .share(message, null, null, null) // Share via native share sheet
          .then(
            function (result) {},
            function (err) {}
          );
      });
    };
  }

  function ngEnter() {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if (event.which === 13) {
          scope.$apply(function () {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      });
    };
  }

  idlToggleOverflowScroll.$inject = ["$timeout", "$window", "idlIonicReady"];

  function idlToggleOverflowScroll($timeout, $window, idlIonicReady) {
    return {
      restrict: "A",
      link: link,
    };

    function link(scope, element) {
      var domElement = element[0];

      idlIonicReady().then(function onIdlIonicReady() {
        $window.addEventListener("native.keyboardshow", handleKeyboardShow);
        $window.addEventListener("native.keyboardhide", handleKeyboardHide);

        // remove event listener on destroy
        scope.$on("$destroy", removeKeyboardHandlerListener);

        function handleKeyboardShow() {
          console.log(
            "idlOverflowScrollToggle keyboard show: ELEMENT CLASS LIST: " +
              domElement.classList.toString()
          );
          var isIosOrAndroidFullScreen =
            ionic.Platform.isIOS() ||
            (ionic.Platform.isAndroid() && ionic.Platform.isFullScreen());

          if (isIosOrAndroidFullScreen) {
            console.log(
              "idlOverflowScrollToggle: " +
                "keyboard is shown, set overflow-y to: scroll"
            );
            domElement.style.overflowY = "hidden";
            // set -webkit-overflow-scrolling to auto for having non-momentum scrolling if
            // keyboard is up. Setting it to touch causes screen flicker when closing keyboard
            domElement.style.webkitOverflowScrolling = "auto";

            $timeout(function setOverflowYToScrollIfNeeded() {
              var scrollerHeight = element.height();
              var scrollerContentHeight = domElement.scrollHeight;

              // if scroller contains enough content to enable scrolling
              if (scrollerContentHeight > scrollerHeight + 1) {
                console.log(
                  "idlOverflowScrollToggle keyboard show: " +
                    "scroller height / scroller content height: " +
                    scrollerHeight +
                    " / " +
                    scrollerContentHeight
                );

                console.log(
                  "idlOverflowScrollToggle keyboard show: " +
                    "content larger than scroller, set overflow-y to: scroll"
                );
                domElement.style.overflowY = "scroll";
                // no need to set -webkit-overflow-scrolling as it should remain with value auto
                // whenever keyboard is up. We disable momentum scrolling when keyboard is up.
              }
            }, 400);
          }
        }

        function handleKeyboardHide() {
          console.log(
            "idlOverflowScrollToggle keyboard hide: ELEMENT CLASS LIST: " +
              domElement.classList.toString()
          );
          var isIosOrAndroidFullScreen =
            ionic.Platform.isIOS() ||
            (ionic.Platform.isAndroid() && ionic.Platform.isFullScreen());

          if (isIosOrAndroidFullScreen) {
            domElement.style.overflowY = "hidden";
            // set -webkit-overflow-scrolling to auto for keyboard transition
            domElement.style.webkitOverflowScrolling = "auto";

            $timeout(function setOverflowYToScrollIfNeeded() {
              var scrollerHeight = domElement.clientHeight;
              var scrollerContentHeight = domElement.scrollHeight;

              console.log(
                "idlOverflowScrollToggle keyboard hide: " +
                  "scroller height / scroller content height: " +
                  scrollerHeight +
                  " / " +
                  scrollerContentHeight
              );

              // if scroller contains enough content to enable scrolling
              if (scrollerContentHeight > scrollerHeight + 1) {
                console.log(
                  "idlOverflowScrollToggle keyboard hide: " +
                    "content larger than scroller, set overflow-y to: scroll"
                );
                domElement.style.overflowY = "scroll";
                // set -webkit-overflow-scrolling to touch for default momentum scrolling if
                // keyboard is not up
                domElement.style.webkitOverflowScrolling = "touch";
              }
            }, 400);
          }
        }

        function removeKeyboardHandlerListener() {
          $window.removeEventListener(
            "native.keyboardshow",
            handleKeyboardShow
          );
          $window.removeEventListener(
            "native.keyboardhide",
            handleKeyboardHide
          );
        }
      });
    }
  }
})();
