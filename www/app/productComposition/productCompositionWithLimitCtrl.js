(function () {
  angular
    .module("idelivery")
    .controller(
      "productCompositionWithLimitCtrl",
      productCompositionWithLimitCtrl
    );

  productCompositionWithLimitCtrl.$inject = [
    "$scope",
    "ideliveryServices",
    "$state",
    "$stateParams",
    "productCompositionService",
    "$ionicLoading",
  ];
  function productCompositionWithLimitCtrl(
    $scope,
    ideliveryServices,
    $state,
    $stateParams,
    productCompositionService,
    $ionicLoading
  ) {
    productCompositionService.isPreOrder(false);
    $scope.$on("$ionicView.beforeEnter", function () {
      $scope.checkCompositionCurrentIndex = function (index) {
        var currentData = productCompositionService.getObjects();

        //caso usuario volte a tela anterior, remover seleção
        var ind = productCompositionService.getTabIndex();
        if (ind > index || productCompositionService.wasPreOrder()) {
          currentData.pop();
          productCompositionService.setTabIndex(index--);
        }
        productCompositionService.resetObject(currentData);
        productCompositionService.setTabIndex(index);
      };

      $scope.step = $stateParams.index;
      $scope.checkCompositionCurrentIndex(parseInt($stateParams.index));

      ideliveryServices
        .getCompositionCategory($stateParams.compositionId)
        .then(function (response) {
          $scope.currentComposition = response.data;

          for (
            var i = 0;
            i < $scope.currentComposition.supplements.length;
            i++
          ) {
            $scope.currentComposition.supplements[i].quantity = 0;
          }

          $scope.counter = 0;

          $scope.limitMax = $scope.currentComposition.max;
          $scope.limitMin = $scope.currentComposition.min;
        });

      $scope.product = productCompositionService.getProduct();
      $scope.image_array = [$scope.product.cover_image];
      if ($scope.product.image_one != null)
        $scope.image_array.push($scope.product.image_one);
      if ($scope.product.image_two != null)
        $scope.image_array.push($scope.product.image_two);
      if ($scope.product.image_three != null)
        $scope.image_array.push($scope.product.image_three);
      if ($scope.product.image_four != null)
        $scope.image_array.push($scope.product.image_four);
      if ($scope.product.image_five != null)
        $scope.image_array.push($scope.product.image_five);
    });

    $scope.setState = function (id) {
      ideliveryServices.getCompositionCategory(id).then(function (response) {
        $ionicLoading.hide();

        $scope.nextComp = response.data;
        if ($scope.nextComp.max > 1) {
          $state.go("app.productCompositionWithLimit", {
            productId: $stateParams.productId,
            compositionId: id,
            index: productCompositionService.getObjects().length + 1,
          });
        } else {
          $state.go("app.productComposition", {
            productId: $stateParams.productId,
            compositionId: id,
            index: productCompositionService.getObjects().length + 1,
          });
        }
      });
    };

    $scope.nextComposition = function () {
      $ionicLoading.show({
        template:
          '<ion-spinner icon="circles" class="idl-spinner"></ion-spinner>',
      });

      var id, index, currentIndex;
      //Isso é selva.
      var addedValue = {
        compAdded: [],
      };
      var complements = $scope.currentComposition.supplements;

      for (var i = 0; complements[i]; i++) {
        if (complements[i].quantity > 0) {
          var data = complements[i];
          data.step = $scope.step;
          addedValue.compAdded.push(data);
        }
      }

      if ($scope.counter >= $scope.currentComposition.min) {
        if ($scope.counter > $scope.currentComposition.max) {
          $ionicLoading.hide();
          notie.alert(2, "Você adicinou acima do limite permitido.", 4);
        } else {
          productCompositionService.setObject(
            addedValue.compAdded,
            productCompositionService.getObjects().length
          );

          ideliveryServices
            .getProducts($stateParams.productId)
            .then(function (response) {
              $scope.products = response.data;
              currentIndex = $scope.products.category_supplements.indexOf(
                parseInt($stateParams.compositionId)
              );
              index = currentIndex + 1;

              if (
                currentIndex !=
                $scope.products.category_supplements.length - 1
              ) {
                id = $scope.products.category_supplements[index];
                $scope.setState(id);
              } else {
                $ionicLoading.hide();
                var tabIndex = productCompositionService.getTabIndex();
                productCompositionService.setTabIndex(parseInt(tabIndex++));
                $state.go("app.preOrder");
              }
            });
        }
      } else {
        $ionicLoading.hide();
        notie.alert(2, "Adicione algum item.", 3);
      }
    };

    $scope.duplicateItems = function (index) {
      if ($scope.counter < $scope.limitMax) {
        $scope.currentComposition.supplements[index].quantity++;
        $scope.counter++;
      } else {
        notie.alert(
          2,
          "Você atingiu o máximo de adições possíveis. (Máx: " +
            $scope.limitMax +
            ")",
          5
        );
      }
    };

    $scope.decreaseItems = function (index) {
      if ($scope.currentComposition.supplements[index].quantity > 0) {
        $scope.currentComposition.supplements[index].quantity--;
        $scope.counter--;
      }
    };
    $scope.viewer = function (index) {
      PhotoViewer.show($scope.image_array[index], "Imagem " + (index + 1));
    };
  }
})();
