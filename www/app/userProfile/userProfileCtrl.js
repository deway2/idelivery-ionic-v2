(function () {
  "use strict";

  angular
    .module('idelivery')
    .controller('userProfileCtrl', userProfileCtrl);

  userProfileCtrl.$inject = ['$scope', 'ideliveryServices', '$state'];
  function userProfileCtrl($scope, ideliveryServices, $state) {

    $scope.isUserLoggedIn = Util.getLocalToken();
    $scope.currentUser = JSON.parse(Util.getCurrentUserData());
    $scope.storesQuantity = Util.getStoresQuantity();

    ideliveryServices.getUserProfile($scope.currentUser.id).then(function (response) {
      $scope.userData = response.data;
    });

    $scope.saveChanges = function (userData) {
      ideliveryServices.changeUserProfile(userData, $scope.currentUser.id).then(function (response) {
        notie.alert(2, response.data, 5);
      });

      if ($scope.storesQuantity > 1) {
        $state.go('app.stores');
      } else {
        $state.go('app.products');
      }
    };
  }
})();
