(function () {
  "use strict";

  angular
    .module("idelivery")
    .factory("iDeliveryInterceptor", iDeliveryInterceptor)
    .service("storesService", storesService)
    .service("ideliveryServices", ideliveryServices)
    .service("ideliverySignUpService", ideliverySignUpService)
    .service("ideliverySignInService", ideliverySignInService)
    .service("ideliveryFacebookService", ideliveryFacebookService)
    .service("ideliveryAppleService", ideliveryAppleService)
    .service("ideliverySearchAddressService", ideliverySearchAddressService)
    .service("ideliveryServer", ideliveryServer)
    .service("productCompositionService", productCompositionService)
    .service("sharedProperties", sharedProperties) // Use it to share data between controllers
    .factory("idlIonicReady", idlIonicReady);

  iDeliveryInterceptor.$inject = ["$q", "$injector"];
  function iDeliveryInterceptor($q, $injector) {
    return {
      // optional methods
      request: request,
      requestError: requestError,
      response: response,
      responseError: responseError,
    };

    /* ----- */

    function request(config) {
      // do something on success
      return config;
    }

    function requestError(rejection) {
      // do something on error
      if (rejection.status === 403 || rejection.status === 401) {
        var state = $injector.get("$state");
        state.go("app.login");
        var history = $injector.get("$ionicHistory");
        history.clearHistory();
        history.clearCache();
        localStorage.clear();
      }

      return $q.reject(rejection);
    }

    function response(response) {
      // do something on success
      return response;
    }

    function responseError(rejection) {
      // do something on error
      if (rejection.status === 403 || rejection.status === 401) {
        var state = $injector.get("$state");
        state.go("app.login");
        var history = $injector.get("$ionicHistory");
        history.clearHistory();
        history.clearCache();
        localStorage.clear();
      }

      return $q.reject(rejection);
    }
  }

  storesService.$inject = ["$http", "RESOURCES"];
  function storesService($http, RESOURCES) {
    this.getStores = function (neighborhoodId) {
      var queryNeighborhood = "";

      if (neighborhoodId) {
        queryNeighborhood = "&id_neighborhood=" + neighborhoodId;
      }

      return $http.get(
        Util.getServer() +
          RESOURCES.RESTAURANT +
          "?slug=" +
          configIDL.ESTABLISHMENT_SLUG +
          queryNeighborhood
      );
    };
    this.allFavoriteEstablishments = function (data) {
      return $http({
        method: "POST",
        url:
          Util.getServer() +
          RESOURCES.RESTAURANT +
          "all_favorite_establishments/",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: data,
      })
        .success(function () {})
        .error(function () {});
    };
    this.addressValidation = (establishmentId, address) => {
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.RESTAURANT + "address_validation/",
        headers: {
          "Content-Type": "application/json",
        },
        data: {
          establishment_id: establishmentId,
          address: address,
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };
    this.GetStoresPickUp = (establishmentId) => {
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.RESTAURANT + "get_stores_pickup/",
        headers: {
          "Content-Type": "application/json",
        },
        data: {
          establishment_id: establishmentId,
          slug: configIDL.ESTABLISHMENT_SLUG,
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };
    this.getStore = (establishmentId) => {
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.RESTAURANT + "getStore/",
        headers: {
          "Content-Type": "application/json",
        },
        data: {
          establishment_id: establishmentId,
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };
    this.getAllStores = () => {
      return $http.get(
        Util.getServer() +
          RESOURCES.RESTAURANT +
          "?slug=" +
          configIDL.ESTABLISHMENT_SLUG
      );
    };
    this.searchEstablishments = function (keywords) {
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.RESTAURANT + "search_by_keywords/", //+ idEstablishment + sharedProperties.getEstablishmentId(),
        headers: {
          "Content-Type": "application/json",
        },
        data: {
          slug: configIDL.ESTABLISHMENT_SLUG,
          keywords: keywords,
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };
    this.getStoresByLocation = function (lat, long) {
      return $http.get(
        Util.getServer() +
          RESOURCES.RESTAURANT +
          "?slug=" +
          configIDL.ESTABLISHMENT_SLUG +
          "&latitude=" +
          lat +
          "&longitude=" +
          long
      );
    };
  }

  ideliveryServices.$inject = [
    "$http",
    "RESOURCES",
    "$ionicLoading",
    "sharedProperties",
    "$ionicPopup",
    "$injector",
  ];
  function ideliveryServices(
    $http,
    RESOURCES,
    $ionicLoading,
    sharedProperties,
    $ionicPopup,
    $injector
  ) {
    var idEstablishment = "?id_establishment=";
    var queryOrderDetails = "?djangoQuery=id=";
    var popUp = 0;

    this.maintainingPopup = function () {
      if (popUp === 0) {
        var alertPopup = $ionicPopup.alert({
          title: "Manutenção",
          template: "Desculpe o incômodo estamos em manutenção :(",
          buttons: [
            {
              text: "Tentar novamente",
              type: "button-positive",
            },
          ],
        });
        alertPopup.then(function () {
          popUp = 0;
          var win = $injector.get("$window");
          win.location.reload(true);
        });
      }
      popUp++;
    };

    this.sendToken = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.SEND_TOKEN,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: {
          device_token: Util.getDeviceToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getProducts = function (id) {
      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_PRODUCTS +
          id +
          "/" +
          idEstablishment +
          sharedProperties.getEstablishmentId(),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };

    this.getProductCategories = function (idEstabelecimento) {
      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_PRODUCT_CATEGORIES +
          idEstablishment +
          idEstabelecimento,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };

    this.getCompositionCategory = function (id) {
      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_SUPPLEMENT_CATEGORIES +
          id +
          "/" +
          idEstablishment +
          sharedProperties.getEstablishmentId(),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };

    this.getPaymentTypes = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.PAYMENT_TYPES +
          idEstablishment +
          sharedProperties.getEstablishmentId(),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getAcceptedPaymentTypes = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.ACCEPTED_PAYMENT_TYPES +
          idEstablishment +
          sharedProperties.getEstablishmentId(),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getCards = function (id) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      var cardId = "";

      if (id) {
        cardId = id + "/";
      }

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.CARDS +
          cardId +
          idEstablishment +
          sharedProperties.getEstablishmentId(),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getUpdatePagarme = function (transaction_id, establishment_id) {
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.UPDATE_PAGARME,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: {
          transaction_id: transaction_id,
          establishment_id: establishment_id,
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.fetchStoredCards = function (user) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.STORED_CARDS,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: user,
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
    this.calculateOrder = function (order) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.CALCULATE_ORDER,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: order,
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
    this.setFavorite = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.RESTAURANT + "set_favorite/",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: data,
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
    this.getFavorite = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.RESTAURANT + "get_favorite/",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: data,
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.availableSchedulingShifts = function () {
      var establishmentID = sharedProperties.getEstablishmentId();

      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        params: { establishment_id: establishmentID },
        url: Util.getServer() + RESOURCES.AVAILABLE_SCHEDULING_SHIFTS,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.validateDiscountCode = function (discount) {
      var establishmentID = sharedProperties.getEstablishmentId(); //establishment_id;

      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        data: angular.copy(discount),
        url: Util.getServer() + RESOURCES.VALIDATE_DISCOUNT_CODE,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
    this.dealOrder = function (order) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.ORDER,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: order,
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.orderDetails = function (id) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: Util.getServer() + RESOURCES.ORDER + queryOrderDetails + id,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getOrders = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: Util.getServer() + RESOURCES.ORDER,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getRestaurantStatus = function (id) {
      var _id = id || sharedProperties.getEstablishmentId();

      return $http({
        method: "GET",
        url: Util.getServer() + RESOURCES.RESTAURANT + _id + "/is_open/",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };

    this.getRestaurantDetails = function () {
      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.RESTAURANT +
          sharedProperties.getEstablishmentId() +
          "/",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };

    this.getUserProfile = function (id) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      var _id = id ? id + "/" : 0 + "/";

      return $http({
        method: "GET",
        url: Util.getServer() + RESOURCES.USER_PROFILE + _id,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.changeUserProfile = function (data, id) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "PATCH",
        url: Util.getServer() + RESOURCES.USER_PROFILE + id + "/",
        data: data,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.addDevice = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.ADD_DEVICE,
        data: data,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.validateAppVersion = function (data) {
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.CHECK_DEPRECATED,
        data: data,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };

    this.openMap = function (address) {
      var prefix = "http://maps.google.com/?q=";
      var finalAddress = prefix + address;
      window.open(finalAddress, "_system", "location=yes");
    };

    this.openPhone = function (cellphone) {
      var prefix = "tel:";
      var finalPhone = prefix + cellphone;
      window.open(finalPhone, "_system", "location=yes");
    };

    this.sendFeedback = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.SEND_FEEDBACK,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(data),
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.shareInfo = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });
      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.SHARE_INFO +
          configIDL.ESTABLISHMENT_SLUG,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
  }

  ideliverySignUpService.$inject = ["$http", "RESOURCES", "$ionicLoading"];
  function ideliverySignUpService($http, RESOURCES, $ionicLoading) {
    this.signUp = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.SIGN_UP,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(data),
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
  }

  ideliverySignInService.$inject = ["$http", "RESOURCES", "$ionicLoading"];
  function ideliverySignInService($http, RESOURCES, $ionicLoading) {
    this.signIn = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.SIGN_IN,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(data),
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.userRecoveryPasswordEmail = function (email, slug) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });
      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.USER_CHANGE_PASSWORD,
        data: {
          email: email,
          slug: slug,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
    };
  }

  ideliveryFacebookService.$inject = ["$http", "RESOURCES", "$ionicLoading"];
  function ideliveryFacebookService($http, RESOURCES, $ionicLoading) {
    this.signUp = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.LOGIN_FACEBOOK,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(data),
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
  }

  ideliveryAppleService.$inject = ["$http", "RESOURCES", "$ionicLoading"];
  function ideliveryAppleService($http, RESOURCES, $ionicLoading) {
    this.signUp = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.LOGIN_APPLE,
        headers: {
          "Content-Type": "application/json",
        },
        data: JSON.stringify(data),
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
  }

  ideliverySearchAddressService.$inject = [
    "$http",
    "RESOURCES",
    "$ionicLoading",
    "sharedProperties",
  ];
  function ideliverySearchAddressService(
    $http,
    RESOURCES,
    $ionicLoading,
    sharedProperties
  ) {
    var idEstablishment = "&id_establishment=";

    this.cepSearch = function (cep) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: RESOURCES.CEP_SEARCH + cep,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.locationSearch = function (lat, long) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: RESOURCES.LOCATION_SEARCH + lat + "," + long,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getStates = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_STATES +
          configIDL.ESTABLISHMENT_SLUG,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getCities = function (stateId) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_CITIES +
          configIDL.ESTABLISHMENT_SLUG +
          "&id_state=" +
          stateId,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getNeighborhoods = function (cityId) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_NEIGHBORHOODS +
          configIDL.ESTABLISHMENT_SLUG +
          "&id_city=" +
          cityId,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getNeighborhoodFreight = function (cityId, neighborhoodId) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_NEIGHBORHOODS +
          configIDL.ESTABLISHMENT_SLUG +
          "&id_city=" +
          cityId +
          "&id_neighborhood=" +
          neighborhoodId +
          idEstablishment +
          sharedProperties.getEstablishmentId(),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.setAddress = function (data) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "POST",
        url: Util.getServer() + RESOURCES.ADDRESS,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
        data: data,
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getAddress = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: Util.getServer() + RESOURCES.ADDRESS,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.deleteAddress = function (id) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "DELETE",
        url: Util.getServer() + RESOURCES.ADDRESS + id + "/",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getAddressByEstablishment = function (establishmentId) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_ADDRESS_BY_ESTABLISHMENT +
          "?id_establishment=" +
          establishmentId,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getAddressById = function (id) {
      return $http({
        method: "GET",
        url: Util.getServer() + RESOURCES.ADDRESS + id + "/",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Token " + Util.getLocalToken(),
        },
      })
        .success(function (response) {})
        .error(function (error) {});
    };

    this.getStateNameByUF = function (uf) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: Util.getServer() + RESOURCES.GET_STATE_NAME + uf,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getStateByName = function (name) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_STATES +
          configIDL.ESTABLISHMENT_SLUG +
          "&query=" +
          name,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getCityByName = function (name, stateId) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_CITIES +
          configIDL.ESTABLISHMENT_SLUG +
          "&id_state=" +
          stateId +
          "&query=" +
          name,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };

    this.getNeighborhoodByName = function (name, cityId) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url:
          Util.getServer() +
          RESOURCES.GET_NEIGHBORHOODS_BY_NAME +
          name +
          "&id_city=" +
          cityId +
          "&slug=" +
          configIDL.ESTABLISHMENT_SLUG,
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function () {
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    };
  }

  ideliveryServer.$inject = ["$http", "RESOURCES", "$ionicLoading"];
  function ideliveryServer($http, RESOURCES, $ionicLoading) {
    this.getProductionServer = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: configIDL.SERVERS[0],
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {
          if (response.server) {
            localStorage.setItem(
              "productionServer-" + configIDL.ESTABLISHMENT_SLUG,
              response.server + "api/mobile/"
            );
            $ionicLoading.hide();
          } else {
            getOtherProductionServer(1).then(function (res) {});
            $ionicLoading.hide();
          }
        })
        .error(function () {
          getOtherProductionServer(1).then(function (res) {});
          $ionicLoading.hide();
        });
    };

    function getOtherProductionServer(server) {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      return $http({
        method: "GET",
        url: configIDL.SERVERS[server],
        headers: {
          "Content-Type": "application/json",
        },
      })
        .success(function (response) {
          localStorage.setItem(
            "productionServer-" + configIDL.ESTABLISHMENT_SLUG,
            response.server + "api/"
          );
          $ionicLoading.hide();
        })
        .error(function () {
          $ionicLoading.hide();
        });
    }
  }

  function productCompositionService() {
    var tabIndex = 0;
    var productComposition = [];
    var wasPreOrder = false;
    var product = {};

    return {
      getTabIndex: function () {
        return tabIndex;
      },
      setTabIndex: function (data) {
        tabIndex = data;
      },
      getProduct: function () {
        return product;
      },
      setProduct: function (data) {
        product = data;
      },
      setObject: function (data, index) {
        if (productComposition[index] == null) {
          productComposition.push([]);
        }

        if (data[0] && data[0].id != null) {
          productComposition[index] = data;
        }
      },
      resetObject: function (data) {
        productComposition = data;
      },
      restartObject: function () {
        productComposition = [];
        product = {};
        tabIndex = 0;
      },
      getObject: function (index) {
        return productComposition[index];
      },
      getObjects: function () {
        return productComposition;
      },
      isPreOrder: function (data) {
        wasPreOrder = data;
      },
      wasPreOrder: function () {
        return wasPreOrder;
      },
    };
  }

  function sharedProperties() {
    var isPickUp = false;
    var isDelivery = true;
    var stringValue = "";
    var cpfcnpj = "";
    var objectAvailableShifts = {
      data: [],
    };
    var objectChooseTimeDelivery = {
      data: "",
    };
    var objectValue = {
      data: [],
    };
    var objectCart = {
      data: [],
    };
    var objectFinishedOrder = {
      data: "",
    };
    var SelectedCard = {
      data: "",
    };

    var objectLocatedAddress = {
      data: "",
    };

    var store = "";

    return {
      getString: function () {
        return stringValue;
      },
      setString: function (value) {
        stringValue = value;
      },
      setObject: function (value) {
        objectValue.data.push(value);
      },
      resetObject: function (value) {
        objectValue.data = value;
      },
      getObject: function () {
        return objectValue;
      },
      setCart: function (value) {
        objectCart.data.push(value);
      },
      getCart: function () {
        return objectCart;
      },
      setFinishedOrder: function (value) {
        objectFinishedOrder.data = value;
      },
      getFinishedOrder: function () {
        return objectFinishedOrder;
      },
      setSelectedCard: function (value) {
        SelectedCard.data = value;
      },
      getSelectedCard: function () {
        return SelectedCard;
      },
      setConditionalNavBackButton: function (value) {
        stringValue = value;
      },
      getConditionalNavBackButton: function () {
        return stringValue;
      },
      setAddress: function (value) {
        objectLocatedAddress.data = value;
      },
      getAddress: function () {
        return objectLocatedAddress;
      },
      getEstablishmentId: function () {
        store = JSON.parse(Util.getLocalSelectedStore());
        console.log(store, "teste");
        if (store) {
          return store.id;
        } else {
          return null;
        }
      },
      getEstablishmentAddress: function () {
        store = JSON.parse(Util.getLocalSelectedStore());

        if (store) {
          return {
            street: store.street,
            neighborhood: store.neighborhood,
            city: store.city.name,
          };
        } else {
          return null;
        }
      },
      setChooseTimeDelivery: function (value) {
        objectChooseTimeDelivery.data = value;
      },
      getChooseTimeDelivery: function () {
        return objectChooseTimeDelivery;
      },
      setAvailableShifts: function (value) {
        objectAvailableShifts.data = value;
      },
      getAvailableShifts: function () {
        return objectAvailableShifts.data;
      },
      setIsPickUp: function (value) {
        isPickUp = value;
      },
      getIsPickUp: function () {
        return isPickUp;
      },
      setIsDelivery: function (value) {
        isDelivery = value;
      },
      getIsDelivery: function () {
        return isDelivery;
      },
      setCpfCnpj: function (value) {
        cpfcnpj = value;
      },
      getCpfCnpj: function () {
        return cpfcnpj;
      },
      setCard: function (value) {
        objectCard.data = value;
      },
      getCard: function () {
        return objectCard;
      },
    };
  }

  idlIonicReady.$inject = ["$ionicPlatform"];
  function idlIonicReady($ionicPlatform) {
    var readyPromise;

    return function () {
      if (!readyPromise) {
        readyPromise = $ionicPlatform.ready();
      }
      return readyPromise;
    };
  }
})();
