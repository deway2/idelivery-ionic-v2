(function () {
  "use strict";

  angular
    .module('idelivery')
    .controller('addressCtrl', addressCtrl);

  addressCtrl.$inject = ['$state', '$scope', '$ionicModal', 'ideliverySearchAddressService', '$cordovaGeolocation', 'sharedProperties', '$ionicLoading', 'RESOURCES', '$ionicHistory'];
  function addressCtrl($state, $scope, $ionicModal, ideliverySearchAddressService, $cordovaGeolocation, sharedProperties, $ionicLoading, RESOURCES, $ionicHistory) {

    $scope.ufs = Util.UFS;
    $scope.cities = {};

    $scope.selectedStore = JSON.parse(Util.getLocalSelectedStore());

    $scope.$on('$ionicView.enter', function () {
      $scope.cView = $ionicHistory.currentView();

      if ($scope.cView.stateName === 'app.region') $scope.locateMe();

    });

    $scope.searchAddressData = {};
    $scope.locateMe = function () {

      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING
      });

      var posOptions = {
        timeout: 5000,
        enableHighAccuracy: true
      };
      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
        var lat = position.coords.latitude;
        var long = position.coords.longitude;

        ideliverySearchAddressService.locationSearch(lat, long).then(
          function (response) {
            if (response.data.status == 200) {
              $scope.searchAddressNameData = response.data.data;

              console.log(response);

              var formatted_address = $scope.searchAddressNameData.formatted_address;
              $scope.searchAddressNameData.formatted_address = formatted_address.substring(0, formatted_address.indexOf(','));

              $scope.searchAddressNameData.latitude = lat;
              $scope.searchAddressNameData.longitude = long;

              ideliverySearchAddressService.getStateByName($scope.searchAddressNameData.state).then(function (resp) {
                if (resp.data[0] && resp.data[0].name === $scope.searchAddressNameData.state) {
                  $scope.selectedStateName = resp.data[0].name;
                  $scope.currentStateId = resp.data[0].id;

                  ideliverySearchAddressService.getCityByName($scope.searchAddressNameData.city, $scope.currentStateId).then(function (res) {
                    if (res.data[0] && res.data[0].name === $scope.searchAddressNameData.city) {
                      $scope.selectedCityName = res.data[0].name;
                      $scope.currentCityId = res.data[0].id;

                      ideliverySearchAddressService.getNeighborhoodByName($scope.searchAddressNameData.neighborhood, $scope.currentCityId).then(function (r) {
                        if (r.data[0] && r.data[0].name === $scope.searchAddressNameData.neighborhood) {
                          $scope.selectedNeighborhoodName = r.data[0].name;
                          $scope.searchAddressData.neighborhood = r.data[0].id;
                        } else {
                          $scope.searchAddressData.neighborhood = "";
                          $scope.selectedNeighborhoodName = $scope.searchAddressData.neighborhood;
                        }
                      });
                    } else {
                      $scope.selectedCityName = "";
                      $scope.searchAddressNameData.city = "";
                    }
                  });
                } else {
                  notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
                  $scope.selectedStateName = "";
                  $scope.searchAddressNameData.state = "";
                }
              });

              $scope.searchAddressNameData.numero = $scope.searchAddressNameData.number;

            } else {
              notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
            }
          },
          function () {
            notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
          }
        );
      }, function () {
        $ionicLoading.hide();
        notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
      });
    };

    $scope.setRegion = function () {
      var regionData = {
        city: $scope.selectedCityName,
        state: $scope.selectedStateName,
        neighborhood: {
          id: $scope.searchAddressData.neighborhood,
          name: $scope.selectedNeighborhoodName
        }
      };

      localStorage.setItem('region-' + configIDL.ESTABLISHMENT_SLUG, JSON.stringify(regionData));

      $state.go('app.stores');
    };

    $scope.addressSearch = function () {
      $state.go('app.addressSearch');
    };

    $scope.addressSearchCEP = function (data) {
      sharedProperties.setAddress(data);
      $state.go('app.addressSearchCEP');
    };

    //////////////////////
    // Modal CEP Search //
    //////////////////////

    $ionicModal.fromTemplateUrl('app/address/searchCEPModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.searchCEPModal = modal;
    });

    $scope.closeCEPSearchModal = function () {
      $scope.searchCEPData = {};
      $scope.searchCEPModal.hide();
    };

    $scope.openCEPSearchModal = function () {
      $scope.searchCEPModal.show();
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.searchCEPModal.remove();
    });

    //////////////////
    // Modal States //
    //////////////////

    $ionicModal.fromTemplateUrl('app/address/statesModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.statesModal = modal;
    });

    $scope.closeStatesModal = function (stateName, stateId, stateUf) {
      $scope.statesModal.hide();
      if (stateId) {
        $scope.selectedStateName = stateName;
        $scope.searchAddressData.state = stateName;
        $scope.searchAddressData.short_state = stateUf;
        $scope.currentStateId = stateId;

        $scope.selectedCityName = "";
        $scope.searchAddressData.city = "";
        $scope.currentCityId = "";

        $scope.selectedNeighborhoodName = "";
        $scope.searchAddressData.neighborhood = "";
      }
    };

    $scope.openStatesModal = function () {
      $scope.statesModal.show();
      ideliverySearchAddressService.getStates().then(function (response) {
        $scope.states = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.statesModal.remove();
    });

    //////////////////
    // Modal Cities //
    //////////////////

    $scope.currentCity = {};

    $ionicModal.fromTemplateUrl('app/address/citiesModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.citiesModal = modal;
    });

    $scope.closeCitiesModal = function (cityName, cityId) {
      $scope.citiesModal.hide();
      if (cityName) {
        $scope.selectedCityName = cityName;
        $scope.searchAddressData.city = cityName;
        $scope.currentCityId = cityId;

        $scope.selectedNeighborhoodName = "";
        $scope.searchAddressData.neighborhood = "";
      }
    };

    $scope.openCitiesModal = function () {
      $scope.citiesModal.show();
      ideliverySearchAddressService.getCities($scope.currentStateId).then(function (response) {
        $scope.stateCities = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.citiesModal.remove();
    });

    /////////////////////////
    // Modal Neighborhoods //
    /////////////////////////

    $scope.currentNeighborhood = {};

    $ionicModal.fromTemplateUrl('app/address/neighborhoodsModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.neighborhoodsModal = modal;
    });

    $scope.closeNeighborhoodsModal = function (neighborhoodName, neighborhoodId) {
      $scope.neighborhoodsModal.hide();
      if (neighborhoodId) {
        $scope.selectedNeighborhoodName = neighborhoodName;
        $scope.searchAddressData.neighborhood = neighborhoodId;
        if (!neighborhoodId) {
          $scope.neighborhoods.name = neighborhoodName;
        }
      }
    };

    $scope.openNeighborhoodsModal = function () {
      $scope.neighborhoodsModal.show();
      ideliverySearchAddressService.getNeighborhoods($scope.currentCityId).then(function (response) {
        $scope.neighborhoods = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.neighborhoodsModal.remove();
    });

  }
})();
