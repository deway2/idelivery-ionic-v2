(function () {
  "use strict";

  angular
    .module('idelivery')
    .controller('addressSearchCtrl', addressSearchCtrl)
    .filter('localeSensitiveComparator', [localeSensitiveComparator]);

  function localeSensitiveComparator() {
    return function (items) {
      if (items) {
        items.sort(function (a, b) {
          return a.name.localeCompare(b.name);
        });
        return items;
      }
    };
  }

  addressSearchCtrl.$inject = ['$state', '$scope', '$ionicModal', 'ideliverySearchAddressService', '$cordovaGeolocation', '$ionicLoading', 'RESOURCES', '$ionicScrollDelegate'];
  function addressSearchCtrl($state, $scope, $ionicModal, ideliverySearchAddressService, $cordovaGeolocation, $ionicLoading, RESOURCES, $ionicScrollDelegate) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.ufs = Util.UFS;
      $scope.cities = {};

      $scope.selectedStore = JSON.parse(Util.getLocalSelectedStore());
    });

    $scope.$on('$ionicView.enter', function () {
      $scope.locateMe();
    });


    $scope.searchAddressData = {};
    $scope.setDeliveryAddressByName = function () {

      var cep = $scope.searchAddressData.postal_code;
      $scope.searchAddressData.postal_code = parseInt(cep.replace(/-/g, ""));

      var addressData = {
        city: $scope.selectedCityName,
        formatted_address: $scope.searchAddressData.formatted_address,
        state: $scope.selectedStateName,
        short_state: $scope.searchAddressData.short_state,
        number: $scope.searchAddressData.number,
        neighborhood: $scope.searchAddressData.neighborhood,
        postal_code: $scope.searchAddressData.postal_code,
        complement: $scope.searchAddressData.complement,
        reference_point: $scope.searchAddressData.reference_point
      };

      if (addressData && Util.getLocalToken()) {
        ideliverySearchAddressService.setAddress(addressData).then(function (response) {
          localStorage.setItem('deliveryAddress-' + configIDL.ESTABLISHMENT_SLUG, JSON.stringify(response.data.address_user));
          localStorage.removeItem('region-' + configIDL.ESTABLISHMENT_SLUG);

          if (response.data.count_establishments > 1) {
            localStorage.removeItem('store_selected-' + configIDL.ESTABLISHMENT_SLUG);
            $state.go('app.stores');
          } else {
            $state.go('app.products')
          }

          notie.alert(1, 'O endereço foi cadastrado.', 3);

        }, function () {
          notie.alert(3, 'Erro ao cadastrar. Verifique se todos os campos estão corretos.', 3);
        });
      } else {
        localStorage.setItem('deliveryAddress-' + configIDL.ESTABLISHMENT_SLUG, JSON.stringify(addressData));

        localStorage.removeItem('store_selected-' + configIDL.ESTABLISHMENT_SLUG);
        localStorage.removeItem('region-' + configIDL.ESTABLISHMENT_SLUG);

        if (Util.getStoresQuantity() > 1)
          $state.go('app.stores');
        else
          $state.go('app.products');

        notie.alert(1, 'O endereço foi salvo.', 3);
      }
    };

    $scope.locateMe = function () {

      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING
      });

      // console.log("ok");


      var posOptions = {
        timeout: 5000,
        enableHighAccuracy: true
      };
      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
        var lat = position.coords.latitude;
        var long = position.coords.longitude;

        ideliverySearchAddressService.locationSearch(lat, long).then(
          function (response) {
            if (response.data.status == 200) {
              $scope.searchAddressData = response.data.data;

              console.log(response);

              var formatted_address = $scope.searchAddressData.formatted_address;
              $scope.searchAddressData.formatted_address = formatted_address.substring(0, formatted_address.indexOf(','));

              $scope.searchAddressData.latitude = lat;
              $scope.searchAddressData.longitude = long;

              ideliverySearchAddressService.getStateByName($scope.searchAddressData.state).then(function (resp) {
                if (resp.data[0] && resp.data[0].name === $scope.searchAddressData.state) {
                  $scope.selectedStateName = resp.data[0].name;
                  $scope.currentStateId = resp.data[0].id;

                  ideliverySearchAddressService.getCityByName($scope.searchAddressData.city, $scope.currentStateId).then(function (res) {
                    if (res.data[0] && res.data[0].name === $scope.searchAddressData.city) {
                      $scope.selectedCityName = res.data[0].name;
                      $scope.currentCityId = res.data[0].id;

                      ideliverySearchAddressService.getNeighborhoodByName($scope.searchAddressData.neighborhood, $scope.currentCityId).then(function (r) {
                        if (r.data[0] && r.data[0].name === $scope.searchAddressData.neighborhood) {
                          $scope.selectedNeighborhoodName = r.data[0].name;
                          $scope.searchAddressData.neighborhood = r.data[0].id;
                        } else {
                          $scope.searchAddressData.neighborhood = "";
                          $scope.selectedNeighborhoodName = $scope.searchAddressData.neighborhood;
                        }
                      });
                    } else {
                      $scope.selectedCityName = "";
                      $scope.searchAddressData.city = "";
                    }
                  });
                } else {
                  notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
                  $scope.selectedStateName = "";
                  $scope.searchAddressData.state = "";
                }
              });

              $scope.searchAddressData.numero = $scope.searchAddressData.number;

            } else {
              notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
            }
          },
          function () {
            notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
          }
        );
      }, function () {
        $ionicLoading.hide();
        notie.alert(3, 'Não foi possível trazer todas as informações da localização atual. Preencha o endereço manualmente.', 7);
      });
    };

    //////////////////
    // Modal States //
    //////////////////

    $ionicModal.fromTemplateUrl('app/address/statesModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.statesModal = modal;
    });

    $scope.closeStatesModal = function (stateName, stateId, stateUf) {
      $scope.statesModal.hide();
      if (stateId) {
        $scope.selectedStateName = stateName;
        $scope.searchAddressData.state = stateName;
        $scope.searchAddressData.short_state = stateUf;
        $scope.currentStateId = stateId;

        $scope.selectedCityName = "";
        $scope.searchAddressData.city = "";
        $scope.currentCityId = "";

        $scope.selectedNeighborhoodName = "";
        $scope.searchAddressData.neighborhood = "";
      }
    };

    $scope.openStatesModal = function () {
      $scope.statesModal.show();
      ideliverySearchAddressService.getStates().then(function (response) {
        $scope.states = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.statesModal.remove();
    });

    //////////////////
    // Modal Cities //
    //////////////////

    $scope.currentCity = {};

    $ionicModal.fromTemplateUrl('app/address/citiesModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.citiesModal = modal;
    });

    $scope.closeCitiesModal = function (cityName, cityId) {
      $scope.citiesModal.hide();
      if (cityName) {
        $scope.selectedCityName = cityName;
        $scope.searchAddressData.city = cityName;
        $scope.currentCityId = cityId;

        $scope.selectedNeighborhoodName = "";
        $scope.searchAddressData.neighborhood = "";
      }
    };

    $scope.openCitiesModal = function () {
      $scope.citiesModal.show();
      ideliverySearchAddressService.getCities($scope.currentStateId).then(function (response) {
        $scope.stateCities = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.citiesModal.remove();
    });

    /////////////////////////
    // Modal Neighborhoods //
    /////////////////////////

    $scope.currentNeighborhood = {};

    $ionicModal.fromTemplateUrl('app/address/neighborhoodsModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.neighborhoodsModal = modal;
    });

    $scope.closeNeighborhoodsModal = function (neighborhoodName, neighborhoodId) {
      $scope.neighborhoodsModal.hide();
      if (neighborhoodId) {
        $scope.selectedNeighborhoodName = neighborhoodName;
        $scope.searchAddressData.neighborhood = neighborhoodId;
        if (!neighborhoodId) {
          $scope.neighborhoods.name = neighborhoodName;
        }
      }
    };

    $scope.openNeighborhoodsModal = function () {
      $scope.neighborhoodsModal.show();
      ideliverySearchAddressService.getNeighborhoods($scope.currentCityId).then(function (response) {
        $scope.neighborhoods = response.data;
      });
    };

    // Cleaning up modals on destroy
    $scope.$on('$destroy', function () {
      $scope.neighborhoodsModal.remove();
    });

    //This event fires when the keyboard will be shown
    if ($scope.isIOS()) {
      window.addEventListener('native.keyboardhide', function () {
        angular.element(document.querySelector('#id-scroll-deway')).addClass('resize-scroll');
      });

      window.addEventListener('native.keyboardshow', function () {
        angular.element(document.querySelector('#id-scroll-deway')).removeClass('resize-scroll');
        $ionicScrollDelegate.resize();
      });
    }

  }
})();
