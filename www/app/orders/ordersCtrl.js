(function () {
  angular
    .module('idelivery')
    .controller('ordersCtrl', ordersCtrl);

  ordersCtrl.$inject = ['$state', '$scope', 'ideliveryServices'];
  function ordersCtrl($state, $scope, ideliveryServices) {

    ideliveryServices.getOrders().then(function (response) {
      $scope.orders = response.data;
      console.log($scope.orders)
    });

    $scope.openOrderDetails = function (id) {
      $state.go('app.orderDetails', {orderId: id});
    };

    $scope.setOrderStatusName = function (status) {
      if (status == 1) {
        return "Pendente";
      } else if (status == 2) {
        return "Confirmado";
      } else if (status == 3) {
        return "Recusado";
      } else if (status == 4) {
        return "Cancelado";
      }
    };

    $scope.doRefresh = function () {
      ideliveryServices.getOrders().then(function (response) {
        $scope.orders = response.data;
      })
        .finally(function () {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        });
    };

  }
})();
