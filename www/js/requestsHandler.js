(function () {
  angular
    .module('idelivery')
    .config(config)
    .factory('InterceptorMaintainingMode', InterceptorMaintainingMode);

  InterceptorMaintainingMode.$inject = ['$injector'];
  function InterceptorMaintainingMode($injector) {
    function maintaining(res) {
      if (res.status == 503) {
        var services = $injector.get('ideliveryServices');
        services.maintainingPopup();
      }
    }

    return {
      response: function (res) {
        maintaining(res);
        return res;
      },
      responseError: function (res) {
        maintaining(res);
        return res;
      }
    }
  }

  config.$inject = ['$httpProvider'];
  function config($httpProvider) {
    $httpProvider.interceptors.push('InterceptorMaintainingMode');
  }
})();
