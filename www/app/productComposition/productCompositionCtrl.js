(function () {
  angular
    .module('idelivery')
    .controller('productCompositionCtrl', productCompositionCtrl);

  productCompositionCtrl.$inject = ['$scope', 'ideliveryServices', '$state', '$stateParams', 'productCompositionService', '$ionicLoading'];
  function productCompositionCtrl($scope, ideliveryServices, $state, $stateParams, productCompositionService, $ionicLoading) {

    productCompositionService.isPreOrder(false);
    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.checkCompositionCurrentIndex = function (index) {
        var currentData = productCompositionService.getObjects();

        //caso usuario volte a tela anterior, remover seleção
        var ind = productCompositionService.getTabIndex();
        if (ind > index || productCompositionService.wasPreOrder()) {
          currentData.pop();
          productCompositionService.setTabIndex(ind--);
        } else {
          productCompositionService.setTabIndex(index);
        }

        productCompositionService.resetObject(currentData);
        productCompositionService.setTabIndex(index);
      };

      $scope.comp = {checked: []};
      $scope.checkCompositionCurrentIndex(parseInt($stateParams.index));

      ideliveryServices.getCompositionCategory($stateParams.compositionId).then(
        function (response) {
          $scope.currentComposition = response.data;
        });

      $scope.product = productCompositionService.getProduct();
      $scope.image_array = [$scope.product.cover_image];
      if ($scope.product.image_one != null)
        $scope.image_array.push($scope.product.image_one);
      if ($scope.product.image_two != null)
        $scope.image_array.push($scope.product.image_two);
      if ($scope.product.image_three != null)
        $scope.image_array.push($scope.product.image_three);
      if ($scope.product.image_four != null)
        $scope.image_array.push($scope.product.image_four);
      if ($scope.product.image_five != null)
        $scope.image_array.push($scope.product.image_five);
    });

    $scope.setState = function (id) {
      ideliveryServices.getCompositionCategory(id).then(function (response) {

        $ionicLoading.hide();

        $scope.nextComp = response.data;
        if ($scope.nextComp.max > 1) {
          $state.go('app.productCompositionWithLimit', {
            productId: $stateParams.productId,
            compositionId: id,
            index: productCompositionService.getObjects().length + 1
          });
        } else {
          $state.go('app.productComposition', {
            productId: $stateParams.productId,
            compositionId: id,
            index: productCompositionService.getObjects().length + 1
          });
        }

      });
    };

    $scope.nextComposition = function () {

      $ionicLoading.show({
        template: '<ion-spinner icon="circles" class="idl-spinner"></ion-spinner>'
      });

      var id, index, currentIndex;
      $scope.comp.checked.quantity = 1;
      var checkedComposition = {compChecked: [$scope.comp.checked]};

      if ($scope.comp.checked.length >= $scope.currentComposition.min || $scope.comp.checked.id ||
        ($scope.comp.checked == false && $scope.currentComposition.min == 0)) {

        productCompositionService.setObject(checkedComposition.compChecked, productCompositionService.getObjects().length);

        ideliveryServices.getProducts($stateParams.productId).then(function (response) {

          $scope.products = response.data;
          currentIndex = $scope.products.category_supplements.indexOf(parseInt($stateParams.compositionId));
          index = currentIndex + 1;
          if (currentIndex != $scope.products.category_supplements.length - 1) {
            id = $scope.products.category_supplements[index];
            $scope.setState(id);
          } else {
            $ionicLoading.hide();
            $state.go('app.preOrder');
          }

        });
      } else {
        $ionicLoading.hide();
        notie.alert(2, 'Selecione algum item.', 3);
      }
    };

    /*-- Photo Viewer --*/
    $scope.viewer = function (index) {
      PhotoViewer.show($scope.image_array[index], ('Imagem ' + (index+1)));
    };

  }
})();
