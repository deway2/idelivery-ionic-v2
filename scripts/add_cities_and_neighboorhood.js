let pgp = require("pg-promise")();
var monitor = require("pg-monitor");
var db = pgp('postgres://postgres:202001@localhost:5432/idlv');


let xlsx = require("xlsx"),
    fileContent = xlsx.readFile('Mato Grosso.xlsx');

let stateId = 12;
db.tx(function(t) {
        var queries = [];
        for (let city in fileContent.Sheets) {
            if (fileContent.Sheets.hasOwnProperty(city)) { // This kind of check must be executed as good practice
                let citySheet = fileContent.Sheets[city];

                queries.push(t.one("insert into common_city(name, state_id, time_zone_id) values($1, $2, $3) returning id", [city, stateId, 2])
                    .then(function(data) {
                        console.log(`--> Created city "${city}";`);

                        for (let row in citySheet) {
                            if (citySheet.hasOwnProperty(row) && citySheet[row].hasOwnProperty("v") && citySheet[row].v !== city) {
                                let neighborhood = citySheet[row].v;

                                queries.push(t.none("insert into common_neighborhood(city_id, name) values($1, $2)", [data.id, neighborhood])
                                    .then(function() {
                                        console.log(`----> Created neighborhood "${neighborhood}" from city "${city}";`);
                                    })
                                    .catch(function(error) {
                                        console.log(`Error creating "${neighborhood}" from "${city}": ${error.message || error};`);
                                    }))
                            }
                        }
                    })
                    .catch(function(error) {
                        console.log(`Error creating "${city}": ${error.message || error};`);
                    }))
            }
        }
        return t.batch(queries);
    })
    .then(data => {
        console.log('Cities added successfully')
    })
    .catch(error => {
        console.log('ERROR!!')
        console.log(error)
    });