"use strict";

describe('mainCtrl', function () {

    var controller,
        ideliverySignInServiceMock,
        stateMock,
        notieMock;

    // Disable template caching
    beforeEach(module(function ($provide, $urlRouterProvider) {
        $provide.value('$ionicTemplateCache', function () {
        });
        $urlRouterProvider.deferIntercept();
    }));

    // Load the App Module
    beforeEach(module('idelivery'));

    // Instantiate the Controller and Mocks
    beforeEach(inject(function ($controller) {

        // mock ideliverySignInService
        ideliverySignInServiceMock = {
            signIn: jasmine.createSpyObj('signIn spy', ['then'])
        };

        // mock $state
        stateMock = jasmine.createSpyObj('$state spy', ['go']);

        // mock notie()
        notieMock = jasmine.createSpyObj('notie() spy', ['alert']);

        // instantiate mainCtrl
        controller = $controller('mainCtrl', {
            'notie': notieMock,
            '$state': stateMock,
            'ideliverySignInService': ideliverySignInServiceMock
        });
    }));

    describe('#doSignIn', function () {

        // Call doSignIn on the Controller
        beforeEach(inject(function () {
            controller.signInData.email = 'test@exemple.com';
            controller.signInData.password = 'password1';
            controller.doSignIn();
        }));

        it('should call signin on ideliverySignInService', function () {
            expect(ideliverySignInServiceMock.signIn).toHaveBeenCalledWith('test@exemple.com', 'password1');
        });

        describe('when the signin is executed,', function () {
            it('if successful, should change state to stores/produtcs', function () {

                // Mock the signin response from ideliverySignInService

                expect(stateMock.go).toHaveBeenCalledWith('app.stores');
            });

            it('if unsuccessful, should show a popup', function () {

                // Mock the signin response from ideliverySignInService

                expect(notieMock.alert).toHaveBeenCalled();
            });
        });
    })
});
