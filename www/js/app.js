(function () {
  angular
    .module("idelivery", [
      "ionic",
      "ngCordova",
      "ui.utils.masks",
      "idf.br-filters",
      "ui.mask",
      "rw.moneymask",
    ])
    .run(run)
    .config(config);

  run.$inject = ["$ionicPlatform", "$cordovaAppVersion", "$ionicPopup"];

  function run($ionicPlatform, $cordovaAppVersion, $ionicPopup) {
    
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        cordova.plugins.Keyboard.disableScroll(true);


        //START ONESIGNAL CODE
        //Remove this method to stop OneSignal Debugging 
        window.plugins.OneSignal.setLogLevel({logLevel: 6, visualLevel: 0});

        // Set your iOS Settings
        var iosSettings = {};
        iosSettings["kOSSettingsKeyAutoPrompt"] = false;
        iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;



        var notificationOpenedCallback = function (jsonData) {
          console.log(JSON.stringify(jsonData));
          $ionicPopup.show({
            title: jsonData.notification.payload.body,
            buttons: [
              {
                text: "OK",
                type: "button-push-color",
                onTap: function () {
                  return true;
                },
              },
            ],
          });
        };

        window.plugins.OneSignal.startInit(configIDL.ONESIGNAL_ID)
          .handleNotificationOpened(notificationOpenedCallback)
          .iOSSettings(iosSettings)
          .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
          .endInit();

        window.plugins.OneSignal.promptLocation();
        // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
        window.plugins.OneSignal.promptForPushNotificationsWithUserResponse(function(accepted) {
          console.log("User accepted notifications: " + accepted);
        });
        //END ONESIGNAL CODE

        try {
          cordova.plugins.diagnostic.isGpsLocationEnabled(
            function (enabled) {
              if (enabled) {
                //enabled
              } else {
                // // open location native location settings
                // cordova.plugins.diagnostic.switchToLocationSettings();
                function onRequestSuccess(success) {
                  console.log(
                    "Successfully requested accuracy: " + success.message
                  );
                }
                function onRequestFailure(error) {
                  console.error(
                    "Accuracy request failed: error code=" +
                      error.code +
                      "; error message=" +
                      error.message
                  );
                  if (
                    error.code !==
                    cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED
                  ) {
                    if (
                      window.confirm(
                        "Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?"
                      )
                    ) {
                      // open location native location settings using diagnostic plugin
                      cordova.plugins.diagnostic.switchToLocationSettings();
                    }
                  }
                }
                cordova.plugins.locationAccuracy.request(
                  onRequestSuccess,
                  onRequestFailure,
                  cordova.plugins.locationAccuracy
                    .REQUEST_PRIORITY_HIGH_ACCURACY
                );
              }
            },
            function (error) {
              console.error("The following error occurred: " + error);
            }
          );
        } catch (e) {
          console.log("Error");
        }

        if (ionic.Platform.isAndroid()) {
          window.FirebasePlugin.getToken(function (token) {
            console.log(token);
          });
        }

        if (ionic.Platform.isIOS()) {
          setTimeout(function () {
            navigator.splashscreen.hide();
          }, 100);
        }
      }

      if (window.cordova) {
        $cordovaAppVersion.getVersionNumber().then(function (version) {
          localStorage.setItem(
            "appVersion-" + configIDL.ESTABLISHMENT_SLUG,
            version
          );
        });
      }

      //Problema resolvido de StatusBar no ios11
      if (window.cordova && $cordovaKeyboard) {
        $cordovaKeyboard.hideAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
    $ionicPlatform.on("deviceready", function () {
      handleBranch();
    });

    $ionicPlatform.on("resume", function () {
      handleBranch();
    });

    function handleBranch() {
      // Branch initialization
      Branch.initSession().then(function (data) {
        if (data["+clicked_branch_link"]) {
          custom_data = JSON.parse(data["$custom_meta_tags"]);
          if (Util.getLocalToken()) {
            if (custom_data.establishment != undefined) {
              sharedProperties.setIsDelivery(true);
              sharedProperties.setIsPickUp(false);
              storesService
                .getStore(custom_data.establishment)
                .then(function (response) {
                  console.log(response);
                  localStorage.setItem(
                    "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                    JSON.stringify(response.data)
                  );
                  $state.go(
                    "app.products",
                    {},
                    {
                      reload: true,
                    }
                  );
                });
            }
          } else {
            if (custom_data.establishment != undefined) {
              sharedProperties.setIsDelivery(true);
              sharedProperties.setIsPickUp(false);
              storesService
                .getStore(custom_data.establishment)
                .then(function (response) {
                  console.log(response);
                  localStorage.setItem(
                    "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                    JSON.stringify(response.data)
                  );
                  $state.go(
                    "app.products",
                    {},
                    {
                      reload: true,
                    }
                  );
                });
            }
          }
          //  else {
          // $state.go('app.products', {}, {
          //     reload: true
          // });
          // }
        }
      });
    }
  }

  config.$inject = [
    "$stateProvider",
    "$urlRouterProvider",
    "$ionicConfigProvider",
    "$httpProvider",
  ];

  function config(
    $stateProvider,
    $urlRouterProvider,
    $ionicConfigProvider,
    $httpProvider
  ) {
    $ionicConfigProvider.backButton.text("").previousTitleText(false);

    $ionicConfigProvider.navBar.alignTitle("center");

    $ionicConfigProvider.scrolling.jsScrolling(false);

    $ionicConfigProvider.views.swipeBackEnabled(false);

    $httpProvider.interceptors.push("iDeliveryInterceptor");

    $stateProvider

      .state("app", {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: "AppCtrl",
      })

      .state("app.login", {
        url: "/login",
        cache: false,
        views: {
          menuContent: {
            templateUrl: "app/main/main.html",
            controller: "mainCtrl",
          },
        },
      })
      .state("app.favorites", {
        url: "/favorites",
        cache: false,
        views: {
          menuContent: {
            templateUrl: "app/stores/favorites.html",
            controller: "StoresCtrl",
          },
        },
      })

      .state("app.stores", {
        url: "/stores",
        views: {
          menuContent: {
            templateUrl: "app/stores/select-store.html",
            controller: "StoresCtrl",
          },
        },
      })

      .state("app.userProfile", {
        url: "/userProfile",
        views: {
          menuContent: {
            templateUrl: "app/userProfile/userProfile.html",
            controller: "userProfileCtrl",
          },
        },
      })

      .state("app.products", {
        url: "/products",
        views: {
          menuContent: {
            templateUrl: "app/products/products.html",
            controller: "productsCtrl",
          },
        },
      })

      .state("app.productCompositionWithLimit", {
        url: "/:productId/productCompositionWithLimit/:compositionId/:index",
        views: {
          menuContent: {
            templateUrl:
              "app/productComposition/productCompositionWithLimit.html",
            controller: "productCompositionWithLimitCtrl",
          },
        },
      })

      .state("app.productComposition", {
        url: "/:productId/productComposition/:compositionId/:index",
        views: {
          menuContent: {
            templateUrl: "app/productComposition/productComposition.html",
            controller: "productCompositionCtrl",
          },
        },
      })

      .state("app.preOrder", {
        url: "/preOrder",
        views: {
          menuContent: {
            templateUrl: "app/preOrder/preOrder.html",
            controller: "preOrderCtrl",
          },
        },
      })

      .state("app.cart", {
        url: "/cart",
        views: {
          menuContent: {
            templateUrl: "app/cart/cart.html",
            controller: "cartCtrl",
          },
        },
      })

      .state("app.speciePayment", {
        url: "/speciePayment",
        views: {
          menuContent: {
            templateUrl: "app/payment/speciePayment.html",
            controller: "paymentCtrl",
          },
        },
      })

      .state("app.onlinePayment", {
        url: "/onlinePayment",
        views: {
          menuContent: {
            templateUrl: "app/payment/onlinePayment.html",
            controller: "onlinePaymentCtrl",
          },
        },
      })
      .state("app.continueDealOrder", {
        url: "/continueDealOrder",
        views: {
          menuContent: {
            templateUrl: "app/payment/continueDealOrder.html",
            controller: "paymentCtrl",
          },
        },
      })
      .state("app.cardPayment", {
        url: "/cardPayment",
        views: {
          menuContent: {
            templateUrl: "app/payment/cardPayment.html",
            controller: "paymentCtrl",
          },
        },
      })

      .state("app.paymentForm", {
        url: "/paymentForm",
        views: {
          menuContent: {
            templateUrl: "app/payment/paymentForm.html",
            controller: "paymentCtrl",
          },
        },
      })

      .state("app.contact", {
        url: "/contact",
        views: {
          menuContent: {
            templateUrl: "app/contact/contact.html",
            controller: "contactCtrl",
          },
        },
      })

      .state("app.address", {
        url: "/address",
        views: {
          menuContent: {
            templateUrl: "app/address/address.html",
            controller: "addressCtrl",
          },
        },
      })

      .state("app.addressSearch", {
        url: "/addressSearch",
        views: {
          menuContent: {
            templateUrl: "app/address/addressSearch.html",
            controller: "addressSearchCtrl",
          },
        },
      })

      .state("app.addressSearchCEP", {
        url: "/addressSearchCEP",
        views: {
          menuContent: {
            templateUrl: "app/address/addressSearchCEP.html",
            controller: "addressSearchCEPCtrl",
          },
        },
      })

      .state("app.addressList", {
        url: "/addressList",
        views: {
          menuContent: {
            templateUrl: "app/address/addressList.html",
            controller: "addressListCtrl",
          },
        },
      })

      .state("app.region", {
        url: "/region",
        cache: false,
        views: {
          menuContent: {
            templateUrl: "app/address/region.html",
            controller: "addressCtrl",
          },
        },
      })

      .state("app.orders", {
        url: "/orders",
        cache: false,
        views: {
          menuContent: {
            templateUrl: "app/orders/orders.html",
            controller: "ordersCtrl",
          },
        },
      })

      .state("app.orderDetails", {
        url: "/orderDetails/:orderId",
        views: {
          menuContent: {
            templateUrl: "app/orders/orderDetails.html",
            controller: "orderDetailsCtrl",
          },
        },
      })

      .state("app.about", {
        url: "/about",
        views: {
          menuContent: {
            templateUrl: "app/about/about.html",
            controller: "aboutCtrl",
          },
        },
      })

      .state("app.ratings", {
        url: "/ratings",
        views: {
          menuContent: {
            templateUrl: "app/ratings/ratings.html",
            controller: "ratingsCtrl",
          },
        },
      });

    // if none of the above states are matched, use this as the fallback
    if (Util.getLocalToken() === null) {
      $urlRouterProvider.otherwise("/app/login");
    } else {
      if (Util.getStoresQuantity() > 1)
        $urlRouterProvider.otherwise("/app/stores");
      else $urlRouterProvider.otherwise("/app/products");
    }
  }
})();
