(function () {
  angular.module("idelivery").controller("mainCtrl", mainCtrl);

  mainCtrl.$inject = [
    "$scope",
    "$state",
    "$ionicModal",
    "ideliveryServices",
    "ideliverySignUpService",
    "ideliverySignInService",
    "$cordovaFacebook",
    "ideliveryFacebookService",
    "$ionicSideMenuDelegate",
    "storesService",
    "$ionicHistory",
    "$ionicLoading",
    "ideliveryAppleService",
  ];
  function mainCtrl(
    $scope,
    $state,
    $ionicModal,
    ideliveryServices,
    ideliverySignUpService,
    ideliverySignInService,
    $cordovaFacebook,
    ideliveryFacebookService,
    $ionicSideMenuDelegate,
    storesService,
    $ionicHistory,
    $ionicLoading,
    ideliveryAppleService
  ) {
    // localStorage.setItem(
    //   "productionServer-" + configIDL.ESTABLISHMENT_SLUG,
    //   "http://0.0.0.0:8000/api/"
    // ); //''
    // localStorage.setItem(
    //   "productionServer-" + configIDL.ESTABLISHMENT_SLUG,
    //   "https://dev-ws.ideliveryapp.com.br/api/mobile/"
    // );
    localStorage.setItem(
      "productionServer-" + configIDL.ESTABLISHMENT_SLUG,
      "https://idl-back-prod.herokuapp.com/api/mobile/"
    );
    localStorage.setItem(
      "listLayout-" + configIDL.ESTABLISHMENT_SLUG,
      configIDL.LIST_LAYOUT
    );

    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();

    $ionicSideMenuDelegate.canDragContent(false);

    $scope.doLoginLater = function () {
      storesService.getStores().then(function (response) {
        if (response.data.length > 1) {
          $state.go("app.stores");
          localStorage.setItem(
            "store_quantity-" + configIDL.ESTABLISHMENT_SLUG,
            response.data.length
          );
        } else {
          $state.go("app.products");
          var establishment = {
            id: response.data[0].id,
          };
          localStorage.setItem(
            "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
            JSON.stringify(establishment)
          );
        }
      });
    };
    $scope.doAppleLogin = function () {
      window.cordova.plugins.SignInWithApple.signin(
        { requestedScopes: [0, 1] },
        function (succ) {
          $scope.resultRequestApple = succ;

          var data = {
            resultRequestApple: $scope.resultRequestApple,
            slug: configIDL.ESTABLISHMENT_SLUG,
            email: $scope.resultRequestApple.email,
            step: 1,
          };

          ideliveryAppleService.signUp(data).then(
            function (response) {
              if (response.data.type == 1) {
                $scope.signUpData.email = response.data.email;
                $scope.signUpData.name = response.data.name;
                $scope.openAppleLoginPhoneModal();
              } else {
                var user = {
                  id: response.data.id,
                  name: response.data.user,
                  photo: response.data.photo,
                };

                localStorage.setItem(
                  "token_app-" + configIDL.ESTABLISHMENT_SLUG,
                  response.data.token
                );
                localStorage.setItem(
                  "user_data-" + configIDL.ESTABLISHMENT_SLUG,
                  JSON.stringify(user)
                );

                if (window.cordova) {
                  $scope.setupPushNotifications();
                }

                if (response.data.ids_establishment.length > 1) {
                  $state.go("app.stores");
                } else {
                  $state.go("app.products");
                  var establishment = {
                    id: response.data.ids_establishment[0],
                  };
                  localStorage.setItem(
                    "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                    JSON.stringify(establishment)
                  );
                }
              }
            },
            function () {
              notie.alert(
                3,
                "Não foi possível efetuar login. Tente Novamente.",
                3
              );
            }
          );
        },
        function (err) {
          console.error(err);
          console.log(JSON.stringify(err));
        }
      );
    };
    $scope.doFacebookLogin = function () {
      $cordovaFacebook.login(["public_profile", "email"]).then(
        function (success) {
          $scope.resultRequestFacebook = success;

          var data = {
            resultRequestFacebook: $scope.resultRequestFacebook,
            slug: configIDL.ESTABLISHMENT_SLUG,
            step: 1,
          };

          ideliveryFacebookService.signUp(data).then(
            function (response) {
              if (response.data.type == 1) {
                $scope.signUpData.email = response.data.email;
                $scope.openFacebookLoginPhoneModal();
              } else {
                var user = {
                  id: response.data.id,
                  name: response.data.user,
                  photo: response.data.photo,
                };

                localStorage.setItem(
                  "token_app-" + configIDL.ESTABLISHMENT_SLUG,
                  response.data.token
                );
                localStorage.setItem(
                  "user_data-" + configIDL.ESTABLISHMENT_SLUG,
                  JSON.stringify(user)
                );

                if (window.cordova) {
                  $scope.setupPushNotifications();
                }

                if (response.data.ids_establishment.length > 1) {
                  $state.go("app.stores");
                } else {
                  $state.go("app.products");
                  var establishment = {
                    id: response.data.ids_establishment[0],
                  };
                  localStorage.setItem(
                    "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                    JSON.stringify(establishment)
                  );
                }
              }
            },
            function () {
              notie.alert(
                3,
                "Não foi possível efetuar login. Tente Novamente.",
                3
              );
            }
          );
        },
        function () {
          notie.alert(3, "Não foi possível efetuar login. Tente Novamente.", 3);
        }
      );
    };

    ////////////////////////////////
    // Facebook Login Phone Modal //
    ////////////////////////////////

    $ionicModal
      .fromTemplateUrl("app/main/facebookLoginPhoneModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.facebookLoginPhoneModal = modal;
      });

    $scope.closeFacebookLoginPhoneModal = function () {
      $scope.facebookLoginPhoneModal.hide();
    };

    $scope.openFacebookLoginPhoneModal = function () {
      $scope.facebookLoginPhoneModal.show();
      document.getElementById("userPhoneFacebook").focus();
    };

    $ionicModal
      .fromTemplateUrl("app/main/appleLoginPhoneModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.appleLoginPhoneModal = modal;
      });

    $scope.closeAppleLoginPhoneModal = function () {
      $scope.appleLoginPhoneModal.hide();
    };

    $scope.openAppleLoginPhoneModal = function () {
      $scope.appleLoginPhoneModal.show();
      document.getElementById("userPhoneApple").focus();
    };

    $scope.sendAppleLoginPhone = function (signUpData) {
      var data = {
        resultRequestApple: $scope.resultRequestApple,
        slug: configIDL.ESTABLISHMENT_SLUG,
        name: signUpData.name,
        email: signUpData.email,
        userPhone: signUpData.userPhone,
        userPhone2: signUpData.userPhone2,
        useTerms: signUpData.useTerms,
        step: 2,
      };

      if (!signUpData.useTerms) {
        notie.alert(3, "Aceite os termos de uso para efetuar o cadastro.", 5);
      } else if (!signUpData.email) {
        notie.alert(3, "Insira um e-mail válido para efetuar o cadastro.", 5);
      } else {
        ideliveryAppleService.signUp(data).then(
          function (response) {
            if (response.status === 200) {
              var user = {
                id: response.data.id,
                name: response.data.user,
                photo: response.data.photo,
              };

              localStorage.setItem(
                "token_app-" + configIDL.ESTABLISHMENT_SLUG,
                response.data.token
              );
              localStorage.setItem(
                "user_data-" + configIDL.ESTABLISHMENT_SLUG,
                JSON.stringify(user)
              );

              if (window.cordova) {
                $scope.setupPushNotifications();
              }
              $scope.closeAppleLoginPhoneModal();
              if (response.data.ids_establishment.length > 1) {
                $state.go("app.stores");
              } else {
                $state.go("app.products");
                var establishment = {
                  id: response.data.ids_establishment[0],
                };
                localStorage.setItem(
                  "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                  JSON.stringify(establishment)
                );
              }

              $scope.closeAppleLoginPhoneModal();
            } else {
              notie.alert(3, response.data, 3);
            }
          },
          function () {
            notie.alert(
              3,
              "Não foi possível efetuar login. Tente Novamente.",
              3
            );
          }
        );
      }
    };

    // $scope.versionIos = ionic.Platform.isIOS();
    $scope.isIOS = ionic.Platform.isIOS();
    if (
      $scope.isIOS === ionic.Platform.isIOS() &&
      ionic.Platform.version() >= 13
    ) {
      var idApple = (document.getElementById("buttonApple").style.display =
        "block");
    } else {
      if (
        $scope.isIOS === ionic.Platform.isIOS() &&
        window.matchMedia("(min-width:768px)").matches
      ) {
        var idApple = (document.getElementById("buttonApple").style.display =
          "block");
      } else {
        // window width is greater than 570px
        var idApple = (document.getElementById("buttonApple").style.display =
          "none");
      }
    }

    $scope.sendFacebookLoginPhone = function (signUpData) {
      var data = {
        resultRequestFacebook: $scope.resultRequestFacebook,
        slug: configIDL.ESTABLISHMENT_SLUG,
        email: signUpData.email,
        userPhone: signUpData.userPhone,
        userPhone2: signUpData.userPhone2,
        useTerms: signUpData.useTerms,
        step: 2,
      };

      if (!signUpData.useTerms) {
        notie.alert(3, "Aceite os termos de uso para efetuar o cadastro.", 5);
      } else if (!signUpData.email) {
        notie.alert(3, "Insira um e-mail válido para efetuar o cadastro.", 5);
      } else {
        ideliveryFacebookService.signUp(data).then(
          function (response) {
            if (response.status === 200) {
              var user = {
                id: response.data.id,
                name: response.data.user,
                photo: response.data.photo,
              };

              localStorage.setItem(
                "token_app-" + configIDL.ESTABLISHMENT_SLUG,
                response.data.token
              );
              localStorage.setItem(
                "user_data-" + configIDL.ESTABLISHMENT_SLUG,
                JSON.stringify(user)
              );

              if (window.cordova) {
                $scope.setupPushNotifications();
              }

              if (response.data.ids_establishment.length > 1) {
                $state.go("app.stores");
              } else {
                $state.go("app.products");
                var establishment = {
                  id: response.data.ids_establishment[0],
                };
                localStorage.setItem(
                  "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                  JSON.stringify(establishment)
                );
              }

              $scope.closeFacebookLoginPhoneModal();
            } else {
              notie.alert(3, response.data, 3);
            }
          },
          function () {
            notie.alert(
              3,
              "Não foi possível efetuar login. Tente Novamente.",
              3
            );
          }
        );
      }
    };

    $scope.$on("$destroy", function () {
      $scope.facebookLoginPhoneModal.remove();
      $scope.appleLoginPhoneModal.remove();
    });

    ///////////////////////////
    // Forgot Password Modal //
    ///////////////////////////

    $scope.forgotPassword = {};

    $scope.recoveryPasswordEmail = null;
    $ionicModal
      .fromTemplateUrl("app/main/forgotPasswordModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.forgotPasswordModal = modal;
      });

    $scope.closeForgotPasswordModal = function () {
      $scope.forgotPasswordModal.hide();
    };

    $scope.openForgotPasswordModal = function () {
      $scope.forgotPasswordModal.show();
    };

    $scope.sendRecoveryPasswordEmail = function (email) {
      ideliverySignInService
        .userRecoveryPasswordEmail(email, configIDL.ESTABLISHMENT_SLUG)
        .then(function (response) {
          if (response.status == 200) {
            notie.alert(1, response.data.message, 5);
            $scope.closeForgotPasswordModal();
            $scope.closeSignInModal();
          } else {
            notie.alert(3, response.data.message, 5);
          }
        })
        .catch(function (e) {
          notie.alert(3, e.data, 5);
        })
        .then(function () {
          $ionicLoading.hide();
        });
    };

    $scope.$on("$destroy", function () {
      $scope.forgotPasswordModal.remove();
      $scope.signUpModal.remove();
    });

    ///////////////////
    // Modal Sign Up //
    ///////////////////

    $ionicModal
      .fromTemplateUrl("app/main/signupModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.signUpModal = modal;
      });

    $scope.closeSignUpModal = function () {
      $scope.signUpModal.hide();
    };

    $scope.openSignUpModal = function () {
      $scope.signUpModal.show();
    };

    // Cleaning up modals on destroy
    $scope.$on("$destroy", function () {
      $scope.forgotPasswordModal.remove();
      $scope.signUpModal.remove();
    });

    $scope.signUpData = {};
    $scope.password = {};

    $scope.doSignUp = function () {
      if (!$scope.signUpData.useTerms) {
        notie.alert(3, "Aceite os termos de uso para efetuar o cadastro.", 3);
      } else {
        $scope.signUpData.slug = configIDL.ESTABLISHMENT_SLUG;
        if ($scope.signUpData.password === $scope.password.confirmPassword) {
          ideliverySignUpService.signUp($scope.signUpData).then(
            function (response) {
              if (response.status === 200) {
                var user = {
                  id: response.data.id,
                  name: response.data.user,
                  photo: response.data.photo,
                };

                localStorage.setItem(
                  "token_app-" + configIDL.ESTABLISHMENT_SLUG,
                  response.data.token
                );
                localStorage.setItem(
                  "user_data-" + configIDL.ESTABLISHMENT_SLUG,
                  JSON.stringify(user)
                );

                if (window.cordova) {
                  $scope.setupPushNotifications();
                }

                if (response.data.ids_establishment.length > 1) {
                  $state.go("app.stores");
                } else {
                  $state.go("app.products");
                  var establishment = {
                    id: response.data.ids_establishment[0],
                  };
                  localStorage.setItem(
                    "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                    JSON.stringify(establishment)
                  );
                }

                $scope.signUpModal.hide();
              } else {
                notie.alert(3, response.data, 3);
              }
            },
            function (error) {
              notie.alert(3, error, 3);
            }
          );
        } else {
          notie.alert(3, "Senha e Confirmar Senha estão diferentes", 3);
        }
      }
    };

    ///////////////////
    // Modal Sign In //
    ///////////////////

    $ionicModal
      .fromTemplateUrl("app/main/signinModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.signInModal = modal;
      });

    $scope.closeSignInModal = function () {
      $scope.signInData = {};
      $scope.signInModal.hide();
    };

    $scope.openSignInModal = function () {
      $scope.signInModal.show();
    };

    // Cleaning up modals on destroy
    $scope.$on("$destroy", function () {
      $scope.forgotPasswordModal.remove();
      $scope.signInModal.remove();
    });

    $scope.signInData = {};

    $scope.doSignIn = function () {
      $scope.signInData.slug = configIDL.ESTABLISHMENT_SLUG;
      ideliverySignInService.signIn($scope.signInData).then(
        function (response) {
          if (response.status === 200) {
            var user = {
              id: response.data.id,
              name: response.data.user,
              photo: response.data.photo,
            };

            localStorage.setItem(
              "token_app-" + configIDL.ESTABLISHMENT_SLUG,
              response.data.token
            );
            localStorage.setItem(
              "user_data-" + configIDL.ESTABLISHMENT_SLUG,
              JSON.stringify(user)
            );

            if (window.cordova) {
              // window.ga.setUserId(response.data.token);
              $scope.setupPushNotifications();
            }

            if (response.data.ids_establishment.length > 1) {
              $state.go("app.stores");
            } else {
              $state.go("app.products");
              var establishment = {
                id: response.data.ids_establishment[0],
              };
              localStorage.setItem(
                "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
                JSON.stringify(establishment)
              );
            }

            $scope.signInModal.hide();
          } else {
            notie.alert(3, response.data, 3);
          }
        },
        function (error) {
          notie.alert(3, error, 3);
        }
      );
    };

    /////////////////
    // Modal Terms //
    /////////////////

    $ionicModal
      .fromTemplateUrl("app/main/termsModal.html", {
        scope: $scope,
      })
      .then(function (modal) {
        $scope.termsModal = modal;
      });

    $scope.closeTermsModal = function () {
      $scope.termsModal.hide();
    };

    $scope.openTermsModal = function () {
      $scope.termsModal.show();
    };

    // Cleaning up modals on destroy
    $scope.$on("$destroy", function () {
      $scope.termsModal.remove();
    });
  }
})();
