var gulp = require('gulp'),
    gutil = require('gulp-util'),
    bower = require('bower'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-minify-css'),
    sh = require('shelljs');

gulp.task('default', ['sass']);

gulp.task('serve:before', ['default', 'watch']);

gulp.task('sass', function () {
    return gulp.src('www/css/*.scss')
        .pipe(sass())
        .pipe(minifyCss({keepSpecialComments: false}))
        .pipe(gulp.dest('www/css'));
});

gulp.task('watch', function () {
    gulp.watch('www/css/*.scss', ['sass']);
});

gulp.task('install', ['git-check'], function () {
    return bower.commands.install()
        .on('log', function (data) {
            gutil.log('bower', gutil.colors.cyan(data.id), data.message);
        });
});

gulp.task('git-check', function (done) {
    if (!sh.which('git')) {
        console.log(
            '  ' + gutil.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required to download Ionic.',
            '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});
