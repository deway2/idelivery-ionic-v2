(function () {
  "use strict";

  angular.module("idelivery").controller("productsCtrl", productsCtrl);

  productsCtrl.$inject = [
    "$scope",
    "ideliveryServices",
    "ideliverySignInService",
    "$state",
    "sharedProperties",
    "productCompositionService",
    "$ionicModal",
    "$ionicPopup",
    "$ionicHistory",
    "$cordovaGeolocation",
    "ideliverySearchAddressService",
    "$ionicScrollDelegate",
    "$ionicLoading",
    "$location",
    "$http",
  ];
  function productsCtrl(
    $scope,
    ideliveryServices,
    ideliverySignInService,
    $state,
    sharedProperties,
    productCompositionService,
    $ionicModal,
    $ionicPopup,
    $ionicHistory,
    $cordovaGeolocation,
    ideliverySearchAddressService,
    $ionicScrollDelegate,
    $ionicLoading,
    $location,
    $http
  ) {
    $scope.$on("$ionicView.beforeEnter", function () {
      var currentDevicePlatform = {
        name: ionic.Platform.platform(),
        version: ionic.Platform.version(),
      };

      if (
        currentDevicePlatform.name === "android" &&
        currentDevicePlatform.version <= 4.3
      ) {
        $scope.productsLayout = "list";
        $scope.isOldAndroid = true;
      } else {
        $scope.productsLayout = localStorage.getItem(
          "listLayout-" + configIDL.ESTABLISHMENT_SLUG
        );
      }

      $scope.currCat = localStorage.getItem(
        "currentCategory-" + configIDL.ESTABLISHMENT_SLUG
      );
      if (configIDL.ESTABLISHMENT_SLUG == "betapizzaria.deway.com.br") {
        $scope.isMarketPlace = true;
      }
      configIDL.ESTABLISHMENT_ID = sharedProperties.getEstablishmentId();

      $scope.currentAddress = JSON.parse(
        localStorage.getItem("deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG)
      );

      if ($scope.currentAddress) {
        if ($scope.currentAddress.complement)
          $scope.complement = ", " + $scope.currentAddress.complement;
      }

      sharedProperties
        .getObject()
        .data.splice(0, sharedProperties.getObject().data.length);
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
    });

    $scope.$on("$ionicView.enter", function () {
      ideliveryServices
        .getProductCategories(configIDL.ESTABLISHMENT_ID)
        .then(function (response) {
          $scope.allProducts = [];
          $scope.fuseResult = [];

          if (!$scope.productsCategories) {
            $scope.productsCategories = response.data;
            if ($scope.productsCategories[$scope.currCat])
              $scope.shownCategory = $scope.productsCategories[$scope.currCat];
            else if (
              $scope.productsCategories[0] &&
              configIDL.OPEN_FIRST_CATEGORY
            )
              $scope.shownCategory = $scope.productsCategories[0];

            $scope.createOrUpdateAllProductsList($scope.productsCategories);
          } else {
            var productsLocal = JSON.stringify($scope.productsCategories);
            var productsServer = JSON.stringify(response.data);

            if (productsLocal !== productsServer) {
              $scope.productsCategories = response.data;
              if ($scope.productsCategories[$scope.currCat])
                $scope.shownCategory =
                  $scope.productsCategories[$scope.currCat];
              else if (
                $scope.productsCategories[0] &&
                configIDL.OPEN_FIRST_CATEGORY
              )
                $scope.shownCategory = $scope.productsCategories[0];

              $scope.createOrUpdateAllProductsList($scope.productsCategories);
            } else {
              $scope.createOrUpdateAllProductsList($scope.productsCategories);
            }
          }
        });

      $scope.cart = sharedProperties.getCart().data;

      $scope.getEstablismentDetails();
      $scope.currentUser = JSON.parse(Util.getCurrentUserData());
      var getFavorite = {
        establishment: $scope.restaurant.id,
        user: $scope.currentUser,
      };
      if ($scope.isMarketPlace) {
        ideliveryServices.getFavorite(getFavorite).then(function (response) {
          $scope.isFavorite = response.data.data.isFavorite;
        });
      }
    });

    // Order Function

    $scope.startOrder = function (productId) {
      $ionicLoading.show({
        template:
          '<ion-spinner icon="circles" class="idl-spinner"></ion-spinner>',
      });

      if (!Util.getLocalToken()) {
        $ionicLoading.hide();
        var confirmLoginPopup = $ionicPopup.confirm({
          title: "Você precisa estar logado para iniciar um pedido.",
          template: "Deseja efetuar login?",
          buttons: [
            {
              text: "Cancelar",
              type: "button-light",
            },
            {
              text: "Efetuar Login",
              type: "button-positive",
              onTap: function () {
                return true;
              },
            },
          ],
        });
        confirmLoginPopup.then(function (res) {
          if (res) {
            $state.go("app.login");
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
          }
        });
      } else if (
        !$scope.currentAddress ||
        $scope.currentAddress == "" ||
        $scope.currentAddress == null
      ) {
        ideliverySearchAddressService.getAddress().then(function (response) {
          $ionicLoading.hide();

          if (response.data.address.length > 0) {
            var selectAddressPopup = $ionicPopup.confirm({
              title: "Você não selecionou nenhum endereço.",
              template: "Deseja selecionar um dos Seus Endereços?",
              buttons: [
                {
                  text: "Cancelar",
                  type: "button-light",
                },
                {
                  text: "Sim",
                  type: "button-positive",
                  onTap: function () {
                    return true;
                  },
                },
              ],
            });
            selectAddressPopup.then(function (res) {
              if (res) {
                $state.go("app.addressList");
              }
            });
          } else {
            $ionicLoading.hide();
            var confirmAddressPopup = $ionicPopup.confirm({
              title: "Você não possui nenhum endereço cadastrado. :(",
              template: "Deseja cadastrar um endereço?",
              buttons: [
                {
                  text: "Ainda não",
                  type: "button-light",
                },
                {
                  text: "Cadastrar Endereço",
                  type: "button-positive",
                  onTap: function () {
                    return true;
                  },
                },
              ],
            });
            confirmAddressPopup.then(function (res) {
              if (res) {
                $state.go("app.address");
              }
            });
          }
        });
      } else {
        ideliverySearchAddressService.getAddress().then(function (response) {
          for (var i = 0; i < response.data.address.length; i++) {
            if (response.data.address[i].id === $scope.currentAddress.id) {
              $scope.currentAddress = response.data.address[i];
              break;
            }
          }

          ideliveryServices.getRestaurantStatus().then(function (r) {
            $scope.restaurantStatus = r.data.isOpen;

            if ($scope.restaurantStatus) {
              $scope.restaurantStatusLabel = "ABERTO";
            } else {
              $scope.restaurantStatusLabel = "FECHADO";
            }

            if ($scope.currentAddress.attending_status.active) {
              if (!$scope.restaurantStatus) {
                $ionicLoading.hide();
                notie.alert(
                  2,
                  "Estamos Fechados. Não atendemos neste horário. :(",
                  5
                );
              } else {
                ideliveryServices.getProducts(productId).then(function (res) {
                  $scope.products = res.data;
                  if ($scope.products.category_supplements.length != 0) {
                    var compositionId = $scope.products.category_supplements[0];
                    ideliveryServices
                      .getCompositionCategory(compositionId)
                      .then(function (res) {
                        $scope.currentComposition = res.data;
                        if ($scope.currentComposition.max > 1) {
                          $ionicLoading.hide();
                          productCompositionService.setProduct($scope.products);
                          $state.go("app.productCompositionWithLimit", {
                            productId: productId,
                            compositionId: compositionId,
                            index: 1,
                          });
                        } else {
                          $ionicLoading.hide();
                          productCompositionService.setProduct($scope.products);
                          $state.go("app.productComposition", {
                            productId: productId,
                            compositionId: compositionId,
                            index: 1,
                          });
                        }
                      });
                  } else {
                    $ionicLoading.hide();
                    if ($scope.products.price_in_supplement) {
                      $scope.products.price = 0;
                      productCompositionService.setProduct($scope.products);
                    } else {
                      productCompositionService.setProduct($scope.products);
                    }
                    $state.go("app.preOrder");
                  }
                });
              }
            } else {
              //notie().alert(2, $scope.currentAddress.attending_status.message, 5);
              var selectAnotherAddressPopup = $ionicPopup.confirm({
                title: $scope.currentAddress.attending_status.message,
                buttons: [
                  {
                    text: "Cancelar",
                    type: "button-light",
                  },
                  {
                    text: "Trocar Endereço",
                    type: "button-positive",
                    onTap: function () {
                      return true;
                    },
                  },
                ],
              });
              selectAnotherAddressPopup.then(function (res) {
                if (res) {
                  $state.go("app.addressList");
                }
              });
            }
          });
        });
      }
    };

    // Establishment Info Function

    $scope.launchNav = function (restaurant) {
      launchnavigator.navigate(
        restaurant.latitude + "," + restaurant.longitude,
        function () {
          console.log("Ok Localizacao:");
        },
        function (error) {
          console.log("not nice");
        },
        {
          preferGoogleMaps: true,
          transportMode: "transit",
          enableDebug: true,
          disableAutoGeolocation: true,
        }
      );
    };

    $scope.getEstablismentDetails = function () {
      ideliveryServices.getRestaurantDetails().then(function (response) {
        $scope.restaurant = response.data;
        $scope.phones = JSON.parse($scope.restaurant.phonesJson);

        $scope.restaurantStatus = $scope.restaurant.is_open;
        $scope.opensMessage = response.data.opens_message;

        var restaurant = $scope.restaurant;
        $scope.formatedMapLink =
          restaurant.street +
          ", " +
          restaurant.number +
          ", " +
          restaurant.neighborhood +
          ", " +
          restaurant.city.name +
          " - " +
          restaurant.state.name;

        $scope.isIOS = ionic.Platform.isIOS() || ionic.Platform.isIPad();

        if ($scope.restaurantStatus) {
          $scope.restaurantStatusLabel = "ABERTO";
        } else {
          $scope.restaurantStatusLabel = "FECHADO";
        }

        localStorage.setItem(
          "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
          JSON.stringify($scope.restaurant)
        );

        var posOptions = {
          timeout: 5000,
          enableHighAccuracy: true,
        };

        $cordovaGeolocation
          .getCurrentPosition(posOptions)
          .then(function (position) {
            var lat = position.coords.latitude;
            var long = position.coords.longitude;
            var distance = Util.getDistanceFromLatLngInKm(
              lat,
              long,
              $scope.restaurant.latitude,
              $scope.restaurant.longitude
            );

            $scope.restaurantDistance = distance.toFixed() + " km";
          });
      });
    };
    $scope.favorite = function (isfavor) {
      var dataFavorite = {
        establishment: $scope.restaurant.id,
        user: $scope.currentUser.id,
        isFavorite: isfavor,
      };
      ideliveryServices.setFavorite(dataFavorite).then(function (response) {
        $scope.isFavorite = response.data.data.isFavorite;
        console.log(response.data);
      });
    };
    // Pull to Refresh Function

    $scope.doRefresh = function () {
      $scope.getEstablismentDetails();

      console.log($scope.restaurantStatus);

      if ($scope.restaurantStatus) {
        $scope.restaurantStatusLabel = "ABERTO";
      } else {
        $scope.restaurantStatusLabel = "FECHADO";
      }

      ideliveryServices
        .getProductCategories()
        .then(function (response) {
          $scope.productsCategories = response.data;
          if ($scope.productsCategories[$scope.currCat])
            $scope.shownCategory = $scope.productsCategories[$scope.currCat];
          else if (
            $scope.productsCategories[0] &&
            configIDL.OPEN_FIRST_CATEGORY
          )
            $scope.shownCategory = $scope.productsCategories[0];
        })
        .finally(function () {
          // Stop the ion-refresher from spinning
          $scope.$broadcast("scroll.refreshComplete");
        });
    };

    ///// Modals /////

    // Maps Modal
    $ionicModal
      .fromTemplateUrl("app/products/iosMapsModal.html", {
        scope: $scope,
        animation: "slide-in-up",
      })
      .then(function (modal) {
        $scope.modal = modal;
      });

    $scope.openModal = function () {
      $scope.modal.show();
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
    };

    // Establishment Details Modal
    $ionicModal
      .fromTemplateUrl("app/products/restaurantDetailsModal.html", {
        scope: $scope,
        animation: "slide-in-right",
      })
      .then(function (restaurantDetailsModal) {
        $scope.restaurantDetailsModal = restaurantDetailsModal;
      });

    $scope.closeRestaurantDetails = function () {
      $scope.restaurantDetailsModal.hide();
    };

    $scope.openRestaurantDetails = function () {
      $scope.getEstablismentDetails();
      $scope.restaurantDetailsModal.show();
    };

    // Establishment Contact Modal
    $ionicModal
      .fromTemplateUrl("app/products/contactRestaurant.html", {
        scope: $scope,
        animation: "slide-in-up",
      })
      .then(function (contactRestaurant) {
        $scope.contactRestaurant = contactRestaurant;
      });

    $scope.opencontactRestaurant = function () {
      $scope.contactRestaurant.show();
      ideliveryServices.getRestaurantDetails().then(function (response) {
        $scope.nameRestaurant = response.data.name;
        $scope.emailRestaurant = response.data.email;
        $scope.imgRestaurant = response.data.image_url;
        console.log($scope.imgRestaurant);
      });
      //console.log($scope.userProfileData);
      $scope.currentUser = JSON.parse(Util.getCurrentUserData());
      ideliveryServices
        .getUserProfile($scope.currentUser.id)
        .then(function (response) {
          $scope.emailclientprofile = response.data.email;
          $scope.nameclientprofile = response.data.full_name;
          $scope.phoneclientprofile = response.data.phone;
          console.log($scope.phoneclientprofile);
        });
      $scope.SelectEmoji = {
        selectEmoji: "DEFAULT",
      };
      $scope.getElementEmojiContact = function (item) {
        console.log("Selecionado: ", item.value);
        $scope.valueSelect = item.value;
        getEmojiContact();
      };
      console.log($scope.valueSelect);

      $scope.EmojisOptions = [
        {
          text: "1",
          value: "RUIM",
          classname: "user-choose icon fa fa-frown-o cc-selector",
        },
        {
          text: "2",
          value: "REGULAR",
          classname: "user-choose icon fa fa-meh-o cc-selector",
        },
        {
          text: "3",
          value: "ÓTIMO",
          classname: "user-choose icon fa fa-smile-o cc-selector",
        },
      ];
    };
    function clearform() {
      // Clean the form
      // $("#name").val("");
      // $("#contactEmail").val("");
      $("#contactMessage").val("");
    }
    $scope.closecontactRestaurant = function () {
      $scope.contactRestaurant.hide();
    };

    function getEmojiContact() {
      console.log($scope.valueSelect);
      var result = $scope.valueSelect;
      console.log(result);
    }
    $scope.sendemail = function submitForm() {
      $ionicLoading.show({
        template:
          '<ion-spinner icon="circles" class="idl-spinner"></ion-spinner>',
      });

      var imgRestaurant = $scope.imgRestaurant;
      var emoji = $scope.valueSelect;
      var name = $scope.nameclientprofile;
      var contactPhone = $scope.phoneclientprofile;
      var contactEmail = $scope.emailclientprofile;
      var contactMessage = document.getElementById("contactMessage").value;
      var nameRestaurant = $scope.nameRestaurant;
      var emailRestaurant = $scope.emailRestaurant;
      // console.log(emailFaleconosco);
      // console.log(emailtestemunho);
      // console.log(emoji);
      if (typeof emoji == "undefined") {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Escolha uma das imagens acima.",
          showConfirmButton: false,
          timer: 1500,
        });
      } else if (contactMessage == "") {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Digite sua mensagem",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        console.log("tudo foi preenchido!");
      }

      var dataString =
        "name=" +
        name +
        "&contactEmail=" +
        contactEmail +
        "&contactMessage=" +
        contactMessage +
        "&nameRestaurant=" +
        nameRestaurant +
        "&emailRestaurant=" +
        emailRestaurant +
        "&emoji=" +
        emoji +
        "&imgRestaurant=" +
        imgRestaurant +
        "&contactPhone=" +
        contactPhone;

      if (name.length > 1 && contactEmail != "" && contactMessage != "") {
        $http
          .post(
            "https://www.deway.com.br/sendEmailiDelivery/sendmail_pt_br.php/",
            dataString,
            { headers: { "Content-Type": "application/x-www-form-urlencoded" } }
          )
          .then(
            function (response) {
              clearform();
              $ionicLoading.hide();
              Swal.fire({
                position: "center",
                icon: "success",
                title: "Enviado com sucesso!",
                showConfirmButton: false,
                timer: 1500,
              });
            },
            function (error) {
              $ionicLoading.hide();
              // swal("SENDI 2020", "Sua mensagem não foi enviada", "error")
              clearform();
              Swal.fire(
                "iDelivery",
                "Sua mensagem foi enviada com sucesso!",
                "success"
              );
            }
          );
      } else {
        $ionicLoading.hide();
        //noinspection JSUnresolvedFunction
        $(".error").fadeIn(1000);
        //noinspection JSUnresolvedFunction
        $(".success").fadeOut(500);
      }
      return false;
    };

    // finish Contact Modal

    // Search Modal
    $ionicModal
      .fromTemplateUrl("app/products/searchModal.html", {
        scope: $scope,
        animation: "slide-in-up",
      })
      .then(function (modal) {
        $scope.searchModal = modal;
      });

    $scope.openSearchModal = function () {
      $scope.searchModal.show();
    };
    $scope.closeSearchModal = function (productId) {
      $scope.pattern = "";

      if (productId) {
        $scope.startOrder(productId);
      }

      $scope.searchModal.hide();
    };

    ///// End Modals /////

    ///// Functions /////

    //Switch Products Layout: Grid or List
    $scope.switchProductsLayout = function () {
      $scope.productsLayout =
        $scope.productsLayout === "list" ? "grid" : "list";

      if ($scope.productsLayout !== "list") {
        localStorage.setItem(
          "listLayout-" + configIDL.ESTABLISHMENT_SLUG,
          "grid"
        );
      } else {
        localStorage.setItem(
          "listLayout-" + configIDL.ESTABLISHMENT_SLUG,
          "list"
        );
      }
    };

    // Update allProducts list Function
    $scope.createOrUpdateAllProductsList = function (category) {
      for (var i = category.length - 1; i >= 0; i--) {
        if (category[i].products.length > 0) {
          for (var j = category[i].products.length - 1; j >= 0; j--) {
            $scope.allProducts.push(category[i].products[j]);
            $scope.fuseResult = $scope.allProducts;
          }
        }
      }
    };

    $scope.isAndroid = function () {
      return ionic.Platform.isAndroid();
    };

    $scope.isIOS = function () {
      return ionic.Platform.isIOS();
    };

    $scope.openCart = function () {
      $state.go("app.cart");
    };

    $scope.openMap = function (address) {
      ideliveryServices.openMap(address);
    };

    $scope.openPhone = function (phone) {
      function regexPhone(p) {
        return p.replace(/\D/g, "");
      }

      ideliveryServices.openPhone(regexPhone(phone));
    };

    $scope.openAddress = function () {
      $state.go("app.addressList");
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
    };

    $scope.changeAddress = function () {
      if (Util.getLocalToken()) $state.go("app.addressList");
      else $state.go("app.region");
    };

    /*
     * if given category is the selected category, deselect it
     * else, select the given category
     */
    $scope.toggleCategory = function (category, index) {
      if ($scope.isCategoryShown(category)) {
        $scope.shownCategory = null;
      } else {
        $scope.shownCategory = category;
        $scope.scrollToCategory(category.id);

        localStorage.setItem(
          "currentCategory-" + configIDL.ESTABLISHMENT_SLUG,
          index
        );
      }
    };

    $scope.isCategoryShown = function (category) {
      return $scope.shownCategory === category;
    };
    /*
     * end
     */

    $scope.toggleSchedule = function (shift) {
      if (shift === 0) return "Fechado";
      else if (shift === 3) return "24 horas";
    };

    $scope.showSchedule = configIDL.SHOW_SCHEDULE;

    //Ng-Focus

    $scope.scrollOnInputFocus = function () {
      $ionicScrollDelegate.scrollBy(0, 150, true);
    };

    //Scroll to opened category

    $scope.scrollToCategory = function (categoryId) {
      $location.hash("category" + categoryId);
      $ionicScrollDelegate.anchorScroll();
    };

    //Filter/Search remove accents function

    $scope.searchFn = function (actual, expected) {
      if (angular.isObject(actual)) return false;

      function removeAccents(value) {
        return value
          .toString()
          .replace(/[áãâà]/g, "a")
          .replace(/[éê]/g, "e")
          .replace(/í/g, "i")
          .replace(/[óôõ]/g, "o")
          .replace(/ú/g, "u")
          .replace(/ñ/g, "n");
      }

      actual = removeAccents(angular.lowercase("" + actual));
      expected = removeAccents(angular.lowercase("" + expected));

      return actual.indexOf(expected) !== -1;
    };

    $scope.pattern = "";

    $scope.searchResult = function (pattern, list) {
      var options = {
        shouldSort: true,
        findAllMatches: true,
        threshold: 0.3,
        location: 0,
        distance: 1000,
        maxPatternLength: 32,
        keys: ["name", "description"],
      };

      var fuse = new Fuse(list, options);

      if (!pattern || pattern === "") {
        $scope.fuseResult = $scope.allProducts;
      } else {
        $scope.fuseResult = fuse.search(pattern);
      }
    };
  }
})();
