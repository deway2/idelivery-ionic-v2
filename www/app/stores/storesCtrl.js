(function () {
  angular.module("idelivery").controller("StoresCtrl", StoresCtrl);

  StoresCtrl.$inject = [
    "$scope",
    "$ionicLoading",
    "$state",
    "sharedProperties",
    "storesService",
    "$ionicHistory",
    "$cordovaGeolocation",

    "RESOURCES",
    "$ionicPopup",
  ];
  function StoresCtrl(
    $scope,
    $ionicLoading,
    $state,
    sharedProperties,

    storesService,
    $ionicHistory,
    $cordovaGeolocation,

    RESOURCES,
    $ionicPopup
  ) {
    $scope.$on("$ionicView.beforeEnter", function () {
      $ionicHistory.clearHistory();

      $scope.noEstablishments = false;
      $scope.keywords = {
        data: "",
      };
      $scope.searchFailed = null;
      $scope.isDelivery = true;
      $scope.isPickUp = false;
      $scope.selectedAddress = "";

      $scope.currentNeighborhoodId = "";

      $scope.stores = [];
      $scope.isMarketPlace = null;
      if (
        configIDL.ESTABLISHMENT_SLUG == "betapizzaria.deway.com.br" ||
        configIDL.ESTABLISHMENT_SLUG == "br.com.ideliveryapp.chicodocarangueijo"
      ) {
        $scope.isMarketPlace = true;
      }
      $scope.establishmentName = configIDL.ESTABLISHMENT_NAME;

      var currentAddress = JSON.parse(
        localStorage.getItem("deliveryAddress-" + configIDL.ESTABLISHMENT_SLUG)
      );
      var currentRegion = JSON.parse(
        localStorage.getItem("region-" + configIDL.ESTABLISHMENT_SLUG)
      );
      $scope.currentAddress = {};

      if (currentRegion && currentRegion.neighborhood.id) {
        $scope.selectedAddress =
          currentRegion.neighborhood.name +
          ", " +
          currentRegion.city +
          " - " +
          currentRegion.state;
        $scope.currentNeighborhoodId = currentRegion.neighborhood.id;
        $scope.currentAddress = currentRegion;
      } else if (currentAddress && currentAddress.neighborhood.id) {
        $scope.selectedAddress =
          currentAddress.neighborhood.name +
          ", " +
          currentAddress.city +
          " - " +
          currentAddress.state;
        $scope.currentNeighborhoodId = currentAddress.neighborhood.id;
        $scope.currentAddress = currentAddress;
      }
    });

    $scope.handleRestaurantStatusName = function (status) {
      return status ? "Aberto" : "Fechado";
    };

    $scope.$on("$ionicView.enter", function () {
      if ($scope.isMarketPlace && window.location.hash == "#/app/favorites") {
        $scope.currentUser = JSON.parse(Util.getCurrentUserData());
        $scope.getAllFavorites();
      }
      if ($scope.selectedAddress) {
        if ($scope.isDelivery && !$scope.isPickUp) {
          $scope.isDelivery = true;
          $scope.isPickUp = false;
          sharedProperties.setIsDelivery($scope.isDelivery);
          sharedProperties.setIsPickUp($scope.isPickUp);
          $scope.getAllStoresNotAccepted();
          if ($scope.currentNeighborhoodId) {
            $scope.getStoresByNeighborhood($scope.currentNeighborhoodId);
          } else {
            $scope.locateMe();
          }
        }
        if ($scope.isPickUp && !$scope.isDelivery) {
          $scope.isDelivery = false;
          $scope.isPickUp = true;
          sharedProperties.setIsDelivery($scope.isDelivery);
          sharedProperties.setIsPickUp($scope.isPickUp);
        }
      }
      if (!$scope.selectedAddress) {
        $scope.locateMe();
      }
    });
    $scope.goToStores = function () {
      $state.go("app.stores");
      $ionicHistory.clearCache();
    };
    $scope.getAllFavorites = function () {
      var favoriteData = {
        user: $scope.currentUser.id,
      };
      storesService
        .allFavoriteEstablishments(favoriteData)
        .success(function (response) {
          $ionicLoading.show({
            template: RESOURCES.TEMPLATE_LOADING,
          });

          var posOptions = {
            timeout: 5000,
            enableHighAccuracy: true,
          };
          if (!response.success) {
            $scope.noFavEstablishments = true;
          }
          $cordovaGeolocation.getCurrentPosition(posOptions).then(
            function (position) {
              var stores = response.data;
              var lat = position.coords.latitude;
              var long = position.coords.longitude;

              if (lat) {
                for (var i = 0; i < response.data.length; i++) {
                  var distance = Util.getDistanceFromLatLngInKm(
                    lat,
                    long,
                    stores[i].establishment.latitude,
                    stores[i].establishment.longitude
                  );
                  if (distance) {
                    stores[i].establishment.distance = distance.toFixed(1);
                  }
                }

                $scope.favoriteStores = stores.sort(function (a, b) {
                  var _a = Number(a.establishment.distance);
                  var _b = Number(b.establishment.distance);
                  if (_a > _b) {
                    return 1;
                  } else if (_a < _b) {
                    return -1;
                  } else {
                    return 0;
                  }
                });
                $scope.favoriteStores.sort((a, b) => {
                  var a = Number(a.establishment.is_open);
                  var b = Number(b.establishment.is_open);
                  if (a < b) return 1;
                  else if (a > b) return -1;
                  else return 0;
                });
              } else {
                $scope.favoriteStores = stores;
              }

              $ionicLoading.hide();
            },
            function () {
              $ionicLoading.hide();
              $scope.favoriteStores = response.data;
            }
          );
        })
        .finally(function () {
          $ionicLoading.hide();
          $scope.$broadcast("scroll.refreshComplete");
        });
    };
    $scope.selectStoreFavorite = function (store) {
      Swal.fire({
        title: "AVISO",
        text: "Você selecionou a loja " + store.name,
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#d33",
        confirmButtonText: "Receber no endereço",
        cancelButtonText: "Retirar na loja",
      }).then((result) => {
        if (result.value) {
          $scope.teste = store.EstablishmentOperation;
          if ($scope.teste == 2 || $scope.teste == 1) {
            console.log("ok");
            $scope.isDelivery = true;
            $scope.isPickUp = false;
            sharedProperties.setIsDelivery(true);
            sharedProperties.setIsPickUp(false);
            localStorage.setItem(
              "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
              JSON.stringify(store)
            );
            $state.go("app.products");
            $ionicHistory.clearCache();
          } else {
            Swal.fire({
              position: "center",
              type: "warning",
              title: "O estabelecimento não ativou esta opção",
              showConfirmButton: false,
              timer: 5000,
            });
            $state.go("app.stores");
          }
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          $scope.teste = store.EstablishmentOperation;
          if ($scope.teste == 3 || $scope.teste == 1) {
            console.log("ok");
            // console.log(store.EstablishmentOperation)
            $scope.isPickUp = true;
            $scope.isDelivery = false;
            sharedProperties.setIsPickUp(true);
            sharedProperties.setIsDelivery(false);
            localStorage.setItem(
              "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
              JSON.stringify(store)
            );
            $state.go("app.products");
            $ionicHistory.clearCache();
          } else {
            Swal.fire({
              position: "center",
              type: "warning",
              title: "O estabelecimento não ativou esta opção",
              showConfirmButton: false,
              timer: 5000,
            });
            $state.go("app.stores");
          }
        }
      });
    };

    $scope.doRefresh = function () {
      if ($scope.isPickUp) {
        //GetStoresPickUp();
        setTimeout(function () {
          $scope.$broadcast("scroll.refreshComplete");
        }, 4000);
      } else {
        if ($scope.currentNeighborhoodId) {
          storesService
            .getStores($scope.currentNeighborhoodId)
            .success(function (response) {
              $ionicLoading.show({
                template: RESOURCES.TEMPLATE_LOADING,
              });

              var posOptions = {
                timeout: 5000,
                enableHighAccuracy: true,
              };

              $cordovaGeolocation.getCurrentPosition(posOptions).then(
                function (position) {
                  var stores = response;
                  var lat = position.coords.latitude;
                  var long = position.coords.longitude;

                  if (lat) {
                    for (var i = 0; i < response.length; i++) {
                      var distance = Util.getDistanceFromLatLngInKm(
                        lat,
                        long,
                        stores[i].latitude,
                        stores[i].longitude
                      );
                      if (distance) {
                        stores[i].distance = distance.toFixed(1);
                      }
                    }

                    $scope.stores = stores.sort(function (a, b) {
                      var _a = Number(a.distance);
                      var _b = Number(b.distance);
                      if (_a > _b) {
                        return 1;
                      } else if (_a < _b) {
                        return -1;
                      } else {
                        return 0;
                      }
                    });
                    $scope.stores.sort((a, b) => {
                      var a = Number(a.is_open);
                      var b = Number(b.is_open);
                      if (a < b) return 1;
                      else if (a > b) return -1;
                      else return 0;
                    });
                  } else {
                    $scope.stores = stores;
                  }

                  $ionicLoading.hide();
                },
                function () {
                  $ionicLoading.hide();
                  $scope.stores = response;
                }
              );
            })
            .finally(function () {
              $ionicLoading.hide();
              $scope.$broadcast("scroll.refreshComplete");
            });
        } else {
          $scope.locateMe();
          setTimeout(function () {
            $scope.$broadcast("scroll.refreshComplete");
          }, 4000);
        }
      }
    };
    $scope.FailedSearch = function () {
      $ionicPopup.show({
        title: "Ops",
        content: "Não encontramos nenhum resultado para sua pesquisa",
        buttons: [
          {
            text: "Tentar Novamente",
            type: "button-positive",
            onTap: function () {
              $scope.keywords.data = "";
            },
          },
        ],
      });
    };
    $scope.setDelivery = () => {
      $scope.isDelivery = true;
      $scope.isPickUp = false;
      sharedProperties.setIsDelivery($scope.isDelivery);
      sharedProperties.setIsPickUp($scope.isPickUp);
      if ($scope.currentNeighborhoodId) {
        $scope.getAllStoresNotAccepted();
        $scope.getStoresByNeighborhood($scope.currentNeighborhoodId);
      } else {
        $scope.refB = true;
        $scope.locateMe();
      }
    };
    $scope.openShowSearchModal = function () {
      $scope.establishments_filtered = [];
      if ($scope.keywords.data === "") {
        $scope.showSearchModal.show();
        // $("inputSearch").blur();
        document.getElementById("inputSearch").blur();

        return;
      }

      var keywords = $scope.keywords.data;
      $scope.searchEstablishment(keywords);
      // $("inputSearch").blur();
      document.getElementById("inputSearch").blur();
    };
    $scope.searchEstablishment = function (data) {
      storesService
        .searchEstablishments(data, $scope.currentNeighborhoodId)
        .then(function (response) {
          console.log(response.data, "OOOO");
          var establishments_filtered = response.data;
          if (establishments_filtered.success == false) {
            $scope.FailedSearch();
          } else {
            $scope.stores = establishments_filtered.sort(function (a, b) {
              var _a = Number(a.distance);
              var _b = Number(b.distance);
              if (_a > _b) {
                return 1;
              } else if (_a < _b) {
                return -1;
              } else {
                return 0;
              }
            });
          }
        });
    };

    $scope.changeAddress = function () {
      $scope.selectedAddress = "";

      if (Util.getLocalToken()) {
        $state.go("app.addressList");
      } else {
        $state.go("app.region");
      }
    };

    $scope.selectStore = function (store) {
      var slugEstabelecimento = configIDL.ESTABLISHMENT_SLUG;

      //console.log($scope.slugEstabelecimento);

      if (
        slugEstabelecimento === "betapizzaria.deway.com.br" ||
        slugEstabelecimento == "br.com.ideliveryapp.chicodocarangueijo"
      ) {
        $scope.teste = store.EstablishmentOperation;
        if ($scope.isDelivery) {
          if ($scope.teste == 2 || $scope.teste == 1) {
            console.log("ok");
            $scope.isDelivery = true;
            $scope.isPickUp = false;
            sharedProperties.setIsDelivery(true);
            sharedProperties.setIsPickUp(false);
            localStorage.setItem(
              "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
              JSON.stringify(store)
            );
            $state.go("app.products");
            $ionicHistory.clearCache();
          } else {
            Swal.fire({
              position: "center",
              type: "warning",
              title: "O estabelecimento não ativou esta opção",
              showConfirmButton: false,
              timer: 5000,
            });
            $state.go("app.stores");
          }
        } else if ($scope.isPickUp) {
          if ($scope.teste == 3 || $scope.teste == 1) {
            console.log("ok");
            // console.log(store.EstablishmentOperation)
            $scope.isPickUp = true;
            $scope.isDelivery = false;
            sharedProperties.setIsPickUp(true);
            sharedProperties.setIsDelivery(false);
            localStorage.setItem(
              "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
              JSON.stringify(store)
            );
            $state.go("app.products");
            $ionicHistory.clearCache();
          } else {
            Swal.fire({
              position: "center",
              type: "warning",
              title: "O estabelecimento não ativou esta opção",
              showConfirmButton: false,
              timer: 5000,
            });
            $state.go("app.stores");
          }
        }
      } else {
        localStorage.setItem(
          "store_selected-" + configIDL.ESTABLISHMENT_SLUG,
          JSON.stringify(store)
        );
        $state.go("app.products");
        $ionicHistory.clearCache();
      }
    };
    $scope.setPickUp = () => {
      $scope.isDelivery = false;
      $scope.isPickUp = true;
      sharedProperties.setIsDelivery($scope.isDelivery);
      sharedProperties.setIsPickUp($scope.isPickUp);
      $scope.getStoresPickUp();
    };
    $scope.getStoresPickUp = () => {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });
      if ($scope.currentNeighborhoodId || $scope.currentAddress) {
        storesService
          .GetStoresPickUp()
          .success(function (response) {
            console.log(response.data);
            var posOptions = {
              timeout: 5000,
              enableHighAccuracy: true,
            };

            $cordovaGeolocation.getCurrentPosition(posOptions).then(
              function (position) {
                var stores = response.data;
                var lat = position.coords.latitude;
                var long = position.coords.longitude;

                if (lat) {
                  for (var i = 0; i < response.data.length; i++) {
                    var distance = Util.getDistanceFromLatLngInKm(
                      lat,
                      long,
                      stores[i].latitude,
                      stores[i].longitude
                    );
                    if (distance) {
                      stores[i].distance = distance.toFixed(1);
                    }
                  }
                } else {
                  $scope.stores = stores;
                }

                $ionicLoading.hide();
              },
              function () {
                $ionicLoading.hide();
                $scope.stores = response.data;
              }
            );
          })
          .finally(function () {
            $ionicLoading.hide();
            $scope.$broadcast("scroll.refreshComplete");
          });
      } else {
        $scope.locateMe();
        setTimeout(function () {
          $scope.$broadcast("scroll.refreshComplete");
        }, 4000);
      }
    };
    $scope.getAllStoresNotAccepted = () => {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });
      var posOptions = {
        timeout: 5000,
        enableHighAccuracy: true,
      };
      $cordovaGeolocation.getCurrentPosition(posOptions).then((position) => {
        var lat = position.coords.latitude;
        var long = position.coords.longitude;

        $scope.storesNotAccepted = [];
        storesService.getAllStores().then((response) => {
          $scope.allStores = response.data;
          $scope.allStores.sort((a, b) => {
            var a = Number(a.is_open);
            var b = Number(b.is_open);
            if (a < b) return 1;
            else if (a > b) return -1;
            else return 0;
          });

          $scope.allStores.forEach((establishment) => {
            storesService
              .addressValidation(establishment.id, $scope.currentAddress)
              .then((response) => {
                let addressValidation = response.data;
                //console.log(addressValidation, "teste");

                if (!addressValidation.success) {
                  $scope.storesNotAccepted.push(establishment);
                }

                var storesNotAccepted = $scope.storesNotAccepted;
                if (storesNotAccepted.length > 0) {
                  for (var i = 0; i < $scope.storesNotAccepted.length; i++) {
                    var distance = Util.getDistanceFromLatLngInKm(
                      lat,
                      long,
                      storesNotAccepted[i].latitude,
                      storesNotAccepted[i].longitude
                    );
                    if (distance) {
                      $scope.storesNotAccepted[i].distance = distance.toFixed(
                        1
                      );
                    }
                  }
                  $scope.storesNotAccepted = storesNotAccepted.sort(function (
                    a,
                    b
                  ) {
                    var _a = Number(a.distance);
                    var _b = Number(b.distance);
                    if (_a > _b) {
                      return 1;
                    } else if (_a < _b) {
                      return -1;
                    } else {
                      return 0;
                    }
                  });
                  $scope.storesNotAccepted.sort(function (a, b) {
                    var a = Number(a.is_open);
                    var b = Number(b.is_open);
                    if (a < b) return 1;
                    else if (a > b) return -1;
                    else return 0;
                  });

                  $ionicLoading.hide();
                }
              });
          });
        });

        $ionicLoading.hide();
      });
    };
    $scope.updateLocation = function () {
      $scope.currentNeighborhoodId = "";
      $scope.stores = [];
      $scope.noEstablishments = false;
      $scope.selectedAddress = "";
      $scope.locateMe();
    };

    /////////////////
    ///Locate User///
    /////////////////

    $scope.locateMe = function () {
      $ionicLoading.show({
        template: RESOURCES.TEMPLATE_LOADING,
      });

      var posOptions = {
        timeout: 5000,
        enableHighAccuracy: true,
      };
      $cordovaGeolocation.getCurrentPosition(posOptions).then(
        function (position) {
          var lat = position.coords.latitude;
          var long = position.coords.longitude;

          if (lat) {
            $scope.storesNotAccepted = [];
            storesService.getStoresByLocation(lat, long).then(
              function (response) {
                if (!response.data.success) {
                  $ionicLoading.hide();
                  $scope.noEstablishments = true;

                  notie.alert(3, response.data.message, 5);
                }

                if (response.data.data.neighborhood) {
                  $scope.selectedAddress =
                    response.data.data.neighborhood +
                    ", " +
                    response.data.data.city +
                    " - " +
                    response.data.data.state;

                  $scope.currentAddress = {
                    neighborhood: {
                      name: response.data.data.neighborhood,
                      city: { name: response.data.data.city },
                    },
                  };
                }

                var stores_ = response.data.data.establishments;

                if (stores_.length > 0) {
                  for (
                    var i = 0;
                    i < response.data.data.establishments.length;
                    i++
                  ) {
                    var distance = Util.getDistanceFromLatLngInKm(
                      lat,
                      long,
                      stores_[i].latitude,
                      stores_[i].longitude
                    );
                    if (distance) {
                      stores_[i].distance = distance.toFixed(1);
                    }
                  }

                  $scope.stores = stores_.sort(function (a, b) {
                    var _a = Number(a.distance);
                    var _b = Number(b.distance);
                    if (_a > _b) {
                      return 1;
                    } else if (_a < _b) {
                      return -1;
                    } else {
                      return 0;
                    }
                  });
                  $scope.stores.sort(function (a, b) {
                    var a = Number(a.is_open);
                    var b = Number(b.is_open);
                    if (a < b) return 1;
                    else if (a > b) return -1;
                    else return 0;
                  });
                  storesService.getAllStores().then((response) => {
                    $scope.allStores = response.data;
                    $scope.storesNotAccepted = [];
                    $scope.allStores.sort((a, b) => {
                      var a = Number(a.is_open);
                      var b = Number(b.is_open);
                      if (a < b) return 1;
                      else if (a > b) return -1;
                      else return 0;
                    });
                    $scope.allStores.forEach((establishment) => {
                      storesService
                        .addressValidation(
                          establishment.id,
                          $scope.currentAddress
                        )
                        .then((response) => {
                          let addressValidation = response.data;

                          if (!addressValidation.success) {
                            $scope.storesNotAccepted.push(establishment);
                          }
                          var storesNotAccepted = $scope.storesNotAccepted;
                          if (storesNotAccepted.length > 0) {
                            for (
                              var i = 0;
                              i < $scope.storesNotAccepted.length;
                              i++
                            ) {
                              var distance = Util.getDistanceFromLatLngInKm(
                                lat,
                                long,
                                storesNotAccepted[i].latitude,
                                storesNotAccepted[i].longitude
                              );
                              if (distance) {
                                $scope.storesNotAccepted[
                                  i
                                ].distance = distance.toFixed(1);
                              }
                            }

                            $scope.storesNotAccepted = storesNotAccepted.sort(
                              function (a, b) {
                                var _a = Number(a.distance);
                                var _b = Number(b.distance);
                                if (_a > _b) {
                                  return 1;
                                } else if (_a < _b) {
                                  return -1;
                                } else {
                                  return 0;
                                }
                              }
                            );
                            $scope.storesNotAccepted.sort(function (a, b) {
                              var a = Number(a.is_open);
                              var b = Number(b.is_open);
                              if (a < b) return 1;
                              else if (a > b) return -1;
                              else return 0;
                            });
                            $ionicLoading.hide();
                          }
                        });
                    });
                  });
                } else {
                  storesService.getAllStores().then((response) => {
                    $scope.allStores = response.data;
                    $scope.storesNotAccepted = [];
                    $scope.allStores.sort((a, b) => {
                      var a = Number(a.is_open);
                      var b = Number(b.is_open);
                      if (a < b) return 1;
                      else if (a > b) return -1;
                      else return 0;
                    });

                    $scope.allStores.forEach((establishment) => {
                      storesService
                        .addressValidation(
                          establishment.id,
                          $scope.currentAddress
                        )
                        .then((response) => {
                          let addressValidation = response.data;

                          if (!addressValidation.success) {
                            $scope.storesNotAccepted.push(establishment);
                          }
                          var storesNotAccepted = $scope.storesNotAccepted;
                          if (storesNotAccepted.length > 0) {
                            for (
                              var i = 0;
                              i < $scope.storesNotAccepted.length;
                              i++
                            ) {
                              var distance = Util.getDistanceFromLatLngInKm(
                                lat,
                                long,
                                storesNotAccepted[i].latitude,
                                storesNotAccepted[i].longitude
                              );
                              if (distance) {
                                $scope.storesNotAccepted[
                                  i
                                ].distance = distance.toFixed(1);
                              }
                            }

                            $scope.storesNotAccepted = storesNotAccepted.sort(
                              function (a, b) {
                                var _a = Number(a.distance);
                                var _b = Number(b.distance);
                                if (_a > _b) {
                                  return 1;
                                } else if (_a < _b) {
                                  return -1;
                                } else {
                                  return 0;
                                }
                              }
                            );
                            $scope.storesNotAccepted.sort(function (a, b) {
                              var a = Number(a.is_open);
                              var b = Number(b.is_open);
                              if (a < b) return 1;
                              else if (a > b) return -1;
                              else return 0;
                            });
                            $ionicLoading.hide();
                          }
                        });
                    });
                  });
                }

                var stores_ = response.data.data.establishments;

                if (stores_.length > 0) {
                  for (
                    var i = 0;
                    i < response.data.data.establishments.length;
                    i++
                  ) {
                    var distance = Util.getDistanceFromLatLngInKm(
                      lat,
                      long,
                      stores_[i].latitude,
                      stores_[i].longitude
                    );
                    if (distance) {
                      stores_[i].distance = distance.toFixed(1);
                    }
                  }

                  $scope.stores = stores_.sort(function (a, b) {
                    var _a = Number(a.distance);
                    var _b = Number(b.distance);
                    if (_a > _b) {
                      return 1;
                    } else if (_a < _b) {
                      return -1;
                    } else {
                      return 0;
                    }
                  });
                  $scope.stores.sort(function (a, b) {
                    var a = Number(a.is_open);
                    var b = Number(b.is_open);
                    if (a < b) return 1;
                    else if (a > b) return -1;
                    else return 0;
                  });
                  $ionicLoading.hide();
                  console.log($scope.storesNotAccepted, "ihjul");
                } else {
                  $ionicLoading.hide();
                  $scope.noEstablishments = true;
                }
              },
              function () {
                $ionicLoading.hide();
                $scope.noEstablishments = true;
                Swal.fire({
                  position: "center",
                  type: "warning",
                  title:
                    "Não foi possível obter informações da localização atual.",
                  showConfirmButton: false,
                  timer: 5000,
                });
                // notie.alert(
                //     3,
                //     "Não foi possível obter informações da localização atual.",
                //     5
                // );
              }
            );
          } else {
            $ionicLoading.hide();
            $scope.noEstablishments = true;
            Swal.fire({
              position: "center",
              type: "warning",
              title: "Não foi possível obter informações da localização atual.",
              showConfirmButton: false,
              timer: 5000,
            });
            // notie.alert(
            //     3,
            //     "Não foi possível obter informações da localização atual.",
            //     5
            // );
          }
        },
        function () {
          $ionicLoading.hide();
          $scope.noEstablishments = true;
          Swal.fire({
            position: "center",
            type: "warning",
            title: "Não foi possível obter informações da localização atual.",
            showConfirmButton: false,
            timer: 5000,
          });
          // notie.alert(
          //     3,
          //     "Não foi possível obter informações da localização atual.",
          //     5
          // );
        }
      );
    };

    ////////////////////////////////
    ///Get stores by neighborhood///
    ////////////////////////////////

    $scope.getStoresByNeighborhood = function (id) {
      storesService.getStores(id).success(
        function (response) {
          $ionicLoading.show({
            template: RESOURCES.TEMPLATE_LOADING,
          });

          var posOptions = {
            timeout: 5000,
            enableHighAccuracy: true,
          };

          $cordovaGeolocation.getCurrentPosition(posOptions).then(
            function (position) {
              var stores = response;
              var lat = position.coords.latitude;
              var long = position.coords.longitude;

              if (lat) {
                for (var i = 0; i < response.length; i++) {
                  var distance = Util.getDistanceFromLatLngInKm(
                    lat,
                    long,
                    stores[i].latitude,
                    stores[i].longitude
                  );
                  if (distance) {
                    stores[i].distance = distance.toFixed(1);
                  }
                }

                $scope.stores = stores.sort(function (a, b) {
                  var _a = Number(a.distance);
                  var _b = Number(b.distance);
                  if (_a > _b) {
                    return 1;
                  } else if (_a < _b) {
                    return -1;
                  } else {
                    return 0;
                  }
                });
                $scope.stores.sort((a, b) => {
                  var a = Number(a.is_open);
                  var b = Number(b.is_open);
                  if (a < b) return 1;
                  else if (a > b) return -1;
                  else return 0;
                });
              } else {
                $scope.stores = stores;
              }

              $ionicLoading.hide();
            },
            function () {
              $ionicLoading.hide();
              $scope.stores = response;
            }
          );
        },
        function () {
          $ionicLoading.hide();
          notie.alert(
            3,
            "Não foi possível obter informações da localização atual.",
            5
          );
        }
      );
    };
  }
})();
