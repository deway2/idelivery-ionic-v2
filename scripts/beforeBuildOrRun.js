module.exports = function (ctx) {

    // get required modules
    var fs = ctx.requireCordovaModule('fs');
    var parse = require('xml2js'); // third module
    var deferral = ctx.requireCordovaModule('q').defer();
    var exec = require('child_process').execSync;

    console.log("HOOK BEFORE BUILD OR RUN STARTING...");

    // get app_name param
    var app_name = ctx.opts.options.app_name;

    // get config.xml
    var config = fs.readFileSync('config.xml', 'utf8');

    // get settings file and convert to js object
    var customerSettingsFile = fs.readFileSync('customer-settings/' + app_name + '/settings.json', 'utf8');
    var customerSettings = JSON.parse(customerSettingsFile);

    console.log("customerSettings: ", JSON.stringify(customerSettings));
    
    // set style configs
    fs.writeFileSync("www/css/_variables.scss", customerSettings.style);

    // read icon and splash files
    var icon = fs.readFileSync('customer-settings/' + app_name + '/icon.png');
    var splash = fs.readFileSync('customer-settings/' + app_name + '/splash.png');
    var logo = fs.readFileSync('customer-settings/' + app_name + '/logo.png');
    var background = fs.readFileSync('customer-settings/' + app_name + '/background.png');
    var googleservices = fs.readFileSync('customer-settings/' + app_name + '/google-services.json');
    var googleservicesiOS = fs.readFileSync('customer-settings/' + app_name + '/GoogleService-Info.plist');
    // var iphonex = fs.readFileSync('customer-settings/' + app_name + '/Default@2x~universal~anyany.png');

    // write new icon and splash files
    fs.writeFileSync('resources/icon.png', icon);
    fs.writeFileSync('resources/splash.png', splash);
    fs.writeFileSync('www/img/logo.png', logo);
    fs.writeFileSync('www/img/background.png', background);
    fs.writeFileSync('./google-services.json', googleservices);
    fs.writeFileSync('./GoogleService-Info.plist', googleservicesiOS);
    // fs.writeFileSync('./resources/ios/splash/Default@2x~universal~anyany.png', iphonex);


    try {
        if (fs.existsSync('customer-settings/') !== null) {
            fs.rename('www/img/logo.png', 'www/img/logo.png', function (err) {
                if (err) throw err;
                fs.stat('www/img/logo.png', function (err, stats) {
                });
            });
        }
    }
    catch (err) {

    }

    console.log("ionic resources starting...");
    console.log("...");

    exec("ionic resources");
    console.log("ionic resources finishing...");

    //Set Login Facebook plugin config
    //P.S.: phonegap-facebook-plugin folder must be on User Home folder.
    console.log("Executing config Facebook Login and Sender ID...");
    console.log("APP_ID = " + customerSettings.facebookAppId + "  APP_NAME = " + customerSettings.facebookAppName);
    console.log("SENDER_ID = " + customerSettings.senderId);
    console.log("ONESIGNAL_ID = " + customerSettings.ONESIGNAL_ID);

    try {
        console.log("check if plugins are installed: ");
        if (fs.existsSync('plugins/cordova-plugin-facebook4/plugin.xml')) {
            console.log("Facebook plugin is installed. Will be removed...");

            var command = "cordova plugin remove cordova-plugin-facebook4 --save";
            exec(command, function (error, stdout) {
                console.log(stdout);
            });

            console.log("Facebook plugin removed");
        }

        if (fs.existsSync('plugins/phonegap-plugin-push/plugin.xml')) {
            console.log("Push plugin is installed. Will be removed...");

            var command = "cordova plugin remove phonegap-plugin-push --save";
            exec(command, function (error, stdout) {
                console.log(stdout);
            });

            console.log("Push plugin removed");
        }

    } catch (err) {
        console.log("facebook and push plugins are not installed");
    } finally {
         // parse xml to js object
        parse.parseString(config, function (err, result) {
            // set properties
            result.widget.name = customerSettings.name;
            result.widget.$.id = customerSettings.id;
            //result.widget.$.ios-CFBundleIdentifier = customerSettings.id;

            // set Facebook plugin app name and app id properties; set push plugin sender id
            for (var i = 0; i < result.widget.plugin.length; i++){
                if (result.widget.plugin[i].$.name === 'cordova-plugin-facebook4') {
                    result.widget.plugin[i].variable[0].$.value = customerSettings.facebookAppName;
                    result.widget.plugin[i].variable[1].$.value = customerSettings.facebookAppId;
                }
                if (result.widget.plugin[i].$.name === 'phonegap-plugin-push') {
                    result.widget.plugin[i].variable[0].$.value = customerSettings.senderId;
                }
            };

            // object to parse js object to xml
            var builderXml = new parse.Builder();

            // parse js object to xml
            var finalXml = builderXml.buildObject(result);

            fs.writeFileSync('config.xml', finalXml);
        });

        deferral.resolve();
    }


    //Set Config.js
    fs.writeFileSync("www/js/config.js", customerSettings.config);

    console.log("HOOK BEFORE BUILD OR RUN FINISHED!");

    return deferral.promise;
};
