(function () {
  angular.module("idelivery").controller("contactCtrl", contactCtrl);

  contactCtrl.$inject = ["$scope", "$ionicPopup", "ideliveryServices"];
  function contactCtrl($scope, $ionicPopup, ideliveryServices) {
    var currentDevicePlatform = {
      name: ionic.Platform.platform(),
      version: ionic.Platform.version(),
    };

    if (
      currentDevicePlatform.name === "android" &&
      currentDevicePlatform.version <= 4.3
    ) {
      $scope.isOldAndroid = true;
    }

    $scope.contactData = {};

    $scope.submit = function () {
      $scope.contactData.slug = configIDL.ESTABLISHMENT_SLUG;

      ideliveryServices
        .sendFeedback($scope.contactData)
        .then(function (response) {
          if (response.status == 201) {
            $ionicPopup.show({
              title: "Contato",
              subTitle:
                "Seu contato foi enviado com sucesso! Em breve retornaremos sua mensagem.",
              scope: $scope,
              buttons: [
                {
                  type: "button-positive",
                  text: "Ok",
                },
              ],
            });
            $scope.contactData = {};
          } else {
            $ionicPopup.show({
              title: "Contato",
              subTitle:
                "Mensagem não enviada, tivemos problemas durante o envio de sua mensagem, tente novamente mais tarde.",
              scope: $scope,
              buttons: [
                {
                  type: "button-positive",
                  text: "Ok",
                },
              ],
            });
          }
        });
    };

    //This event fires when the keyboard will be shown
    // if ($scope.isIOS()) {
    // 	window.addEventListener('native.keyboardhide', function (e) {
    // 		angular.element(document.querySelector('#id-scroll-deway-contact')).addClass('resize-scroll');
    // 	});
    //
    // 	window.addEventListener('native.keyboardshow', function () {
    // 		angular.element(document.querySelector('#id-scroll-deway-contact')).removeClass('resize-scroll');
    // 		$ionicScrollDelegate.resize();
    // 	});
    // }
  }
})();
