var configIDL = { ESTABLISHMENT_ID: null, 
 ESTABLISHMENT_SLUG: 'br.com.ideliveryapp.idrink', 
 ESTABLISHMENT_NAME: 'WeDrink', 
 SENDER_ID: '1045813999761',
 ONESIGNAL_ID: '54bbdc10-77f8-415f-8066-f13300b0282f', 
 ANALYTICS_ID: 'UA-125964510-1', 
 LIST_LAYOUT: 'list', 
 MENU_STYLE: 2, 
 OPTIONAL_RECEIPT: 1, 
 COMMENTS_TEXT: 'Observações', 
 SHOW_SCHEDULE: true, 
 SERVERS: [
'http://idlnotifications.deway.com.br/servers.json', 
 'https://ideliveryapp.com.br/servers.json' 
], 
 PAY_TYPES: [ 
 { 
 id: 1, name: 'Espécie', negative_id: -1 
},
 { 
 id: 2, name: 'Cartão', negative_id: -2 
},
 { 
 id: 3, name: 'Online', negative_id: -3 
} 
 ] 
 };